Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - équipe Formateurs & Référents SIG

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.16 de QGIS



# Ajouter un flux WMS/WFS

{% hint style='info' %} L'accès à certains flux peut être limité au réseau Inrap (éventuellement via une connexion VPN), c'est le cas pour le Scan 25 de l'IGN notamment {% endhint %}

le Web Map Service (WMS), le Web Map Tile Service (WMTS), le Web Feature Service (WFS) sont des protocoles fournissant des données géoréfencées par des URL. On parle souvent de flux de données. Ces différents services sont mis à disposition par des serveurs cartographiques. 

**WMS** : *Web Map Service* fournit une image géoréférencée (rasters) à partir de différents serveurs de données (IGN, BRGM, DREAL, GeoBretagne, etc.)

**WMTS**: *Web Map Tile Service* fournit des images géoréfencées tuilées, et donc theoriquement un temps de chargement des sonnées plus rapide que le WMS.

**WFS** : *Web feature Service* permet de manipuler des objets géographiques (vecteurs : lignes, points, polygones)



Les informations générales sont à voir sur l'intranet Inrap à la page [Se connecter aux webservices de l'IGN](https://intranet.inrap.fr/reseaux/sig/articles/se-connecter-aux-webservices-de-lign).

{% hint style='danger' %}Pour rappel, les données de l'IGN sont utilisables pour un usage scientifique, technique et pédagogique. On n'oublie pas de citer ses sources (c'est pas parce que c'est gratuit qu'on peut s'en dispenser) !{% endhint %}




## 1. Configurer une connexion WMS/WMTS/WFS

## Protocole 1 : créer les connexions une par une en copiant les URL

Vous n'avez à faire cette configuration que lors de la première connexion sur votre poste ou bien en cas de changement de version de QGIS. Une fois vos connexions configurées, allez à l'étape *2. Ajouter une couche du serveur WMS*

![icône ajout de flux](images/WMS_01.png)

- Cliquer sur l'une des icônes ci-dessus en fonction du format (WMS/WMTS-WCS-WFS).

- Cliquer sur le bouton [Nouveau]

![charger une connexion via une URL](images/WMS_02.png)

- Saisir un nom au choix (explicite) et copier l'URL du flux dans la cellule dédiée et valider avec [OK] ↓

![charger une connexion via une URL 2](images/WMS_03.png)



## Protocole 2 : créer les connexions par lot à l'aide du fichier xml

- Télécharger, dézipper et stocker les fichiers xml sur votre ordinateur (fichier accessible depuis [l’article de l’intranet](https://intranet.inrap.fr/reseaux/sig/articles/se-connecter-aux-webservices-de-lign)) :warning: Il y a un fichier par format de données (WMTS, WFS).

> vous pouvez aussi télécharger indépendamment le fichier xml pour le flux WMTS [IGN_WMTS_20220208.xml](https://gitlab.com/formationsig/fiches-techniques/-/raw/master/fichiers/IGN_WMTS_20220208.xml?inline=false) et celui pour les flux WFS [IGN_WFS_20220201.xml](https://gitlab.com/formationsig/fiches-techniques/-/raw/master/fichiers/IGN_WFS_20220201.xml?inline=false)
- Dans QGIS, en fonction du format d’affichage de la ressources (WMS/WMTS, WCS, WFS), ouvrir le gestionnaire des sources de données en cliquant sur l’un des trois boutons de la barre « Gestion des couches ».

![icône ajout de flux](images/WMS_01.png)

- Dans le panneau Gestionnaire de sources de données, cliquer sur [Charger], puis aller chercher le fichier .xml

![charger une connexion via un fichier xml](images/WMS_02.png)

- Sélectionner tout ou partie des ressources puis [Importer]
- ![sélection des flux](images/WMS_04.png)

Les connexions sont automatiquement crées.

{% hint style='info' %} Attention ! Le fichier xml est une aide à la création des connexions. Il propose une sélection des ressources existantes. Si vous voulez explorer d’autres ressources, référez-vous au [tableau de correspondances des ressources IGN](https://geoservices.ign.fr/documentation/services/tableau_ressources). {% endhint %}


## 2. Ajouter une couche du serveur WMS

- Pour ajouter une couche, cliquer sur l'icône ou menu *Couche > Ajouter une couche > Ajouter une couche WMS/WMTS* ou Ctrl + Maj + W

- Dans le menu déroulant, choisir le serveur puis cliquer sur [Connexion]

![choix su serveur](images/WMS_05.png)

- La liste des couches disponibles s'affiche 

![liste des couches disponibles](images/WMS_06.png)

- Une fois la couche sélectionnée, cliquer sur [Ajouter], puis [Fermer].



{% hint style='danger' %}  N'oubliez pas de noter le copyright des données. Exemple : SCAN25® © IGN 2011 {% endhint %}
