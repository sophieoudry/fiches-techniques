Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - Emeline Le Goff, Frédéric Audouit

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# Les données altimétriques du RGEalti®-IGN© (grande échelle d'exploitation : à l'échelle communale ou à l'échelle d'un chantier) 



## Récupérer les données altimétriques du RGEalti®-IGN © via le NAS de la DST

L’IGN**©** (l’Institut National de l’Information Géographique et Forestière) a produit un MNT (modèle numérique de terrain) représentant le relief du territoire. Deux précisions sont mises à disposition sous Licence Ouvert, issues du RGE (le référentiel à grande échelle) : le RGEalti® au pas de 5 m (1 pixel correspond à 5 m), et le RGE-alti**®** au pas de 1 m (1 pixel correspond à 1 m). Ces dalles ont une emprise de 5 km² pour le RGE-alti**®** au pas de 5 m, et de 2 km² pour le RGE-alti**®** au pas de 1 m.

Source d’information : https://www.data.gouv.fr/fr/datasets/referentiel-a-grande-echelle-rge/

Ces deux produits sont des documents de type image (raster). Pour faciliter le choix des dalles à récupérer en fonction de l’emprise de l’opération étudiée, l’IGN a créé des dalles vectorielles qui permettent l’affichage, dans un SIG, du nom de chaque dalle (via une table attributaire dédiée). 

La DST de l’Inrap met à disposition des topographes et des référents SIG, depuis le 06/03/2020, ces données sur le NAS de la DST (nas-DST - **10.210.1.214**), récupérables sur des serveurs dédiés, par direction.

Exemple (pour la DIR-GO) : Compte utilisateur : ign-go ; Mot de passe : Igngo@2020



![](images\alti_01.jpg)



Pour récupérer les **fichiers vecteurs identifiant les dalles** (shapefiles ou shapes) :

- Le **RGE-alti**® **à 1 m** :

  - les **dalles** **comportant le nom des dossiers** des dalles MNT sont récupérables dans les dossiers «3_SUPPLEMENTS_LIVRAISON_... » : copier toutes les extensions (ex : pour « Dalles_GO », les .shp, .dbf, .prj, .shx) et les coller dans un dossier de votre ordinateur. 

  ![](images\alti_02.jpg)

  - les **dalles** **comportant le numéro de chaque dalle** sont récupérables dans un fichier zippé à la racine du serveur (ex : Découpage_dalles_GO.zip > RGEAlti_1m_fichiers_GO). Ces dalles ont une emprise 5 km² chacune.

- Le **RGE-alti**® **à 5 m** : les dalles sont intégrées dans des dossiers compressés par département, et sont présentes dans chaque dossier « 5_SUPPLEMENTS_LIVRAISON_... » :

![](images\alti_03.jpg)



{% hint style='info' %} 
NB : Avant la création de ces documents, l’IGN© a produit la **Bd-Alti®** aux pas de 20 m / 50 m / 75 m /… (en l’état sur le serveur de la DST : une dalle au pas de 20 m par département). Aussi, **selon l’échelle de la zone d’étude** (petite échelle d'exploitation - échelles régionale ou départementale), et dans un souci de poids lié à l’archivage (et au temps pour réaliser les traitements à venir), vous pouvez tout aussi bien récupérer les données du RGE-alti® au pas de 5m, de la **Bd-Alti®**, mais aussi du **fonds STRM®** accessible via une extension dans QGIS (cf. fiche technique STRM ®©NASA/NGA - à venir). 
{% endhint%}





## Intégrer les dalles du RGE-alti® dans un SIG



Dans un projet QGIS (paramétré en **SCR Lambert 93**), **importer le SHP emprise de l’opération** vectorisé à partir de l’arrêté de prescription (ou bien zoomer sur la zone concernée en intégrant au préalable le Scan25 de l’IGN via le flux WMS ou les limites communales via CAVIAR). 

Ensuite, **importer les SHP des dalles** : **Dalles_GO.shp** (dalles au pas de 5 m comportant le nom des dossiers des dalles MNT) + **RGEAlti_1m_fichiers_GO.shp** (dalles au pas de 1m comportant le numéro de chaque dalle MNT).

**Etiqueter (afficher) les entités**, respectivement sur le **nom des dossiers** (NomDossier) et le **nom des dalles** (NOM_DALLES).

**Copier-coller les dalles repérées**, dans un dossier lié aux fonds cartographiques de votre opération.

Pour cette démonstration, il s’agit d’un site diagnostiqué sur les communes de Trézilidé et Mespaul. Par exemple : zoomer sur le SHP de l’emprise (D117921_emprise.shp) >> vous constatez que 3 dalles de 1 m seront nécessaires pour couvrir l’ensemble de la surface diagnostiquée. Et à des fins d’homogénéisation, nous pourrons donc récupérer les 4 dalles afin d’avoir un zone d’étude rectangulaire. Ces dalles se trouvent dans le Dossier « 3H » et portent les intitulés : 

RGEALTI_FXX_0179_6858_MNT_LAMB93_IGN69 ; RGEALTI_FXX_0180_6858_MNT_LAMB93_IGN69 ; 

RGEALTI_FXX_0179_6857_MNT_LAMB93_IGN69 ; RGEALTI_FXX_0180_6857_MNT_LAMB93_IGN69.

{% hint style= 'danger' %} Dans le nom des dalles MNT figure le **début des** **coordonnées X et Y de l’angle nord-ouest**. {% endhint %}



![](images\alti_04.jpg)



