Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - Emeline Le Goff, Frédéric Audouit

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# Gérer la symbologie d'un MNT (Modèle Numérique de Terrain)



## Affichage du relief

Dans un projet QGIS (paramétré en **SCR Lambert 93**), **importer les dalles raster MNT** concernées (ou le raster si fusion des dalles effectuée au préalable). Vous constatez que les dégradés de couleurs correspondant aux variations altimétriques ne sont pas semblables d’une dalle à l’autre : c’est logique car **les valeurs d’altitude minimales ou maximales pour chaque dalle ne sont pas les mêmes**. Nous allons donc vouloir **les homogénéiser en indiquant des bornes min-max similaires** pour avoir des mêmes dégradés de couleur cohérents.



![](images\alti_05.jpg)



Pour cette démonstration, sur le diagnostic D117921 au lieu-dit Lan ar Marc'h (communes de Trézilidé et Mespaul), on peut voir que les valeurs d’altitude sur les 4 dalles MNT concernées oscillent entre 33.1906 et 88.1726. Aussi, nous allons vouloir **imposer pour chaque dalle, une plage de dégradé de couleurs englobant les bornes d’altitude minimales et maximales du site** (ici, allant de 33 à 89 m, voire 30 à 90 m). 

Pour ce faire, aller dans les Propriétés d’un des rasters > **onglet Symbologie** : **changer les valeurs**, et mettre les valeurs 30 pour le Min et 80 pour 90 pour le Max. Par défaut, le rendu est en dégradé de gris : choisissez comme **type de rendu Pseudo-couleur à bande unique** pour avoir une sélection de camaïeu de couleurs. 

{% hint style= 'info' %} allez de la variation la plus claire pour les valeurs minimales aux plus foncées pour les valeurs d’altitude maximales. {% endhint %}





![](images\alti_06.jpg)



Pour vous faciliter la tâche et afin de ne pas produire d’erreur, vous pouvez **pour les autres rasters, copier-coller le style du 1er raster et l’appliquer sur les autres**. Dans QGIS, cliquer sur le 1er raster, clic-droit > Styles > Copier le style. Puis allez sur un autre > clic-droit > Style > Coller le style.



![](images\alti_07.jpg)



![](images\alti_08.jpg)



{% hint style= 'tip' %} si vous souhaitez que cette symbologie soit appliquée aux dalles dès lors dans un autre projet (cf. si envoi à d’autres collègues), vous pouvez **pérenniser ce style dans l’onglet Symbologie > Style** (en bas à gauche de la fenêtre de Symbologie) **> Enregistrer par défaut**. Par contre, il faudra répéter cette manipulation pour chacune des dalles. Aussi, le plus pratique est de générer un seul et même raster à partir de ces 4 dalles, en les fusionnant. {% endhint %}



## Appliquer un ombrage au MNT pour mieux faire ressortir le relief

Réimporter le raster final (D117921_emprise) ou **dupliquer la couche**. Le but est d’avoir une couche avec le dégradé de couleur (« MNT_D117921 altitudes », ; et par-dessus, avoir cette même couche (« MNT_D117921 ombrage ») mais en lui appliquant cette fois-ci, un indice d’ombrage et une transparence pour mieux faire ressortir les anfractuosités du relief. Vous pouvez renommer (virtuellement dans le projet) les couches afin de vous guider.

Sur la couche du raster « MNT_D117921 ombrage », allez dans Propriétés > **onglet Symbologie > Type de rendu : Ombrage** (laisser les paramètres par défaut, pour un éclairage au nord-ouest). Par défaut, le raster est opaque et recouvre donc le MNT illustrant les variables d’altitude. Aussi, vous pouvez appliquer une transparence à ce raster > **onglet Transparence > mettre l’opacité à 40%**.

![](images\alti_13.jpg)



![](images\alti_14.jpg)

