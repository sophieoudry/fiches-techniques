Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - Emeline Le Goff, Frédéric Audouit

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# Fusionner des dalles MNT en un seul raster



Passer par le Menu Raster ou la Boîte à outils de traitement > outil Fusionner. Mais il peut y avoir un bug. Dans ce cas, utiliser le **Menu Raster > Divers > Construire un raster virtuel**.

![](images/alti_09.jpg) 



Vous pouvez générer un raster virtuel, ou bien l’enregistrer en dur dans votre dossier déjà lors de cette étape. L’extension du raster sera alors un .vrt -Viscape For Netscape Navigator, compatible sous Windows ; si vous désirez en faire un .TIFF ou .JPGEG, faites ensuite sur le fichier MNT obtenu un clic-droit > exporter > enregistrer-sous > Format GeoTIFF.



![](images\alti_10.jpg) 



![](images\alti_11.jpg)



Vous pouvez regrouper les dalles dans un groupe et le désélectionner ; et appliquer le style précédent au MNT final : clic-droit sur la couche > Style > Coller le style (cf. fiche technique [Gérer la symbologie d'un raster](https://formationsig.gitlab.io/fiches-techniques/raster/06_symbol_MNT.html)).



![](images\alti_12.jpg) 