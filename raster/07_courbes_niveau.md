Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - Emeline Le Goff, Frédéric Audouit

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# Générer des courbes de niveau à partir d'un MNT 



Le but est de créer une couche vectorielle (shape de ligne) qui contiendra dans la table attributaire, un champ avec les valeurs d’altitudes de chaque courbe/ligne, à partir d’une couche Raster.



## Menu Raster > Extraction > Contour

Choisir le MNT (issu de la fusion des 4 dalles dans l’exemple présent). Par défaut, le **nom du champ dans la future table attributaire** est noté ELEV (pour élévation), vous pouvez le laisser ainsi ou le modifier en « ALT » ou « Z ».

Pour les **intervalles**, comme il s’agit d’un raster au **pas de 1 m**, il est bon de savoir que si vous souhaitez des courbes au pas de 0.5 m ou 0.2 m, les courbes intermédiaires aux valeurs entières métriques seront interpolées ; aussi, par défaut, indiquer « 1 m » pour les intervalles (ici le choix a porté sur un pas de 0.5 m pour insister visuellement sur les fortes pentes par endroits). **Enregistrer la future couche** dans votre dossier opération (par exemple, CdN_50cm.shp).



![](images\alti_15.jpg)





## Gestion de l'affichage des couches

**Symbologie** : Vous pouvez simplement mettre une couleur brune pour chaque ligne. Mais l’on peut également créer des **Ensembles de règles** pour mettre une couleur et une épaisseur différentes selon les pas métriques (marron foncé, épaisseur 0.20) ou décimétriques (marron clair, épaisseur 0.15).

Pour ce faire, ajouter une règle (en cliquant sur « + », puis cliquer sur la règle pour l’éditer via le moteur SQL (cf. [fiche technique SQL](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/08_operateurs_sql.html), et taper "Z" LIKE '%.5%' pour la règle ne concerne que les valeurs décimales. Ajouter une autre règle pour les valeurs rondes métriques en saisissant ELSE (sous-entendu « autre » que la première règle, donc ne concernant pas les valeurs décimales).

![](images\alti_16.jpg)



**Etiquettes** : Vous pouvez réitérer l’opération mais via l’onglet des Etiquettes, en créant une règle d’affichage pour les valeurs d’altitude décimales (que vous pourrez d’ailleurs décocher pour ne laisser ensuite que les valeurs métriques), puis une règle pour les pas de 1 m (Filtre = ELSE). Vous pouvez mettre la même couleur marron foncé, taille 6.5.

Vous pouvez mettre un tampon (halo blanc, de taille 0.3) pour mieux mettre en évidence les chiffres ; et placer (position) les numéros sur la ligne.

{% hint style= 'danger' %} n’oubliez pas d’indiquer le champ à afficher (ici « Z ») en cochant au préalable « Etiquettes ». {% endhint %}

![](images\alti_17.jpg)



## Découper les courbes de niveau selon l'emprise étudiée

Selon l’emplacement de votre emprise par rapport aux dalles MNT concernées, il vous sera peut-être nécessaire de découper le shape des courbes de niveau en fonction d’une emprise plus restreinte que celle de la dalle d’origine de création des courbes.

Aussi, placez l’emprise de votre opération au centre du canevas de la carte (espace de visualisation du projet QGIS), puis Menu **Vecteur > outils de Géotraitement > Intersection**.



![](images\alti_18.jpg) 



Enregistrez la couche vectorielle dans votre dossier opération. Elle est importée automatiquement. Copier/coller-lui le même style que celui configuré sur le shape des courbes de niveau d’origine.

Vous pouvez compléter la cartographie des données liées au cadastre, parcelles et bâti (importées depuis un flux WFS - si existant - ou présentes sur le fonds aménageur d’origine en DXF - si existant).



![](images\alti_19.jpg) 

