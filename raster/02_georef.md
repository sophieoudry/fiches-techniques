Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - équipe Formateurs & Référents SIG

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# Géoréférencer des images

## 1. Manipulations

Avec l'extension Géoréférenceur GDAL (éventuellement à [activer dans les Extensions](https://formationsig.gitlab.io/fiches-techniques/priseenmain/05_extensions.html))

Avant tout géoréférencement, on doit choisir la couche de référence dans son projet QGIS. 

Si la couche de référence est une couche vecteur, il faut alors activer l’accrochage d’objets via la [barre d'outils d'accrochage](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/02_accrochage.html) :

![barre d'outils d'accrochage](images/georef_01.jpg)



Menu *Raster > Géoréférencer*

Depuis la fenêtre du Géoréférenceur, ouvrir le raster à géoréréfencer. 

![ajouter des points de contrôle](images/georef_02.png)



- Ajouter un nouveau point de contrôle en cliquant sur le raster à géoréférencer
- Choisir «Depuis le canevas de la carte»
- Cliquer sur le point d’amer dans le canevas.
Refaire la manipulation autant de fois que l’on veut mettre de points
- Lancer le géoréférencement
- Vérifier le SCR (2154 !)
- Sauvegarder le fichier en gardant «_georef»
- Choisir la compression DEFLATE
- Cocher «employer 0 pour la transparence»
- Cocher «Charger dans QGis lorsque terminé»

![paramètres de transformation](images/georef_03.png)



## 2. Rappel sur les transformations à utiliser

* Helmert : mise à l’échelle, translation et rotation (2 pts minimum)
* Polynomiale de degré 1 : mise à l’échelle, translation et rotation (4 pts minimum)
* Polynomiale de degré 2 : déformation du raster, distorsion «courbe» (6 pts minimum)
* Polynomiale de degré 3 : déformation du raster (10 pts minimum)
* Projective : déformation du raster (à utiliser lorsque la photographie présente un angle de fuite, 4 pts minimum)