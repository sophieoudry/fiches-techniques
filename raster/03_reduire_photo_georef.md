Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - équipe Formateurs & Référents SIG

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# Réduire en lot des photos géoréférencées

Lorsque vous avez à géoréférencer plusieurs images, cela peut vite générer un problème de stockage car les rasters géoréférencés sont lourds. Cette fiche vous explique comment réduire un lot d'images géoréférencées avec une perte de qualité minime.



### Menu Raster > Conversion > Convertir

Dans la fenêtre qui s'ouvre, en bas à gauche, cliquer sur *Exécuter comme processus de lot*

![processus de lot](images/red_georef_01.png) 



### Ajout des fichiers à géoréférencer

Dans la colonne *Couche en entrée*, choisir *Sélectionner des fichiers* et y ajouter les photos géoréférencées.

![sélection des fichiers](images/red_georef_02.png) 



### Choix du SCR 

Dans la deuxième colonne *Ecraser la projection du fichier en sortie*, cliquer sur le dessin du SCR et choisir la projection

![choix du SCR](images/red_georef_03.png) 



Reproduire ce choix pour les lignes suivantes en cliquant sur *Remplir* dans *Auto-remplissage*

![auto-remplissage](images/red_georef_04.png) 



**Option** : dans *Assigner une valeur nulle spécifique aux bandes*, assigner la valeur "0" pour ne pas avoir le cadre noir autour des photos géoréférencées. 



### Choix du mode de compression

Avec la molette de paramètres, ajouter des options supplémentaires. 

![mode avancé](images/red_georef_05.png) 

Dans *Options de création supplémentaires*, ajouter la ligne de commande “COMPRESS=DEFLATE”. Il s’agit de l’algorithme de compression sans perte. 

![compression](images/red_georef_06.png) 



### Choix du nom des fichiers réduits

Enfin, pour indiquer l’endroit et le nom des fichiers réduits, à droite de la colonne *Converti*, cliquer sur les trois points et fournir le chemin d’accès vers le dossier choisi et ajouter un préfixe pour les photos, par exemple “red_” pour réduit. 

Dans le menu déroulant *Auto-remplissage*, choisir les paramètres de remplissage automatique comme indiqué ci-dessus. 

![choix du nom du fichier](images/red_georef_07.png) 

Enfin, cliquer sur Exécuter et patienter.