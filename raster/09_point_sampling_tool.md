Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](/images/CC.png)  

Inrap - Camille Mangier-Merceron

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.22 de QGIS

# Joindre les attributs d'un raster à une couche de points 

Les données issues de calculs photogrammétriques peuvent être sous la forme d'une ortho-image et d'un modèle numérique de surface (MNS).
Les valeurs altimétriques que contient le MNS sont consultables avec l'outil ![Identifier](images\pst_01.JPG). 

En utilisant l'extension **Point Sampling Tool** ![Point Sampling Tool](images\pst_02.JPG), il est possible d'attribuer à une couche de points les valeurs d'une ou plusieurs bandes d'un ou plusieurs rasters.

## Création de la couche de points
La couche peut être créée de différentes manières en fonction des besoins scientifiques. 
* Manuellement avec la création d'une **nouvelle couche vecteur**![nouvelle couche vecteur ](images\pst_03.JPG) de géométrie **point** et en numérisant les points à un à un en fonction des besoins (cf. [dessiner dans Qgis](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/01_numerisation_1.html)) 
* Automatiquement avec les outils 
  * **_Traitement > Boîte à outils > Outils de géométrie vectorielle > Point dans la surface_** 
  
  ou  
  
  * **_Vecteur > Outils de Géométrie > Centroïdes_** 

Ces deux outils sont présentés dans la fiche technique [Analyse par maille](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/06_analyse_maille.html).



## Point Sampling Tool ![point sampling tool](images\pst_02.JPG)

L'extension est téléchargeable sur le dépôt des extensions de Qgis.
Pour savoir comment rajouter une extension, il existe une [fiche technique](https://formationsig.gitlab.io/fiches-techniques/priseenmain/05_extensions.html).

**_Extensions > Analyses > Point Sampling Tool_** ![point sampling tool](images\pst_02.JPG)

### Onglet Général

* Couche vecteur contenant les points : couche de points précédemment créée
* Couches avec les champs/bandes d'où extraire les valeurs : bande du MNS qui contient les valeurs altimétriques
* Couche de points en sortie : chemin et nom de la couche points en sortie

![Point sampling tool : onglet général](images\pst_04.JPG)



### Onglet Champs

* nom : renommer le nom du champ qui contiendra les valeurs de la bande 

![point sampling tool : onglet champs](images\pst_05.JPG)



Le résultat est une couche vecteur de points qui possède un champ dont les valeurs correspondent aux informations de la bande du raster.

![résultat table attributaire couche de points](images\pst_06.JPG)

![](images\pst_07.JPG)



## Jointure spatiale 

**_Vecteur > Outils de gestion de données > Joindre les attributs par localisation_**

Lien vers la [fiche technique](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/03_jointure_spatiale.html)

Afin d'attribuer aux points le numéro du vestige qu'ils intersectent.
![jointure spatiale](images\pst_08.jpg)

