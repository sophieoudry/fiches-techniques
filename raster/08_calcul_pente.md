Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - Emeline Le Goff, Frédéric Audouit

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# Générer un profil du terrain et calculer le pourcentage de pente 



## Emploi de l'extension Profile Tool dans QGIS

Une extension à installer via le **Menu Extension** (Installer/Gérer les extensions) permet de générer des profils de façon automatique, à partir d’un MNT (ou d’une orthophoto : tout document raster comportant des valeurs Z d’altitude sur chaque pixel) : extension **Profile Tool** (icône **Terrain profile**).



![](images\alti_20.jpg)



 La fenêtre s’ouvre (**onglet Profile**) et s’accroche par défaut en bas de l’espace de travail.

- Cliquer sur la couche du **MNT** (ici, le fichier **MNT_D117921**).

- Cliquer sur **Add Layer** pour importer l’image de fonds (MNT) dans l’espace de travail.

- Sélectionner **‘‘Temporary polyline’’ (ou “Selected polyline”** et cliquer sur le SHP de ligne si vous avez vectorisé au préalable la ligne de l’axe en 2D sur le plan).

- **Tracer l’axe** virtuel sur le canevas de la carte, à l’endroit où vous désirez obtenir le profil 

  {% hint style= 'info' %} faire attention au sens de création de la ligne ; ici : profil Nord-Sud, partir du haut vers le bas {% endhint %}

- **Le profil apparaît** sous la forme d’un graphique dans la fenêtre de l’extension.

- Vous pouvez **sauvegarder ce profil** (soit au format image, soit au format SVG -vectoriel- qui vous permettra d’affiner la mise en page ensuite dans un logiciel de DAO).

- Vous pouvez **exporter les points** qui ont servi à faire ce graphique, via l’onglet Table.



![](images\alti_21.jpg)



→ Dans l’**onglet Table** de l’outil Profile Tool, plusieurs options sont proposées :

- L’option **Copy to clipboard (with coordinates)** pourra vous permettre de réimporter les points dans un SIG puisqu’il comportera les coordonnées X-Y-Z mais aussi, les distances en XY de localisation de ces points >**cliquer sur cet outil, puis ouvrir un logiciel de tableur et coller**.

- L’option **Create Temporary layer** permet de générer un shape virtuel avec les points de l’axe. 

{% hint style='working' %} Vous pouvez ainsi conserver cette couche en l’exportant/sauvegarder-sous, avec la table attributaire des valeurs d’altitude (pour afficher les X-Y, utilisez la calculatrice de champs). {% endhint %}



![](images\alti_22.jpg)



## Calculer le pourcentage de pente

Le pourcentage de pente permet de décrire le relief en exprimant le **rapport entre la dénivellation et la distance horizontale** (mesure prise sur la carte). Par exemple, une pente de 3 % correspond à une dénivellation de 3 mètres sur une distance horizontale de 100 mètres.

{% hint style='danger' %} Attention : une pente de 100 % signifie que pour 100 m à l'horizontale on progresse de 100 m en vertical, ce qui correspond donc à un angle moyen de 45° (et non 90°). {% endhint %}

[http://www.toujourspret.com/techniques/orientation/topographie/calcul_du_pourcentage_d%27une_pente.php](http://www.toujourspret.com/techniques/orientation/topographie/calcul_du_pourcentage_d'une_pente.php)

 

Sur les cartes, nous avons la distance à plat, c'est-à-dire la distance horizontale ; elle ne prend pas en considération le relief du terrain. L’indication de la pente est donc importante pour la prise en compte sur le terrain (lors de la prescription de fouille), de la localisation par exemple de la base vie ou de l’implantation de drains.

Pour calculer la pente d'un trajet, il suffit d’afficher la longueur du profil, puis de repérer les valeurs d’altitude haute et basse (via la table attributaire), et d’appliquer la formule suivante :

**Pente (%) = Dénivelé (hauteur entre le point d’arrivée et le point de départ) / Longueur (m)**

