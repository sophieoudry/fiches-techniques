Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - équipe Formateurs & Référents SIG

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# Créer un couche de points à partir d'un fichier .csv



Outil Ajouter une couche de texte délimité ![icone outil](images/csv_01.png)

(si vous ne l'avez pas, cet outil est dans la Barre d'outils "Gestion des couches")



![fenêtre import csv](images/csv_02.png)

1. Sélectionner le fichier .csv à importer. Modifier l'encodage si nécessaire
2. Choisir le délimiteur à utiliser : le plus souvent, ce sera virgule ou point-virgule. Le tableau en bas de la fenêtre permet de visualiser le résultat
3. Cocher si votre 1ere ligne correspond à l'entête du champ
4. Choisir de n'importer qu'une table sans géométrie (simple tableau pour faire un jointure) ou une couche de point avec coordonnées X et Y. Dans ce cas, penser à indiquer quel champ correspond aux coordonnées X et quel champ correspond aux coordonnées Y.



Prenez le temps de lire la fenêtre de haut en bas et de gauche à droite pour remplir les rubriques !

Attention, le fichier qui apparaît dans le panneau des Couches n'est pas une couche vecteur, seulement un import du fichier csv ! Vous pouvez alors si besoin l'enregistrer en format vectoriel :

- clic droit sur la couche *Exporter > Sauvegarder les entités sous*