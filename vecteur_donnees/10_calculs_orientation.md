Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - Emeline Le Goff, Sylvain Badey

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# Les calculs d'orientation

Pour toutes les explications de fond nécessaires, allez voir [l'article de Sylvain Badey consacré à ce sujet](https://archeomatic.wordpress.com/2014/12/05/whitebox-gat-3-2-qgis-2-x-automatiser-le-calcul-de-longueur-et-dorientation-dun-polygone/). 

QGIS ne permet pas pour l’instant de calculer des orientations significatives : mais point d’affolement, notre collègue Sly a trouvé la solution via le programme WhiteboxGAT.

- Téléchargez et ouvrez Whitebox : http://www.uoguelph.ca/~hydrogeo/Whitebox/download.shtml
- Dans le dossier installé (il n’y a pas de raccourci), cliquer directement sur l’extension Whitebox.bat



## 1. Utiliser Whitebox

### 1.1. Créer l'emprise minimale englobant chaque entité

Avec l’outil **MBB (Minimum Bounding Box)**, à partir de la couche vecteur de polygones (poly.shp), créer l’emprise orientée minimale de chaque fait (poly_orient.shp). Cette couche comportera les informations de largeur et de longueur (2 champs comportant ces valeurs vont être créés), et servira de support au calcul de l’axe médian de chaque fait, nécessaire au calcul d’orientation.
- En entrée, allez chercher la couche poly.shp
- En sortie, nommez votre future couche poly_MBB.shp

![image 1](images/orient_01.jpg)

 

### 1.2. Créer la médiatrice des polygones et en calculer l'orientation par rapport au Nord

Nous allons à présent créer la ligne médiatrice de chaque polygone - ou axe selon le plus grand côté. Cette couche comportera les informations de longueur de l’axe et d’orientation (exprimées entre 0° et 180°).
- En entrée, choisissez poly_MBB.shp
- En sortie, nommez votre future couche de type poly_orient_axe.shp

![image 2](images/orient_02.jpg)



## 2. Lier les couches d'axe et de polygone pour récupérer les valeurs angulaires

### 2.1. Créer un champ d'indexation

Dans QGIS, importer la couche poly_orient_axe.shp et ouvrir la table attributaire. Vous constatez qu’il n’y a pas les champs d’origine de votre couche polygone, mais qu’il existe tout de même un champ PARENT_ID avec des numéros uniques de 1 à n.

Créer un nouveau **champ d’indexation** "Parent_ID" au moyen de la calculatrice de champs et de la **fonction @row_number** pour reprendre l’indexation de la couche d’origine (poly_orient) pour générer les identifiants uniques correspondant à l’indexation de la couche vecteur.

![fig 3](images/orient_03.jpg)



### 2.2. Récupérer les identifiants des faits

Pour faire le lien et récupérer les données angulaires calculées, faire une [**jointure attributaire**](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/02_jointure_att.html) avec poly_orient.shp pour récupérer les identifiants des faits.

Pour le sens de la jointure, à voir si vous préférez travailler ensuite sur la couche des axes ou sur celle des polygones : si vous voulez conserver les polygones, faites la jointure sur poly_orient ; si vous voulez conserver les axes, faites la jointure sur poly_orient_axe.

![fig4](images/orient_04.jpg)





**Pérenniser la jointure** (clic-droit : *Exporter > Sauvegarder les entités sous* fait_orient.shp)  

![Fig5](images/orient_05.jpg)



Vous obtenez ainsi votre couche vecteur complétée des informations angulaires entre 0 et 180°.

![Fig6](images/orient_06.jpg)



## 3. Exprimer les valeurs d'angles sous d'autres formes

Dans la calculatrice de champs, isoler en premier lieu, les entités liées aux murs et fossés.

Les valeurs issues de WhiteBox (champ "ORIENT") sont exprimées entre 0° (plein nord), 90° (plein est) et 180° (plein sud).

Au moyen de la calculatrice de champs, il est possible, selon les besoins, d'**exprimer ces informations angulaires sous d'autres formes** (peuvent être complémentaires) :

1. Raisonnement à l’échelle des US / faits ("ORIENT" : angles de 1° à 180°)

2. Raisonnement à l’échelle des US / faits ("ORIENT 90" : angles de -90° à +90°)

3. Raisonnement à l’échelle des ensembles / réseaux ("RESEAU NL" -nord Lambert- : angles exprimés entre -45° et +45° pour repérer les murs perpendiculaires et réseaux orthogonaux).



![Fig7](images/orient_07.png)





### 3.1. Raisonnement à l’échelle des US / faits : ‘‘ORIENT’’ - angles de 1° à 180°



Les valeurs angulaires sont énoncées de 1° à 180° (champ existant "ORIENT").

On peut créer un champ (""**SENS**") qui fait référence aux orientations Nord-Est Sud-Ouest (1° à 45°) ou Nord-Ouest Sud-Est (45° à 180°).

Dans QGIS, on conserve le champ "ORIENT". On va **créer un champ complémentaire** "**SENS**" sur la couche poly_orientation.shp : utiliser la calculatrice de champs permettant la saisie automatique de valeurs via l’expression **CASE** (dans le cas) **WHEN** (où : noter la condition en SQL) **THEN** (alors : noter la valeur à saisir) **END** (fin de la syntaxe SQL) :

```sql
CASE
WHEN "ORIENT" >= 0 AND "ORIENT"<= 90 THEN 'orienté NE-SO'
WHEN "ORIENT" > 90 AND "ORIENT" <= 180 THEN 'orienté NO-SE'
END


```

 ![Fig8](images/orient_08.jpg) 









![Fig9](images/orient_09.jpg)






### 3.2. Raisonnement à l’échelle des US / faits : ‘‘ORIENT 90’’ - angles de -90° à +90°

Les valeurs angulaires de 1° à 180° (champ "ORIENT") sont recalculées pour exprimer des angles entre -90° (plein ouest) et +90° (plein est) dans un nouveau champ ‘‘**ORIENT 90**’’.

Comme précédemment, un champ ‘‘**SENS 90**’’ donnera les grandes orientations Nord Nord-Ouest (-90° à 0°) ou Nord Nord-Est (0° à +90°).

Dans QGIS, on va **créer un champ** ‘‘**ORIENT 90**’’ sur la couche poly_orientation.shp au moyen de la calculatrice de champs pour rapporter les valeurs angulaires à des valeurs comprises entre 90° O et 90° Est. Saisir l’expression suivante : 

```sql
CASE
WHEN "ORIENT" <=90 THEN "ORIENT"
WHEN "ORIENT" > 90 AND "ORIENT" <= 180 THEN "ORIENT"- 18
END
```

![Fig10](images/orient_10.jpg)





Au moyen de la calculatrice de champs, on peut créer un champ complémentaire "SENS 90" :

```sql
CASE
WHEN "ORIENT 90" >= 0 AND "ORIENT 90"<= 90 THEN 'N-NE'
WHEN "ORIENT 90" <= 0 AND "ORIENT 90" >= -90 THEN 'N-NO'
END
```

![Fig11](images/orient_11.jpg)






### 3.3. Raisonnement à l’échelle des ensembles / réseaux : "RESEAU NL" - angles de -45° à +45° 

Pour faciliter l'identification de murs perpendiculaires et/ou la réflexion sur des réseaux orthogonaux (parcellaires par exemple), on peut exprimer les angles entre 45° est (+45) et 45° ouest (-45). Les valeurs angulaires de 1° à 180° (champ "ORIENT") sont alors recalculées pour exprimer des angles entre -45° et 45° (champ "RESEAU NL") pour une expression en Nord Lambert des réseaux d’orientation et ainsi repérer, soit en statistique au moyen de graphes (logiciels R ou GRASS), soit en cartographie au moyen de classes de valeurs (recours aux ensembles de règles pour la symbologie), les orientations Est ou Ouest du réseau auquel sont rattachées des structures perpendiculaires entre elles et qui peuvent fonctionner ensemble.

Dans QGIS, on va **créer un champ** **"RESEAU NL"** sur la couche poly_orientation.shp au moyen de la calculatrice de champs pour rapporter les valeurs angulaires de "ORIENT" à des valeurs comprises entre 45° ouest (-45) et 45° est (45). Cliquer l’expression suivante : 

```sql
CASE
WHEN "ORIENT" <=45 THEN "ORIENT" 
WHEN "ORIENT" > 45 AND "ORIENT" <= 135 THEN "ORIENT"- 90 
WHEN "ORIENT" > 135 THEN "ORIENT"-180
END
```

![Fig12](images/orient_12.jpg)



![Fig13](images/orient_13.jpg)






## 4. Créer des classes de valeurs

Selon votre problématique de recherche (orientations parcellaires notamment), vous pouvez **créer des classes en regroupant des valeurs** pour faire des statistiques ou des moyennes quant aux orientations (par exemple, regrouper les angles par tranche de 5° ou par tranche de 10°). Le but est de repérer les entités qui fonctionnent ensemble (orthogonaux) et appartiennent donc à un réseau.

![Fig14](images/orient_14.jpg)

 

Pour cela, il existe 2 possibilités ou méthodes :

### 4.1. Via la symbologie

Le plus adéquat par rapport à la sémiologie (#Stage Statistique) est de faire des Ensembles de règles . Mais comme il y a plusieurs classes et que cela peut être fastidieux de tout noter, voilà un petit conseil, passer par "Gradué" :

- **Symbologie > Gradué** : faire des classes selon le mode *Jolies Ruptures*. Il est à noter que QGIS derrière a développé des règles SQL en fonction des classes (une règle par classe). Ici pour les classes du mode Gradué, à vous de jouer sur le nombre de classes pour en générer en fonction des classes souhaitées - ici, toutes les 5 valeurs -) ;




![Fig15](images/orient_15.jpg) 




- Allez ensuite sur **Symbologie > Ensembles de Règles**, pour récupérer les classes traduites en SQL (une règle par classe). Cette démarche est plus facile, et évite des erreurs de frappe.

![Fig16](images/orient_16.jpg) 

- Modifier les légendes  
- Mettez les couleurs que vous voulez (couleurs similaires par pas de 5° E/O)
- Vous n’avez plus qu’à aller dans *Style > Enregistrer le style* pour pouvoir récupérer ce style sur chaque jeu de données à venir - !! : toujours nommer le champ "RESEAU NL").



### 4.2. Via la calculatrice de champ

Comme précédemment, on peut recourir à la calculatrice de champs pour figer les résultats. Au final, le raisonnement est le même car pour réaliser des ensembles de règles, il faut rédiger du SQL et vous pourrez conserver vos symbologies : à vous de voir si vous désirez conserver ou non les valeurs dans un champ (plus pratique si statistiques ultérieures, et lien avec d’autres logiciels).

- On crée un nouveau champ "**CLASSE5NL**" (si tous les 5°) - que l’on ira catégoriser par la suite dans l’onglet Symbologie. Conseil : copier-coller l’expression SQL page ci-après.




![Fig17](images/orient_17.jpg) 

```sql
CASE 
WHEN "RESEAU NL" >= -45 AND "RESEAU NL" <= -40 THEN 'NL 40°-45° O'
WHEN "RESEAU NL" > -40 AND "RESEAU NL" <= -35 THEN 'NL 35°-40° O'
WHEN "RESEAU NL" > -35 AND "RESEAU NL" <= -30 THEN 'NL 30°-35° O'
WHEN "RESEAU NL" > -30 AND "RESEAU NL" <= -25 THEN 'NL 25°-30° O'
WHEN "RESEAU NL" > -25 AND "RESEAU NL" <= -20 THEN 'NL 20°-25° O'
WHEN "RESEAU NL" > -20 AND "RESEAU NL" <= -15 THEN 'NL 15°-20° O'
WHEN "RESEAU NL" > -15 AND "RESEAU NL" <= -10 THEN 'NL 10°-15° O'
WHEN "RESEAU NL" > -10 AND "RESEAU NL" <= -5 THEN 'NL 5°-10° O'
WHEN "RESEAU NL" > -5 AND "RESEAU NL" <= 0 THEN 'NL 0°-5° O'
WHEN "RESEAU NL" > 0 AND "RESEAU NL" <= 5 THEN 'NL 0°-5° O'
WHEN "RESEAU NL" > 5 AND "RESEAU NL" <= 10 THEN 'NL 5°-10° E'
WHEN "RESEAU NL" > 10 AND "RESEAU NL" <= 15 THEN 'NL 10°-15° E'
WHEN "RESEAU NL" > 15 AND "RESEAU NL" <= 20 THEN 'NL 15°-20° E'
WHEN "RESEAU NL" > 20 AND "RESEAU NL" <= 25 THEN 'NL 20°-25° E'
WHEN "RESEAU NL" > 25 AND "RESEAU NL" <= 30 THEN 'NL 25°-30° E'
WHEN "RESEAU NL" > 30 AND "RESEAU NL" <= 35 THEN 'NL 30°-35° E'
WHEN "RESEAU NL" > 35 AND "RESEAU NL" <= 40 THEN 'NL 35°-40° E'
WHEN "RESEAU NL" > 40 AND "RESEAU NL" <= 45 THEN 'NL 40°-45° E'
END
```






- Aller ensuite sur **Symbologie > Catégorisé**, sur le nouveau champ "**CLASSE5NL**" (vous n’aurez alors que les catégories concernées par votre jeu de données – certaines « classes » // catégories d’orientation ne seront donc pas présentes, si non concernées par votre site) :

![Fig18](images/orient_18.jpg) 



## 5. Conclusion

Ainsi, toutes ces étapes sont donc complémentaires, et peuvent permettre de travailler *a minima* sur **les orientations individuelles des faits (murs/fossés >> cette manipulation peut être également appliquée à l’orientation de sépultures, tranchées, …)**. Egalement, les dernières opérations permettent de réaliser des cartographies automatiques pour créer des ensembles selon les orientations perpendiculaires (en tenant compte des angles par rapport au Nord Lambert).

![Fig19](images/orient_19.jpg) 