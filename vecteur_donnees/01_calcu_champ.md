1. Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - équipe Formateurs & Référents SIG

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# La calculatrice de champ ![calculatrice de champ](images/calcu_champ_01.png) 


Basé sur le Constructeur d’expression et donc sur le langage SQL, la Calculatrice de champs permet d’automatiser la saisie (de valeurs ou de calculs) dans les champs d’une table attributaire. 

On peut : 

- Mettre à jour un champ existant (donc faire de la saisie dans un champ déjà créé) 
- Créer et alimenter un nouveau champ. A la différence de l'outil *Ajouter un nouveau champ*, ![créer un nouveau champ](images\calcu_champ_02.png) la calculatrice de champs ne créera un champ que si on lui indique une valeur ou le résultat d’un calcul.

Dès que l’on effectue un calcul, le mode Edition s’active (normal puisqu’il a complété la
table attributaire). Pensez donc à enregistrer les modifications (après vérification) .

La Calculatrice fonctionne sur le modèle SQL (sujet / verbe / complément) 

**‘‘champ’’ Operateur ‘valeur’**
cependant, contrairement à la fenêtre d‘expression, l’opérateur est inclus dans l’usage de l’outil.



## 1. Saisir du texte ou des valeurs numériques à la main

![calculatrice de champ, manuel](images\calcu_champ_03.png) 

Dans l'exemple ci-dessus, on a sélectionné trois entités qu'on souhaite modifier

- Cocher "Ne mettre à jour que les 3 entités sélectionnées"
- Cocher "Mise à jour d'un champ existant"
- Choisir le champ en question dans le menu déroulant
- Dans la console à gauche, saisir directement le texte avec des guillemets simples (apostrophes de la touche 4)



## 2. Utiliser les guides pour les valeurs 

Pour faciliter la saisie, des modules peuvent vous guider (déployer les menus déroulants).
On peut aller chercher le nom des champs et des valeurs déjà saisies dans « Champs et Valeurs ».

Pour faire apparaitre les valeurs saisies dans un champ, cliquer sur le nom du champ puis dans
l’interface de droite (déployer la fenêtre au besoin),  cliquer sur le bouton «**Tous uniques**» en bas pour
faire apparaître les valeurs qui sont saisies au moins une fois dans le champ choisi.

![calculatrice de champs, valeurs uniques](images\calcu_champ_04.png) 



## 3. Changer le format d'un champ

Si dans votre table des nombres apparaissent dans un champ en format texte, vous pouvez créer une copie de ce champ en format numérique et ainsi accéder aux outils de calcul.

- créer un nouveau champ
- choisir le format et la longueur du champ
- ajouter le nom du champ à copier

![calculatrice de champ changement de format](images/calcu_champ_05.png)	

Dans l'image ci-dessus, le champ "_long" était en format texte comme l'indique l'icone "abc" devant le nom. 



## 4. Utiliser les outils de calcul intégrés 

On peut utiliser des fonctions d’automatisation de calcul proposées par la calculatrice (Menus Géométrie -surface, périmètre, longueur, coordonnées - ou Enregistrement, ...)

![calculatrice de champ calcul](images/calcu_champ_06.png) 

:bangbang:  Ne vous inquiétez pas si l'Aperçu du résultat en bas de la fenêtre ne respecte pas le format proposé (exemple ci-dessus avec une seule décimale demandée). L'essentiel est que l'aperçu ne soit pas en rouge.