Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - équipe Formateurs & Référents SIG

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# La jointure spatiale

Permet de joindre les données d’un vecteur à un autre lorsque ceux-ci se superposent (couche axes et couche faits par exemple)



On peut utiliser deux outils selon l'information que l'on souhaite récupérer : 

- soit les données simples
- soit les données avec des calculs ou des statistiques possibles (moyenne, médiane, etc.)



## Cas 1 : récupérer les données simples

Exemple : on voudrait attribuer aux axes le numéro du vestige qu'ils coupent. 

*Vecteur > Outils de gestion de données > Joindre les attributs par localisation*

- Couche source : axe
- Joindre la couche : vestiges
- Champs à ajouter (cliquer à droite sur la boîte à 3 points) : numéro de vestige
- Type de jointure : prendre uniquement les attributs de la première entité localisée

   :heavy_exclamation_mark:   la couche obtenue est temporaire et se nomme *Couche issue de la jointure spatiale*



![joindre les attributs par localisation](images\joint_spat_01.png) 



## Cas 2 : obtenir des statistiques simples sur plusieurs champs

Exemple : on voudrait récupérer l'altitude moyenne des axes de coupe

*Traitement > Boite à outils > Joindre les attributs par localisation (résumé)*

- Couche source : axe
- Joindre la couche : points topo
- Prédicat géométrique : "intersecte"
- Champs à résumer : ne sélectionne que le champ avec l'information d'altitude
- Résumés à calculer : ici, ne sélectionner que la moyenne

  :heavy_exclamation_mark:  la couche obtenue est temporaire et se nomme *Couche issue de la jointure spatiale*

![joindre les attributs par localisation - résumé](images\joint_spat_02.png) 

