Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - équipe Formateurs & Référents SIG

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# La requête spatiale

La requête spatiale (ou "Sélection par localisation") permet de sélectionner des entités d'une couche par rapport à ceux d'une autre couche en fonction d'opérateurs topologiques. 



Attention, les requêtes spatiales nécessitent des fichiers aux géométries "propres" : pas de nœuds en doublon, pas d'intersection de segment, etc. Pour cela, veillez à [**vérifier vos géométries**](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/04_verif_geom.html) au préalable, sinon vous risquez de ne pas obtenir de résultat ou de faire planter le logiciel.



## Quelques opérateurs topologiques

Il en existe beaucoup plus, voici les principaux.

Soient deux couches vectorielles, A et B 

* **Intersecte** : les objets sélectionnés de la couche A ont au moins un point commun avec le polygone de la couche B

* **Est disjoint** : les objets sélectionnés de la couche A n'ont aucun point commun avec le polygone de la couche B

* **A l'intérieur de** : les objets sélectionnés de la couche A sont contenus entièrement dans le polygone de la couche B

- **Chevauche** : les objets sélectionnés de la couche A sont partiellement contenus dans le polygone de la couche B

![opérateurs de requête spatiale](images/req_spatiale_01.png) 

d'après [http://www.geoinformations.developpement-durable.gouv.fr](http://www.geoinformations.developpement-durable.gouv.fr/fichier/pdf/m07_selectionsrequetes_papier_cle0c371c.pdf?arg=177831194&cle=e9e63f16fecf491cf273d81cd08d8ae8e7f1823d&file=pdf%2Fm07_selectionsrequetes_papier_cle0c371c.pdf)



## Effectuer la requête

*Vecteur > Outils de recherche > Sélection par localisation*

Exemple : Quels sont les vestiges vus au diagnostic qui sont compris dans l'emprise de la fouille ?

![sélection par localisation](images/req_spatiale_02.png) 

- **1** : choisir la couche où sera effectuée la localisation, ici les vestiges du diagnostic
- **2** : choisir un ou plusieurs opérateurs topologiques
- **3** : choisir la couche de comparaison, ici l'emprise de la fouille
- **4** : choisir le type de sélection. Par défaut, l'outil créera une nouvelle sélection. Il est aussi possible d'ajouter les entités sélectionnées à une sélection en cours ou encore de les ôter d'une sélection en cours.
- Terminer par **Exécuter**



