Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - équipe Formateurs & Référents SIG

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# La jointure attributaire

Opération consistant à joindre deux tables grâce à un identifiant commun - relation 1 à 1

{% hint style='info' %} On peut joindre un tableau à une couche vecteur ou inversement, ou encore les tables de deux couches vecteurs. {% endhint %}



- Ouvrir le tableau dans un tableur (OpenOffice Calc, LibreOffice Calc, MS Excel), le nettoyer (longueur des champs, caractères, espaces, cellules fusionnées...) et identifier la colonne servant d'identifiant unique.
- Dans QGIS, ajouter le tableur (un cliquer-glisser dans la carte suffit)
- Ouvrir la table attributaire de ce tableau pour vérifier l’encodage, la distribution dans les colonnes
- Clic droit sur le vecteur sur lequel on veut faire la jointure > *Propriétés > Jointure >* ![+](images/iconplus.png) cliquer sur le signe plus
  - Joindre la couche : choisir le tableau
  - Champ de jointure et champ dans la couche cible : identifiant unique identique dans chaque couche
- Choisir éventuellement les champs à joindre et **modifier le préfixe**
- Valider



![jointure attributaire](images\joint_att_01.png) 

:heavy_exclamation_mark:  si une table est jointe à un vecteur, on peut l’interroger, l’afficher, mais on ne
peut pas faire de calcul sur celle-ci. Il faudra alors créer un nouveau vecteur :

- Clic droit sur la couche
- *Exporter > Sauvegarder les entités sous*



**Note** : la table à importer doit être propre mais pas nécessairement «belle»