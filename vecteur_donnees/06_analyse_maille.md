Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - équipe Formateurs & Référents SIG

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# Analyse par maille ou Carte de densité



Si vos données sont contenues dans une couche de points, vous devez commencer par créer une grille. 

## Créer une grille

***Boite à outils de traitements > Création de vecteurs > Créer une grille***

* Type de grille : rectangle (polygone)
* Etendue de la grille : cliquer sur la boîte à trois points à droite la couche qui servira d'étendue. Ici, il s'agit de la couche d'emprise de l'opération
* Espacement horizontal : 10 mètres
* Espacement vertical : 10 mètres
* Après exécution, une couche temporaire *Grille* est créée

![algorithme création grille](images/ana_maille_01.png) 



Notre grille est créée, nous souhaitons maintenant connaître le nombre de points dans chacun des carrés. 

## Compter les points dans la grille

***Vecteur > Outils d'analyse > Compter les points dans les polygones***

* Polygones : Grille
* Points : F111688_point (notre couche de points)
* Champ de décompte : NB_POINTS ; par défaut, il s'appelle "NUMPOINTS", ce qui peut induire en erreur
* Après exécution, une couche temporaire *Compte* est créée

![compter les points dans le polygone](images/ana_maille_02.png)



* Calculer la densité au m² des points (nb de points / surface du carré)

  * Dans la couche "Compte", calculatrice de champ : créer un champ "Freq", de type Nombre décimal, de longueur 5 et de précision 3

  ```sql
   "NB_POINTS"  / '100'
  ```

  > Nous avons divisé par 100 car la surface des carrés utilisés est de 100 m² (10 m x 10 m)

* Cartographier le résultat

  * *Propriétés > Symbologie > Gradué* avec la valeur "Freq"
  * Classer et utiliser le mode de discrétisation "Ruptures naturelles (Jenks)". Si besoin, mettre en blanc pour la valeur 0



## Transformer une couche polygone en une couche de points

Dans l'exemple précédent, nos données étaient déjà dans une couche de points, ce qui est le type de géométrie nécessaire pour l'outil "Compter les points dans un polygone". Il arrive que vos données soient stockées dans une couche de polygones, par exemple : le poids de mobilier dans chaque vestige. Il faut alors transformer cette couche polygone en une couche de points.

***Vecteur > Outils de Géométrie > Centroïdes***

* Couche source :  votre couche polygone existante
* Centroïdes : la couche de points qui sera créée



![centroïdes](images/ana_maille_03.png) 



**Attention**, cet outil renvoie le "vrai" centroïde du polygone, ici, le barycentre. Cela signifie que pour certains vestiges, comme des fossés curvilignes, le centroïde peut être situé à l'extérieur du vestige. 

Si vous souhaitez que le point soit à l'intérieur du polygone, il faut utiliser l'algorithme "Point dans la surface". Le point sera alors bien dans le vestige, mais il ne s'agit pas d'un vrai centroïde. 

***Traitement > Boîte à outils > Outils de géométrie vectorielle > Point dans la surface***

![point dans la surface](images/ana_maille_04.png) 