Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - équipe Formateurs & Référents SIG

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# La requête attributaire

Permet de sélectionner des entités selon leurs attributs. La requête est réalisée sur *une seule* couche et sur *un ou plusieurs* attributs. 

:heavy_exclamation_mark:  les sélections peuvent s'ajouter les unes aux autres, pensez à désélectionner les entités si nécessaire.



Pour les requêtes, on peut passer 

* soit par l'outil de sélection par expression si l'on souhaite une sélection à l'issue de la question ![outil sélection par expression](images/req_att_01.png) 
* soit par le filtre avancé ![filtre avancé](images/req_att_02.png) 



## Présentation de la fenêtre de sélection par expression

**Attention !** il ne faut pas confondre cette fenêtre avec celle de la calculatrice de champ ![calculatrice de champ](images/calcu_champ_01.png)qui est très proche.

![fenêtre d'expression](images/req_att_03.png)		 

**1** : éditeur d'expression

**2** : liste des fonctions, variables et champs. Ne pas hésiter à utiliser la barre de recherche en haut. Avec un double-clic, vous ajoutez l'élément dans le panneau de gauche (éditeur d'expression)

**3** : aide contextuelle



## Poser une question simple

:bulb: [fiche technique : les opérateurs SQL](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/08_operateurs_sql.html)

Exemple : "je voudrais voir les vestiges qui sont des fossés parcellaires"

La question doit être posée sous la forme :

```sql
"champ" operateur de comparaison 'valeur'
```

![requête simple](images/req_att_04.png)	

La liste des **champs** de la table attributaire est accessible dans le panneau central, dans le menu déroulant "Champs et valeurs" (4) ; la liste des valeurs de ce champ apparaît à droite quand on clique sur "tous uniques" (5)

Notez que QGIS reconnaît la structure de votre question et la restitue via la **colorimétrie syntaxique (6)**. Les champs sont en jaune avec des guillemets - les opérateurs sont en violet et en gras - les valeurs sont en vert avec des apostrophes

En bas à gauche, la fenêtre propose un **aperçu** du résultat (7). Attention, ce n'est pas toujours ce à quoi vous vous attendez, comme ici. Toutefois, retenez que tant que ce n'est pas écrit en rouge, la syntaxe est correcte. Cela veut dire que le logiciel comprend la question posée - ce qui ne veut pas dire que la question est bonne.. 



## Poser une question plus élaborée

Exemple : quels sont les fossés parcellaires attribués à l'Antiquité ? 

```sql
 "interpret" LIKE  'fossé parcellaire' 
 AND
  "_chrono" LIKE  'Antiquité' 
```



### Les opérateurs logiques

`OR` pour séparer deux conditions dont au moins une doit être vérifiée (condition 1 ou condition 2 est vraie)
`AND` pour séparer deux conditions qui doivent être vérifiées simultanément (condition 1 et condition 2 sont vraies)
`NOT` permet d'inverser une condition.  

![shéma AND OR NOT](images/and_or_not.png) 



### Les caractères joker

Avec les opérateurs LIKE et ILIKE, il est possible d'utiliser des caractères remplaçant d'autres. C'est utile si vous avez un doute sur la façon dont un mot a été saisi (exemple : "céramique", "ceramique") ou si la valeur que vous cherchez n'est pas unique dans le champ (exemple : "céramique", "TCA, céramique").

- % remplace n'importe quel nombre de caractères

- _ remplace un seul caractère



Les fossés parcellaires ayant livré au moins de la céramique : 

```sql
"interpret" LIKE 'fossé parcellaire'
AND
"mobilier" LIKE '%céramique%'
```

Cette requête renverra les entités répondant par exemple à "céramique", "TCA, céramique", "céramique, faune". En enlevant les %, on aurait les entités n'ayant livré que de la céramique et rien d'autre.

Pour être sûr de l'orthographe utilisée, saisir "_" à la place du caractère accentué :

```sqlite
"interpret" LIKE 'fossé parcellaire'
AND
"mobilier" LIKE '%c_ramique%'
```



## Récupérer la sélection

La requête crée une sélection que vous pouvez visualiser sur le canevas de la carte. Si vous souhaitez utiliser cette sélection, vous pouvez en créer une copie : clic droit sur la couche où a été effectuée la sélection - Exporter des entités - Sauvegarder les entités sélectionnées sous



![sauvegarde de la sélection](images/req_att_05.png)



Vous avez accès aux requêtes précédentes dans le menu "Recent" dans le panneau central de la fenêtre de sélection. Cela vous évite d'avoir à saisir de nouveau une requête.

![requêtes récentes](images/req_att_06.png) 

{% hint style='danger' %} Ces requêtes mémorisées le sont dans le cadre du projet QGIS. Si vous commencez à avoir des requêtes relativement élaborées ou si vous souhaitez pouvoir les réutiliser dans un autre projet, pensez à les copier-coller dans un éditeur de texte. {% endhint %}

Pour aller plus loin : [fiche mémo-sql](https://formationsig.gitlab.io/sig32/memoSQL/memoSQL.html)



