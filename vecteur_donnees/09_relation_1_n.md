Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - équipe Formateurs & Référents SIG

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# Les relations de 1 à n

Voir [ce lien](http://cerig.pagora.grenoble-inp.fr/tutoriel/bases-de-donnees/chap06.htm) pour aller plus dans le détail sur le schéma relationnel de base.



## Les types de relation 

Faire une relation attributaire c’est incrémenter une table attributaire avec de nouveaux champs (en vue de mettre à jour et d’effectuer les analyses statistiques ou des cartographies thématiques). Le but est d’enrichir la table attributaire d’une couche vectorielle au moyen de champs d’un tableur d’inventaire (ou bases de données). Il existe des liens de plusieurs sortes. 

- **relation de 1 à 1** : comme l'inventaire des faits et le tableau de NR de céramique par fait (un fait est lié à un NR)
- **relation de 1 à plusieurs (1 à n)** : comme l'inventaire des faits et l'inventaire des US (chaque fait peut contenir plusieurs US)
- **relation de plusieurs à plusieurs (n à n)** : comme l'inventaire des faits et l'inventaire des photos (chaque fait a été photographié plusieurs fois et chaque photo peut représenter plusieurs faits)

![relations dans la table](images/relation_1_n_01.png) 





## Le lien de 1 à 1

Pour faire une relation - relier deux tables entre elles - on procède à une **jointure** (on joint une table à une autre au moyen de champs communs) : il faut donc un champ commun entre la table attributaire d’un vecteur et le tableur d’inventaire.

On parle de [**jointure attributaire**](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/02_jointure_att.html) pour des liens de 1 à 1 >> les valeurs d’un champ dans la table attributaire d’un vecteur sont les mêmes que celles contenues dans un champ d’un tableur (ou table d’une base de données). Généralement, il s’agit du champ comportant l’identifiant unique. Ex. : mettre à jour le descriptif des faits topographiés >> **1 fait** topographié = **1 fait** décrit dans le tableur d’inventaire. 

![relation de 1 à 1](images/relation_1_n_02.jpg) 

Dans ce cas, il y a une **correspondance unique** entre une valeur présente dans un enregistrement de la table attributaire et celle présente dans une ligne du tableur à joindre.

Dans les liens de 1 à 1, les champs communs peuvent ne pas être nommés de la même façon, ce n'est pas un problème pour faire la [jointure attributaire](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/02_jointure_att.html). 



## Le lien de 1 à n 

On parle de *relation attributaire* pour des liens de 1 à n. Les valeurs d’un champ dans la table attributaire d’un vecteur correspondent à plusieurs valeurs d’un des champs du tableur (ou table d’une base de données) à joindre. 

Généralement, il s’agit du champ comportant l’identifiant unique dans la couche vectorielle, et d’un champ comportant la clef secondaire dans le tableur. 

> Ex. : cartographier le mobilier retrouvé dans chaque fait topographié >> **1 fait** topographié (la clef primaire est le numéro du fait) = **n mobilier** associé à chaque fait dans le tableur d’inventaire (la clef secondaire est le numéro du fait ; la clef primaire du tableur du mobilier étant le numéro unique d’inventaire de chaque mobilier). 



![relation 1 à n](images/relation_1_n_03.jpg) 



Dans ce cas, il y a une **correspondance multiple** entre une valeur présente dans un enregistrement de la table attributaire et celles présentes dans des lignes du tableur à joindre.

{% hint style='info' %} Il existe plusieurs façons de faire des relations de 1 à n, celle qui vous est présentée ici fait appel aux couches virtuelles et créera une nouvelle couche comportant les éléments des deux premières. 

Si vous souhaitez seulement afficher les relations sans obtenir les résultats dans une nouvelle couche, il est préférable d'[établir des relations de référence](https://docs.qgis.org/3.10/fr/docs/user_manual/working_with_vector/attribute_table.html#introducing-1-n-relations) dans les propriétés des couches en question.{% endhint %}

Dans les liens de 1 à n, les champs communs doivent être nommés différemment pour que la relation se fasse. Effectivement, nous allons alors avoir recours à du langage SQL : le SQL sera intuitif pour nous guider, et détectera le nom des couches et celui des champs à joindre. Aussi, pour effectuer la relation correctement et bien paramétrer la relation, il faut que les noms soient différents.

## Faire une relation de 1 à n

Pour créer la relation et avoir accès à une couche (entités géométriques + enregistrements dans la table attributaire), donc pour pouvoir intervenir sur les valeurs (analyses, statistiques, cartographie), nous n’allons pas faire une jointure attributaire à partir du vecteur (sinon nous perdrons les correspondances multiples), mais nous allons créer une couche virtuelle au moyen du SQL. Nous pourrons *in fine* pérenniser cette relation en sauvegardant-sous la couche virtuelle en format vectoriel.

*Couche > Ajouter une couche > Ajouter une couche virtuelle*

![ajouter une couche vitruelle](images/relation_1_n_04.jpg) 



*Importer > Choisissez les couches à mettre en correspondance*, validez.

![sélection des couches à joindre](images/relation_1_n_05.jpg) 



Tapez ensuite dans la fenêtre de Requête, la requête basée sur l’expression SQL suivante : 

**SELECT** (qui veut dire : **« sélectionne-moi »** ; et en indiquant « ***** », ça veut dire « toutes les entités »)

**FROM** (qui veut dire : **« depuis »** ; choisir les couches en indiquant leurs noms, séparés par « **,** »)

**WHERE** (qui veut dire : **« où »** ; c’est-à-dire que vous allez indiquer la condition, donc ici les champs communs, en tapant, aidé par QGIS, les noms des champs séparés par « **=** »).

Donc ici en substance, on ne veut pas d’entités sélectionnées au départ ; on lui demande de joindre toutes les entités à partir du moment où les valeurs rencontrées dans les champs indiqués seront identiques.

![requête SQL à faire](images/relation_1_n_06.jpg) 



La couche virtuelle est ajoutée, ne comportant visuellement que les entités qui sont concernées par les relations entre les couches (ici, les faits qui comportent du mobilier). Et en ouvrant la table attributaire, vous constatez que les faits qui comportent plusieurs mobiliers ont plusieurs enregistrements dans la table, avec les différents mobiliers individualisés en *n* enregistrements.

![résultat de la couche virtuelle](images/relation_1_n_07.jpg) 



## Pérenniser le résultat

A ce stade, vous pouvez vous "contenter" de l’ajout de cette couche virtuelle et faire vos analyses attributaires. Mais cette couche ne sera pas pérennisée dans votre projet lorsque vous le ré-ouvrirez. Aussi, pour la conserver, faites un clic-droit *Exporter > Sauvegarder les entités sous…* pour en faire une couche vectorielle archivée dans votre dossier opération.

![sauvegarder la couche virtuelle](images/relation_1_n_08.jpg) 