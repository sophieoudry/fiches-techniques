Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - équipe Formateurs & Référents SIG

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# Les opérateurs SQL

A utiliser pour faire entre autres des [requêtes attributaires](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/04_requete_att.html).



| Type de champ              | Opérateur de comparaison | Définition                                         |
| -------------------------- | ------------------------ | -------------------------------------------------- |
| Texte (QString)            | LIKE                     | Egal, comme (respect de la casse)                  |
|                            | ILIKE                    | Egal, comme (sans respecter la casse)              |
|                            | IS NOT                   | Pas égal, différent de                             |
|                            | IS NULL                  | Rechercher la valeur NULL                          |
|                            | IS NOT NULL              | Rechercher toutes les autres valeurs que NULL      |
|                            | IN (a, b)                | Rechercher dans un champ toutes les valeurs a et b |
| Numérique (entier ou réel) | =                        | Egal                                               |
|                            | <>                       | Différent de                                       |
|                            | >                        | Strictement supérieur à                            |
|                            | <                        | Strictement inférieur à                            |
|                            | >=                       | Supérieur ou égal à                                |
|                            | <=                       | Inférieur ou égal à                                |

Lorsque la valeur est inconnue, on laisse toujours la rubrique vide pour avoir la valeur *NULL*.	

Si la valeur est connue ET est égale à zéro, on note alors 0 (en chiffre).



### Les opérateurs logiques

`OR` pour séparer deux conditions dont au moins une doit être vérifiée (condition 1 ou condition 2 est vraie)
`AND` pour séparer deux conditions qui doivent être vérifiées simultanément (condition 1 et condition 2 sont vraies)
`NOT` permet d'inverser une condition.  

![schéma AND OR NOT](images/and_or_not.png) 



### Les caractères joker

Avec les opérateurs LIKE et ILIKE, il est possible d'utiliser des caractères remplaçant d'autres. C'est utile si vous avez un doute sur la façon dont un mot a été saisi (exemple : "céramique", "ceramique") ou si la valeur que vous cherchez n'est pas unique dans le champ (exemple : "céramique", "TCA, céramique").

- % remplace n'importe quel nombre de caractères
- _ remplace un seul caractère



Pour aller plus loin : la [fiche mémo SQL](https://formationsig.gitlab.io/sig32/memoSQL/memoSQL.html)