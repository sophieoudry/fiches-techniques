Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - Emeline Le Goff

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# Gestion de l'identifiant unique "IDSIG"

## 1. Théorie : de l'intérêt de conserver une numérotation unique

Il est important d’associer un numéro unique (identifiant = ID) pour chaque entité vectorisée (et donc son enregistrement dans la table attributaire). Cet **identifiant unique (nommé clef primaire)** permet de réaliser des jointures avec les tableurs d’inventaires ou tables des bases de données.

Chaque couche vecteur est individualisée (ouverture, poly, isolat) et sera liée à son tableur d’inventaire pour mettre à jour ses caractéristiques : nous serons alors dans le cadre des [**jointures attributaires de 1 à 1**](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/02_jointure_att.html). ans ce cas simple, les numéros uniques correspondent aux numéros des tranchées/sondages/faits/structures/isolats.

Mais dans le cadre de la cartographie et de l’analyse des mobiliers, nous sommes amenés à faire des [**relations attributaires pour des liens de 1 à n**](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/09_relation_1_n.html), car les correspondances sont multiples. 

![Repérer les clefs primaires](images/id_unique_01.jpg) 

En effet, en archéologie, nous sommes souvent obligés de descendre à des niveaux de précision inférieurs, regroupant des numéros de faits avec leur numéro de tranchée ou des numéros de faits avec leur numéro de sondage. Aussi, dans cette problématique de conservation d’un numéro unique, nous vous conseillons la création d’un **champ "IDSIG"** qui concaténera (associera) plusieurs numéros (ex : les numéros de fait et les numéros de sondage).

{% hint style='working' %} Il va sans dire que les numéros (valeurs) doivent être les mêmes dans la table attributaire de la couche vecteur que dans ceux du tableur à joindre pour les futures relations, et donc que cette manipulation de concaténation avec l’existence d’un champ unique de jointure IDSIG doit être également effectuée dans le tableur d’inventaire (ou table des bases de données).{% endhint %}



## 2. Pratique : une succession d'opérations logiques dans QGIS

Dans QGIS, avant de procéder à cette opération de concaténation de champs, il faut auparavant **découper la couche des vestiges (poly) en fonction de la couche des ouvertures**, pour ensuite associer le numéro du sondage à chaque tracé des faits présents dans ce sondage.

- Ne sélectionner dans la couche *ouverture* que les sondages (‘‘typouvert’’ LIKE ‘sondage’)

![Sélectionner les sondages](images/id_unique_02.jpg) 



- Découper les faits en fonction de ces sondages : Menu *Vecteur - Outils de géotraitement - Intersection* 

![Créer une couche via intersection](images/id_unique_03.jpg)  

- Renommer cette couche en numSGA_poly_SD

![nouveau shape pour jointure attributaire](images/id_unique_04.jpg) 

- Réaliser une jointure spatiale pour récupérer le numéro des sondages sur chaque tronçon de fait nouvellement découpé

 ![jointure spatiale](images/id_unique_05.jpg) 



Pour les paramètres de la jointure spatiale, choisissez bien de conserver tous les enregistrements dans la nouvelle couche  NumSGA_poly_SD_IDSIG. 

![jointure spatiale avec SD pour futur champ IDSIG](images/id_unique_06.jpg) 

- Dans la nouvelle couche *NumSGA_poly_SD_IDSIG*, nous allons alors pouvoir procéder à l’opération de concaténation des valeurs (ex : numéro de fait + numéro de sondage = NumunobsNumouvert) grâce à la calculatrice de champs

![champs IDSIG concaténation](images/id_unique_07.jpg) 

- Quand vous importerez votre tableur d’inventaire pour le lier à la couche vecteur (jointure de 1 à 1 ou relation de 1 à n), il faudra bien que ce champ - nommé différemment dans le cadre des relations, par exemple "FaitSD" - soit également présent.

![Importer tableau csv](images/id_unique_08.jpg) 

Par ailleurs, dans le cadre des études de mobilier, les isolats sont également pris en compte. Aussi, nous vous préconisons l’ajout, dans chaque couche, d’un champ "source" pour conserver les métadonnées, car il y aura donc potentiellement 4 possibilités d’origine du mobilier : Numéro du ‘fait’ ; Numéro du sondage ‘SD’ ; Numéro des ‘fait_SD’ ; Numéro de l’isolat ‘iso’.

![source origine mobilier](images/id_unique_09.jpg) 

Et si vous souhaitez ne faire qu’une seule relation, regroupez toutes vos entités en une couche IDSIG.shp en regroupant dans le champ IDSIG, les numéros de faits, de sondages, de fait_SD, et d’isolats. Attention cependant, si vous faites cela, à bien conserver dès le départ une logique de numérotation propre à chaque domaine, par exemple (selon la surface de l’opération) :

- Les isolats : de 1 à 100
- Les faits de 1000 à 4000
- Les sondages de 5000 à 800

Sinon, faites autant de jointure et de relation que de couches sources (SHP_fait, SHP_ouverture, SHP_poly_SD_IDSIG et SHP_isolat) ; et archivez bien vos données dans les dossiers appropriés. 