Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - S. Oudry

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.14 de QGIS



{% hint style='info' %}

Toutes les nouveautés ne sont pas présentes ici, ce n'est qu'une sélection ; la [liste complète est disponible ici](https://changelog.qgis.org/en/qgis/version/3.14). {% endhint %}



# Les nouveautés QGIS v.3.14 Pi

### Outils cartographiques

- dans la section sur la visibilité selon l'échelle, ajout d'un bouton à droite pour paramétrer l'échelle en fonction de celle d'une mise en page précise. 
- ajout d'un bouton *Désélectionner les entités de la couche active* 



### Interface utilisateur

- possibilité de faire glisser des couches ou des groupes d'un projet à un autre (drag 'n drop)
- si l'option *Intégrer la table attributaire* est activée, les nouvelles tables attributaires qui sont ouvertes le seront sous forme d'onglets à côté de la première



### Symbologie

- Raster Layer Contour Renderer : création de lignes de contour à partir des bandes raster. Il est possible de saisir l'intervalle de ces lignes de contour et d'en paramétrer la symbologie.

- Placement automatique d'étiquettes à l'extérieur des polygones (à partir de l'onglet *position*)

- Contrôle de l'emplacement du connecteur d'étiquette (côté extrémité de l'étiquette) : c'est plus joli maintenant !

  

### Panneau des couches

- Ajout d'une option *Move to bottom* pour déplacer une couche vers le bas de la liste.
- Lorsqu'il y a plus d'une couche sélectionnée et que l'on ajoute un groupe, cela crée un nouveau groupe et déplace les couches à l'intérieur.
- Ajout d'une option *Renommer le thème actuel* dans le menu déroulant des thèmes.
- Cocher ou décocher des couches avec la barre d'espace



### Mise en page

- possibilité de personnaliser les symboles de subdivisions de la barre d'échelle
- possibilité d'ajouter des subdivisions non numérotées dans les segments à droite de la barre d'échelle
- possibilité de placer un saut de colonne manuel dans la légende 
- possibilité de modifier la largeur individuelle des symboles de légende en double-cliquant dessus
- contrôle de l'espacement horizontal devant les symboles de groupes ou sous-groupes
- possibilité de coller une photo directement dans la mise en page
- nouveaux types de légendes

<img src="https://changelog.qgis.org/media/images/entries/77247015-c1b59680-6c78-11ea-92a9-7bbb2ba8638a.png" alt="img" style="zoom: 50%;" /> 

<img src="https://changelog.qgis.org/media/images/entries/77247024-dc880b00-6c78-11ea-827a-344645cec248.png" alt="img" style="zoom:50%;" /> 

### Expressions

- dans le calculateur d'expression, ajout d'un menu déroulant pour sélectionner une entité et en voir le rendu
- possibilité d'enregistrer, corriger et importer des expressions personnalisées



### Numérisation

- *Eviter les intersections* est amélioré avec trois modes disponibles : autoriser les intersections/superpositions ; éviter les intersections/superpositions sur la couche active ; éviter les intersections/superpositions sur une liste de couche définie par l'utilisateur
- Nouveaux modes d'accrochage : centroïde ou milieu de segment (en plus de sommet ; segment ; segment et sommet)
- Possibilité de s'accrocher sur l'entité en cours de numérisation : permet de faire des angles précis. A utiliser avec le raccourci Alt + a pour verrouiller l'angle
- Dans les listes de valeurs, possibilité d'ajouter une description (permet d'ajouter un commentaire ou un conseil pour choisir dans la liste)



### Divers

- nouvel algorithme pour convertir des géométries segmentées en géométries courbes
- possibilité de filtrer les couches WMS grâce à une barre de recherche
- possibilité d'importer des couches Spatialite ou en texte délimité avec des champs de type Date et DateHeure


