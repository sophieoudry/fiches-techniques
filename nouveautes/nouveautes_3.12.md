Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - S. Oudry

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.12 de QGIS

{% hint style='info' %}

Toutes les nouveautés ne sont pas présentes ici, ce n'est qu'une sélection ; la [liste complète est disponible ici](https://qgis.org/fr/site/forusers/visualchangelog312/index.html). {% endhint %}



# Les nouveautés QGIS v.3.12 Bucuresti

### Interface utilisateur

- Format d'enregistrement : dans *Préférences > Options > Général > Fichiers du projet*, possibilité de choisir le format d'enregistrement par défaut (qgz ou qgs). Attention, en faisant cela, vous supprimez la possibilité d'utiliser les tables de stockage auxiliaires, cruciales pour le paramétrage des étiquettes. 

- Personnalisation de l'interface : Si vous utilisez régulièrement l'explorateur de QGIS, vous avez maintenant la possibilité de le personnaliser et de décocher les sources de données dont vous ne vous servez pas. Même chose pour les barres d'outils

Menu *Préférences > Personnalisation de l'interface*, cocher *Autoriser la modification*

![personnalisation explorateur](images/v3.12_01.png) 



- Chargement de couches déplacées : depuis la version 3, QGIS répare automatiquement les chemins vers le nouvel emplacement des couches d'un dossier, à condition que l'on donne le nouveau chemin pour l'un d'entre eux. Il y a désormais la fonction *Auto find* qui fait le travail à notre place. 

- Affichage de la distance (et direction) lors d'un déplacement de carte dans la barre d'état
- Afficher l'action *Ouvrir le document...* dans le navigateur : exemple PDF avec application externe par défaut.



### Symbologie

- Masquage sélectif : Il permet de définir des zones de «masque» autour d’étiquettes ou de marqueurs de points. Masque sélectif d'une couche par rapport à une autre, disponible à partir du panneau des couches. Sélection de la distance et des couches concernées

- Nouveau type de couche de symbole de remplissage : "Marqueur aléatoire". Les polygones peuvent être représentés par des symboles SVG placés de manière aléatoire. Les options incluent : 
  - le nombre de symboles de marqueur à rendre pour le polygone
  - si les marqueurs rendus près des bords des polygones doivent être coupés ou non à la limite du polygone
  - un nombre aléatoire facultatif , pour donner un placement cohérent  des marqueurs chaque fois que les cartes sont actualisées
- Pour les rasters avec des pixels no data : option pour définir leur couleur de rendu
- Etiquettes : possibilité de supprimer la position d'étiquette personnalisée, avec la touche retour arrière pendant le déplacement.
- Diagrammes : nouveau type de diagramme (barres empilées)



### Mise en page

- Glisser-déposer des images sur le composeur depuis l'explorateur de l'ordinateur
- Possibilité de créer des tableaux sans lien avec une table attributaire (Ajouter une table fixe)
- Pour l'ajout d'une table attributaire, possibilité de conserver la mise en forme conditionnelle
- Ajout d'un champ de recherche au gestionnaire de mise en page



### Expressions

- nouvelles expressions `is_empty(geom)`vérifie si une géométrie est vide (géométrie qui ne contient pas de coordonnées) et `is_empty_or_null(geom)` pour vérifier si une géométrie est vide ou NULL
- nouvel algorithme *Renommer un champ de la table* : crée une nouvelle couche avec champ renommé
- possibilité d'enregistrer les expressions : stockées dans *user expressions* 
- prise en charge du Français pour le format _date() : 2012-05-15 transformé en 15 mai 2012



### Numérisation

- Correction des attributs invalides lors de la copie dans une autre couche : en cas de contraintes dans la couche arrivée (par exemple liste de valeurs), une boite de dialogue s'ouvre et permet de modifier chaque entité une par une. Options pour rejeter toutes les propositions, pour tout coller même si c'est invalide ou passer. 



### Traitements

- amélioration de l'algorithme de traitement des couches de package pour pouvoir ajouter de nouvelles couches aux GeoPackages existants. 
- Nouvel algorithme *Repair shapefile* au cas où le .shx serait manquant ou cassé

- Nouvel algorithme *Détecter les modifications de l'ensemble des données* (*Detect Dataset Changes*) : pour comparer deux versions d'une même couche. Crée 3 couches temporaires (entités non changées, entités ajoutées, entités supprimées)
- Nouveau mode pour l'algorithme *Joindre les attributs par localisation* : join attributes with the largest overlap (joindre les attributs ayant le plus grand chevauchement)
- Possibilité de glisser les couches depuis le panneau couche dans un algorithme : Permet d'éviter de chercher la couche dans le menu déroulant


