# Summary

## Fiches techniques

**Nouveautés des versions**

- [Nouveautés de la version 3.12](nouveautes/nouveautes_3.12.md)
- [Nouveautés de la version 3.14](nouveautes/nouveautes_3.14.md)

**Prise en main**

  - [Installer le logiciel](priseenmain/00_installer.md)
  - [L'interface de QGIS](priseenmain/01_interface.md)
  - [Les Systèmes de Coordonnées de Référence](priseenmain/04_projection_SCR.md)
  - [Constituer un SIG](priseenmain/02_alimenter.md)
  - [Interrogation simple des données](priseenmain/03_interroger.md)
  - [Ajouter des extensions](priseenmain/05_extensions.md)

**Vecteur - Dessin et saisie**
  - [Dessiner dans QGIS : les outils de base](vecteur_dessin/01_numerisation_1.md)
  - [Accrochage et topologie](vecteur_dessin/02_accrochage.md)
  - [Outil de nœuds](vecteur_dessin/03_outil_noeuds.md)
  - [Vérification de validité et de géométrie](vecteur_dessin/04_verif_geom.md)
  - [Les courbes de Bézier avec l'extension Bezier Editing](vecteur_dessin/06_bezier_editing.md)
  - [Dessiner du pierre à pierre](vecteur_dessin/05_pap.md)
  - [Coupes stratigraphiques et QGIS](vecteur_dessin/coupe_SIG/coupes_QGIS.md)

**Vecteur - Manipulation et interrogation de données**
  - [Jointure attributaire : liens de 1 à 1](vecteur_donnees/02_jointure_att.md)
  - [Relation attributaire : liens de 1 à n](vecteur_donnees/09_relation_1_n.md)
  - [Jointure spatiale](vecteur_donnees/03_jointure_spatiale.md)
  - [Calculatrice de champs](vecteur_donnees/01_calcu_champ.md)
  - [Les opérateurs SQL](vecteur_donnees/08_operateurs_sql.md)
  - [Requête attributaire](vecteur_donnees/04_requete_att.md)
  - [Requête spatiale](vecteur_donnees/05_requete_spatiale.md)
  - [Gestion des identifiants uniques](vecteur_donnees/11_gestion_id_uniques.md)
  - [Ajouter une couche de points csv](vecteur_donnees/07_couche_points_csv.md)
  - [Analyse par maille ou carte de densité](vecteur_donnees/06_analyse_maille.md)
  - [Calculs d'orientation](vecteur_donnees/10_calculs_orientation.md)
  - Calcul de cubature à partir d'un MNT et utilisation de la calculatrice raster [à venir]

**Vecteur - Symbologie**
  - [Cartes du BRGM et leurs légendes](vecteur_symbologie/01_cartes_legendes_BRGM.md)
  - [Ajouter des palettes de couleurs](vecteur_symbologie/04_palette_couleurs.md)
  - [Connecteurs d'étiquettes (versions antérieures à 3.10)](vecteur_symbologie/02_connecteur_etiquette.md)
  - [Connecteurs d'étiquettes (version 3.10 et après)](vecteur_symbologie/03_connecteurs_v3_10.md)
  - [Utiliser les styles de couches et les thèmes de mise en page](vecteur_symbologie/05_styles_themes.md)
  - [Gérer les superpositions de vestiges](vecteur_symbologie/06_superpositions.md)

**Raster**

  - [Géoréférencer une image](raster/02_georef.md) 
  - [Ajouter un flux WMS/WFS](raster/01_flux_WMS.md)
  - [Réduire des images géoréférencées en lot](raster/03_reduire_photo_georef.md)
  - [Utiliser les données altimétriques du RGE-alti de l'IGN](raster/04_rge_alti.md)
  - [Fusionner des dalles MNT en un seul raster](raster/05_fusion_raster.md)
  - [Gérer la symbologie d'un MNT (Modèle Numérique de Terrain)](raster/06_symbol_MNT.md)
  - [Générer des courbes de niveau à partir d'un MNT](raster/07_courbes_niveau.md)
  - [Générer un profil du terrain à partir d'un MNT et calculer le pourcentage de pente](raster/08_calcul_pente.md)
  - [Joindre les attributs d'un raster à une couche de points : l'extension Point Sampling Tool](raster/09_point_sampling_tool.md)

**Topo**

- L'extension ArchéoCAD [à venir]
- La chaîne opératoire en topographie [à venir]
- Créer un Modèle Numérique de Terrain [à venir]

**Mise en page**
  - [Créer une carte simple](https://formationsig.gitlab.io/fiches-techniques/miseenpage/01_mise_en_page_base.html)
- [Créer un atlas](https://formationsig.gitlab.io/fiches-techniques/miseenpage/05_atlas_01.html)
- [Mise en valeur des entités de l'atlas](https://formationsig.gitlab.io/fiches-techniques/miseenpage/02_atlas_02.html)
- [Créer un catalogue de sépultures](https://halshs.archives-ouvertes.fr/halshs-03782910) : lien externe vers HAL
- [Créer et utiliser des modèles de mise en page](https://formationsig.gitlab.io/fiches-techniques/miseenpage/04_modeles.html)
- [Utiliser les projets pré-paramétrés pour générer les cartes de localisation](https://formationsig.gitlab.io/fiches-techniques/miseenpage/03_carte_loc.html)

**Autres outils**

  - [QField pour QGIS ](autres_outils/01_qfield.md)