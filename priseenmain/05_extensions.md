Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)   

Inrap - équipe Formateurs & Référents SIG

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# Ajouter des extensions

## 1. Une extension c'est quoi ? 

Une extension (ou plugin en Anglais), c'est un outil supplémentaire, non inclus dans le logiciel. Les extensions peuvent être réalisées par des développeurs sans lien avec les développeurs du logiciel, pour répondre à un besoin précis. 

C'est pour cela que la création des extensions peut être financée par des entreprises ou des collectivités. Dans le cas de QGIS, si une extension est très utilisée et stable, elle peut être intégrée au logiciel dans les versions suivantes. 



### 1.1. Ajouter une extension

Pour accéder au vaste catalogue des extensions, aller au Menu *Extension > Installer/Gérer les extensions*.

![menu extension](images/extension_01.png)  



Dans la fenêtre qui s'ouvre, le panneau de gauche présente plusieurs onglets : 

![onglets](images/extension_02.png)  

L'onglet "Tout" vous propose la totalité des extensions disponibles dans le dépôt officiel des extensions QGIS.



### 1.2. Installer une extension

Dans la liste des extensions disponibles, n'hésitez pas à vous servir de la barre de recherche en haut de la fenêtre. 

Une fois l'extension sélectionnée, il suffit de cliquer sur *Installer le plugin*.

![installer le plugin](images/extension_03.png)  



Vous la retrouverez ainsi au moins soit :

- dans la liste des extensions dans le menu *Extension* (Theme Selector, Easy Custom Labeling)
- dans la barre d'outils des Extensions (Maps Printer, Vector Bender)
- soit encore dans leur propre barre d'outils (ArcheoCAD, Bezier Editing, Data Plotly)

Il arrive qu'elles soient intégrées également dans d'autres menus ou d'autres barres d'outils, mais il y a alors plusieurs façons d'accéder aux extensions. 



### 1.3. Mettre à jour les extensions installées

Dans les paramètres, vous pouvez choisir demander à QGIS de chercher des mises à jour à chaque démarrage de QGIS ou moins fréquemment (jusqu'à une fois par mois). Il est fortement **conseillé de mettre à jour** les extensions régulièrement. 



## 2. La différence entre installation et activation

**Attention**, une extension installée n'est pas forcément activée, ce qui peut être source d'énervement. Si vous ne retrouvez pas une extension dans les menus ou les extensions, vérifiez qu'elle est bien activée, c'est-à-dire qu'elle est cochée dans la liste des extensions installées. 

![activer les extensions](images/extension_04.png)  

Dans la photo ci-dessus, les extensions "Area Along Vector", "eVis" et "Outils GPS" ne sont pas actives. 



## 3. Quelques extensions utiles à installer ou à activer

*Ceci n'est pas une liste exhaustive, seulement quelques conseils, n'hésitez pas à demander à votre référent.e*

- **Processing** : ne vous posez même pas la question, activez-la. Grâce à elle, vous aurez accès à tous les traitements de la boîte à outils

- **Georéférenceur GDAL** : inclus dans QGIS, il faut parfois l'activer. Indispensable pour géoréférencer des rasters, comme son nom l'indique. [Pour les manipulations, voir la fiche technique](https://formationsig.gitlab.io/fiches-techniques/raster/fiche_georef.html).
- **Vérificateur de géométrie** et **Vérificateur de topologie** : indispensables pour éviter les erreurs et les bugs de traitement de données. Il y a même une [fiche technique dédiée](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/verif_geom.html). 
- **Bezier editing** : très pratique pour le dessin. On en parle dans [cette fiche sur les outils de numérisation](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/numerisation_1.html).
- **French Locator Filter** : vous saisissez le nom d'une commune ou une adresse dans la barre de recherche de QGIS en bas à gauche de la fenêtre principale et hop, vous y êtes !
- **Group Stats** : pour faire des statistiques dans vos tables
- **Maps Printer** : pour exporter en pdf plusieurs mises en page en une seule fois.
- **Vector Bender** : indispensable pour l'import de dxf dans QGIS. Il y a [un atelier à ce sujet](https://formationsig.gitlab.io/reprise).
- **Data Plotly** : pour faire des diagrammes en bâton ou à moustaches sans passer par un tableur
- **Value Tool** : renvoie la valeur d'un pixel sur un raster. Très pratique quand on utilise un MNT