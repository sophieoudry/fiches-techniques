![Document publié sous licence Creative Commons - CC BY-NC-SA](images/CC.png) 

Inrap - équipe Formateurs SIG

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# Interroger ses données

Vous avez des couches vecteur et vous aimeriez savoir à quoi correspondent les entités visibles sur la carte ? Voici trois façons d'arriver au but.

## 1. Outil "Identifier les résultats"

L'outil "Identifier les résultats" ![outil identifier](images/interroger_01.png) est disponible dans la barre d'outils des attributs. En cliquant à l'endroit voulu sur la carte, une petite fenêtre s'affiche pour vous demander dans quelle(s) couche(s) vous souhaitez trouver l'information. 

![couche](images/interroger_02.png) 

Vous pouvez alors cliquer sur la couche qui vous intéresse ou bien choisir *Identifier tout*. Le nombre entre parenthèses indique le nombre d'entités présentes à l'endroit où vous avez cliqué.

Dans l'exemple ci-dessus, seuls les numéros de vestiges m'intéressent, je vais alors choisir la couche poly. 

![identifier poly](images/interroger_03.png) 

On obtient une liste qui est en fait le contenu de la table attributaire pour les deux entités. Comme la table attributaire n'est pas très remplie (il n'y a que le numéro de vestige), il y a peu de lignes et beaucoup de NULL qui indiquent une case vide. 

{% hint style='working' %} Pour quitter l'outil *Identifier les résultats*, il suffit de cliquer sur la main ou l'outil de sélection. {% endhint %}

## 2. Afficher les étiquettes

Si vous savez où est stockée l'information que vous cherchez dans la table attributaire, vous pouvez aussi afficher les étiquettes pour ce champ. Par exemple vous souhaitez avoir les numéros de vestige, nous allons donc les afficher. 

Sur la couche concernée : clic droit - Propriétés (ou double clic sur la couche), puis aller au menu *Etiquettes*. Choisir alors *Etiquettes simples* puis dans le menu déroulant immédiatement en dessous, choisir la valeur, c'est-à-dire le champ où est stockée votre information. Pour les numéros de vestiges, ce champ est *numpoly*. 

![etiquette](images/interroger_04.png) 



Il est possible que l'étiquette ne se voie pas parce qu'elle est cachée dans la couche. Dans ce cas, vous pouvez ajouter un tampon (fond de couleur), disponible dans les paramètres des étiquettes.

![étiquette avec tampon](images/interroger_05.png)



## 3. Dans la table attributaire

Pour chercher une entité en particulier, vous utiliserez le filtre dans la table attributaire. Dans le bas de la fenêtre, cliquer sur *Montrer toutes les entités* puis choisir *Filtre de champ* et choisir le champ qui contient l'information. Dans l'exemple des vestiges, il faut choisir *numpoly* et saisir le numéro dans la barre juste à droite.

![filtre de champ](images/interroger_06.png) 



Dans la table n'apparaît alors que l'entité que vous cherchiez (à moins que vous n'ayez plusieurs entités portant ce numéro). Un clic droit sur l'entité vous présente les options disponibles : 

![options entité](images/interroger_07.png) 

Pour afficher de nouveau toutes les entités, cliquer en bas de la fenêtre à gauche et choisir *Montrer toutes les entités*.

![montrer toutes les entités](images/interroger_08.png)



## 4. Pour les rasters

Les rasters ne sont normalement pas interrogeables, sauf les MNT (modèles numériques de terrain) où - en substance - chaque pixel a une couleur en fonction de l'altitude. Si vous utilisez le RGE Alti de l'IGN par exemple, vous pouvez utiliser l'outil *Identifier les résultats* ![identifier](images/interroger_01.png) pour obtenir l'altitude de la zone couverte par le pixel (à la ligne *Bande 1*). 

![valeur pixel](images/interroger_09.png) 