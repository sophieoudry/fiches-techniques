Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - équipe Formateurs & Référents SIG

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# Avant de commencer : c'est quoi un SIG ?

Un système d'information géographique (SIG) permet de manipuler des **objets** qui ont une **représentation graphique** **dans un espace géographique défini** (RGF93 - Lambert 93 par exemple), associée à des attributs. Un attribut est une information qui décrit l'objet selon des caractéristiques définies : identifiant, longueur, largeur, nature, etc. 

**Objet dans un SIG = forme géographique (géométrie) + données (attributs)**

Les SIG permettent donc la conception et l'utilisation de bases de  données relationnelles spatiales. On passe ainsi de l'illustration au traitement de données : dès lors qu'on "dessine" dans un SIG, on crée de la donnée potentiellement exploitable à partir de constats et d'un raisonnement scientifique. 

Il existe plusieurs logiciels de SIG, certains payants et d'autres libres. Le choix à l'Inrap a été fait d'utiliser QGIS, car c'est le logiciel libre le plus utilisé. Il est alimenté par une large communautés de développeurs et contributeurs et est régulièrement mis à jour. 



# L'interface de QGIS

![interface](images/interface_01.png) 



Si une barre d'outil ou un panneau ne sont pas présents, faire un clic droit dans la zone des barres d'outils et cocher le panneau ou la barre d'outil souhaité. 



## Barre d'outils de gestion de couches

![barre d'outils gestion de couches](images/interface_02.png) 



Dans l'ordre : 

1 : ajouter de couche vecteur

2 : ajouter de couche raster

3 : ajouter d'un maillage

4 : ajouter une couche de points délimités

5 : ajouter une couche SpatiaLite

6 : ajouter une couche virtuelle

7 : ajouter une couche PostGIS

8 : ajouter une couche WMS

9 : ajouter une couche WCS

10 : ajouter une couche WFS

11 : ajouter une couche GeoPackage

12 : ajouter une couche feuille de calcul



## Barre d'outils des attributs

![barre d'outils attributs](images/interface_03.png) 

Dans l'ordre :

1 : identifier les entités

2 : exécuter l'action de l'entité

3 : outils de sélection

4 : sélectionner des entités par valeur

5 : désélectionner les entités

6 : ouvrir la table attributaire

7 : calculatrice de champ

8 : boîte à outils

9 : statistiques simples

10 : effectuer des mesures

11 : afficher les infobulles

12 : annotation de texte