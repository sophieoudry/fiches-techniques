Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - équipe Formateurs & Référents SIG

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# Installer QGIS sur son PC

Aller sur le site de QGIS et accéder à la [page de téléchargement](https://qgis.org/fr/site/forusers/download.html). 

Dans l'onglet "Téléchargement pour Windows", descendre à :

![installeur](images/installer_01.png) 

Cliquer sur "Installateur indépendant de QGIS 3.10". A moins que vous teniez absolument à avoir la toute nouvelle version, il est préférable de choisir la version long terme (LTR) qui est la plus stable : la plupart des erreurs et bugs ont été corrigés. 



Télécharger le fichier et faire un double clic pour lancer l'installeur. Cliquez sur suivant à toutes les fenêtres pour faire l'installation par défaut. 