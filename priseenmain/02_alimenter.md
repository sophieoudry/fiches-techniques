Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - équipe Formateurs & Référents SIG

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# Constituer un SIG

**AVANT TOUTE CHOSE**, il est important de définir l'espace géographique dans lequel vous travaillez. La projection officielle en France métropolitaine est le **Lambert 93** depuis décembre 2000. 

Vous pouvez avoir à gérer des données qui sont dans d'autres projections : par exemple pour la France métropolitaine, le Lambert 9 zones a pu être utilisé (CC42 à CC50), de même que le Lambert I à IV, Lambert carto et Lambert étendu.

Dans certains logiciels de SIG et notamment QGIS, ces projections sont codées en suivant ceux utilisés par le European Petroleum Survey Group (EPSG). Le Lambert 93 est codé en EPSG 2154.

Pour que vos données atterrissent au bon endroit, il est **crucial** de vérifier régulièrement que vous êtes dans le bon système de coordonnées de référence (**SCR**). 

Le choix du système de projection par défaut se fait dans le menu *Préférences > Options > SCR* : choisir **EPSG 2154 : Lambert 93 IGNF 93**

 ![SCR](images/alimenter_02.png) 



## Créer un nouveau projet

Il faut ensuite créer un projet : Menu Projet > Nouveau

Ce projet regroupe entre autres les couches que vous souhaitez y afficher. 



# 2. Les différents types de couches

Il existe différents types de couches utilisables dans QGIS, toutefois les deux principaux sont les couches vecteur et raster.

## 2.1. Vecteur

Les couches vecteur sont les fichiers que vous rencontrerez le plus dans le SIG. Un modèle de vecteur représente la localisation et la forme d'une entité géographique à partir de *points*, de *lignes* ou de *polygones*. Ces entités géographiques sont associées à des données, appelées *attributs*. 

Chaque couche vecteur contient donc des tables attributaires, visibles sous forme de tableaux où sont répertoriées les informations sur les entités. 

Le principal format de couche vecteur est le *shapefile*, qui est un format créé par la société ESRI. Un shapefile est composé d'au moins 4 fichiers, qu'on peut distinguer par leur extension :

- shp : le fichier de forme en lui même, c'est lui qu'on ouvre dans QGIS
- dbf : le fichier qui stocke les données attributaires
- shx : le fichier qui stocke l'index de la géométrie
- prj : le fichier d'information sur la projection

Attention ! Il ne faut jamais séparer ces fichiers les uns des autres (et ils peuvent être plus nombreux)



## 2.2. Raster

Ce sont généralement des images auxquelles on ne peut attribuer de données. Il peut s'agir de photos géoréférencées, de cartes (comme la carte au 25000e de l'IGN) ou de modèles numériques de terrain (MNT). Les MNT ont quelques données liées aux pixels puisque chaque pixel représente une altitude. 

Ces images ont dans tous les cas une information géographique associée puisqu'elles sont géoréférencées. 



# 3. Faire monter et organiser les couches

## 3.1. Ajouter des couches

Un clic dans la barre d'outils de gestion des couches vous permet d'afficher le gestionnaire de sources de données. 

![ajout couche](images/alimenter_03.png) 

Dans la partie de gauche, vous pouvez choisir le type de fichier à aller chercher. Ici dans la partie de droite, l'explorateur de QGIS fonctionne comme l'explorateur Windows. A partir de l'accueil ou du disque C:, vous avez accès à vos données. 

Un double-clic sur la couche l'importe dans QGIS. Cette couche apparaît alors dans le panneau des couches. 

![panneau couches](images/alimenter_04.png) 

Le panneau des couches liste les couches du projet

- en gras, ce sont les couches visibles dans le canevas de la carte
- en italique grisé, ce sont les couches non visibles, par exemple parce qu'elles sont été décochées (c'est le cas ici), ou parce qu'elles ont été paramétrées pour ne pas s'afficher à certaines échelles
- les couches raster sont identifiées par le logo ![logo raster](images/alimenter_05.png) 
- les couches vecteur polygone sont identifiées par un carré dont la symbologie change selon celle qui est appliquée aux entités (ici cadastre en trait noir et fond blanc ; emprise en trait pointillé et fond blanc)
- les couches vecteur ligne sont identifiées par un trait
- les couches vecteur point sont identifiées par un point
- les couches temporaires, créées à l'issue de calcul, sont repérables par le symbole du transistor![logo couche temporaire](images/alimenter_06.png) à droite. Attention, comme leur nom l'indique, elles sont temporaires et disparaîtront à la fermeture du logiciel. 



## 3.2. Organiser les couches

Le haut du panneau permet d'organiser les couches

![organisation panneau](images/alimenter_07.png) 

De gauche à droite : 

- ouvrir le panneau de style des couches
- créer un groupe
- afficher ou masquer certaines couches
- filtrer certaines couches
- étendre tout ou réduire tout : permet d'ouvrir ou fermer les groupes ou arborescences internes
- supprimer les couches 



On peut aussi sans l'aide d'icônes déplacer des couches : en faisant un cliquer-glisser sur chaque couche. Cela règle l'ordre d'affichage dans le canevas de la carte.

