Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - équipe Formateurs & Référents SIG

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# Projections géographiques et Système de Coordonnées de Référence (SCR)

## 1. Les projections 

Passer de la surface sphérique (ellipsoïde) de la Terre (géoïde) à la surface plane d’une carte nécessite de recourir à une projection cartographique. Il s’agit de techniques mathématiques qui permettent la transformation de coordonnées géographiques en degrés, en coordonnées cartographiques métriques. 

**Attention**, qui dit projection dit **déformation** : aussi, chaque pays possède son propre système de coordonnées (lui-même peut être décomposé en plusieurs SCR).



![](images/proj_scr_01.png)

Coordonnées Géographiques (degré)  // Coordonnées Rectangulaires (mètre)

Longitude (Nord-Sud)   //    X (abscisses)

Latitude (Ouest-Est)   //   Y (ordonnées)

Hauteur (élévation)   //   Z (altitude)



### 1.1. Choisir une projection

Choisir une projection, c’est choisir une représentation plane de la terre.

Il faut choisir un **type** :

- projection équivalente : conserve localement les surfaces ;

- projection conforme : conserve les angles donc les formes ;

-  projection aphylactique : ne conserve ni les distances, ni les angles, mais peut conserver les distances sur les méridiens.

Puis une **forme** : cylindrique, conique ou azimutale.



![formes](images/proj_scr_02.png) 





**Datum** : Référence géographique définie par une origine (position par rapport au centre de la Terre), une orientation (axes des pôles et d’un méridien), une ellipsoïde (surface représentant le géoïde).



![](images/proj_scr_03.png) 



### 1.2. SCR et EPSG

**SCR** : Un Système de Coordonnées de Référence (composé d’une projection et d’un datum), est un référentiel dans lequel on peut représenter des éléments dans l’espace. Ce système permet de se situer sur l’ensemble du globe terrestre grâce à un couple de coordonnées géographiques.

**EPSG** : Le [European Petroleum Survey Group](http://epsg-registry.org/ ) a établi un **identifiant numérique unique** pour chaque SCR.

voir aussi : http://spatialreference.org/



## 2. Les projections utilisées en France

Emprise mondiale // WGS84 : EPSG 4326

Emprise européenne // ETRS89-extended : EPSG 3034

Emprise nationale de la France métropolitaine // Lambert 93 : EPSG 2154

![projection conique conforme](images/proj_scr_04.png) 

Le **Lambert 93** est une projection conique conforme : elle conserve les angles, donc les formes (Lambert 93 -EPSG 2154-, et Lambert 93 ‘‘zones’’// système RGF 93)



![Lambert zones](images/proj_scr_05.png) 

Convertisseur de données en ligne : http://geofree.fr/gf/coordinateconv.asp



### Systèmes métriques en France

Coordonnées cartésiennes

![](images/proj_scr_06.png) 



![](images/proj_scr_07.png) 



Pour retrouver la projection de vos coordonnées :

GeoFree : http://geofree.fr/gf/coordinateconv.asp

DoGeo : https://app.dogeo.fr/Projection/#/point-to-coords



## 3. Le SCR dans QGIS

{% hint style='danger' %}
Attention, le SCR doit se paramétrer à deux niveaux : celui du logiciel et celui du projet en cours. 
{% endhint %}



### 3.1. Paramétrer le logiciel

Menu *Préférences  > Options > SCR*

![paramétrer QGIS](images/proj_scr_08.png) 



### 3.2. Paramétrer le projet

Menu *Projet > Propriétés > SCR*

Il est bon de prendre l'habitude de regarder régulièrement si la projection est bonne - en bas à droite de la fenêtre principale (ici entouré en rouge : EPSG 2154)

![paramétrer le projet](images/proj_scr_09.png) 



### 3.3. Vérification pour les couches du projet

Il est utile de vérifier le SCR des couches dans les propriétés de celle-ci :

*Propriétés > Information*

![SCR de la couche](images/proj_scr_10.png) 



