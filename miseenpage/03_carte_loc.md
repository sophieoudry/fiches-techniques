Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - S. Oudry, G. Achard-Cormpt, C. Font

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# Utiliser les projets pré-paramétrés pour générer les cartes de localisation



- section 1 du rapport : pour répondre aux demandes de l'arrêté de 2004 sur les rapports d'archéologie
- présentation rapide : 
  - A : carte de France avec région mise en rouge
  - B : carte régionale avec départements
  - C : carte au 1/250 000
  - D : carte au 1/25 000 et fond cadastral
- accessible dans le dossier Ressources si vous avez suivi la formation Niveau 2 Figures, sinon demander à vos référents/formateurs, elles sont accessibles sur un serveur national
- les projets ont été mis à jour début 2020 notamment pour intégrer les nouveaux flux WMS de l'IGN et les nouvelles régions
- les différents éléments nécessaires à une carte sont présents et vous n'avez pas à les modifier : flèche nord, échelle graphique, légende, sources, coordonnées géographiques

{% hint style='danger' %}Attention ! à moins que vous ne sachiez exactement ce que vous faites, ne faites que les manipulations indiquées ci-dessous au risque de corrompre le projet. Enfin, une fois vos cartes de localisation éditées, N'ENREGISTREZ PAS votre projet - une fois n'est pas coutume.{% endhint %}

## 1. Copier le dossier et ouvrir le projet

- copier la totalité du dossier et pas seulement le projet QGIS
- un projet par région : sélectionner sa région 
- groupe "emprises" est caché : y ajouter l'emprise de votre opération, cocher le groupe pour l'afficher



## 2. Appliquer les styles

- la couche MODELE est paramétrée pour que le style de l'emprise change en fonction de l'échelle à laquelle vous serez et donc en fonction de la carte que vous allez générer
- pour en savoir plus sur les thèmes de carte, voir la fiche sur les styles et les thèmes
- sur la couche MODELE, clic droit *Style - Copier le style* *- Toutes les catégories de style* 

![copier le style](images/cartes_loc_01.png) 

- sur votre couche emprise, clic droit *Style - Coller le style - Toutes les catégories de style* 



## 3. Mettre les thèmes à jour

- chaque thème correspond à l'une des cartes qui sera générée pour la mise en page
- aller dans *Thème* et cocher le thème A

![appliquer un thème](images/cartes_loc_02.png)

- cocher la couche emprise pour l'afficher et remplacer le thème A

![remplacer le thème](images/cartes_loc_03.png)

- répéter cette manipulation pour chacun des thèmes : 
  - afficher le thème
  - cocher l'emprise pour l'afficher
  - remplacer le thème



## 4. Utiliser les thèmes pour générer les cartes de localisation

### 4.1. Cartes A, B et C

- ouvrir le gestionnaire de mises en page, sélectionner la mise en page "A-B-C-localisation" (éventuellement laisser le temps à la mise en page de s'afficher, il y a un flux WMS dedans)

![gestionnaire de mise en page](images/cartes_loc_04.png) 

- votre carte régionale a bien un petit point rouge pour l'emprise
- pour la carte au 250 000, il faut régler l'emprise de la carte pour qu'elle corresponde à celle du canevas principal (au préalable, vous pourriez avoir besoin de zoomer sur votre emprise). Cela se fait dans les propriétés de l'élément C-Carte 250 et c'est la première icône avec une flèche orange.

![mettre à jour l'emprise de la carte](images/cartes_loc_05.png) 

- vous n'avez plus qu'à exporter votre carte en pdf ou en svg



### 4.2. Cartes D

- ouvrir le gestionnaire de mise en page et choisir D 25000-cadastre, puis Afficher
- cette mise en page est composée de deux cartes, la première avec le fond SCAN 25 de l'IGN et la seconde avec le fond cadastral en vecteur (PCI : Plan Cadastral Informatisé). Tous deux sont des fonds de carte WMS et WFS
- sélectionner la carte au 25 000 (première page) et cliquer sur l'icône "Régler l'emprise de la carte pour qu'elle corresponde à l'emprise du canevas principal", comme indiqué plus haut
- répéter pour la carte sur fond cadastral (deuxième page)
- exporter votre carte