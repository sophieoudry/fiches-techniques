Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - S. Oudry, G. Achard-Corompt, C. Font

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# Créer et utiliser des modèles de mise en page

Pourquoi créer des modèles ? 

Créer des modèles vous permettra de gagner du temps et de ne pas refaire tous les réglages nécessaires pour obtenir de belles figures à la bonne taille et aux bonnes échelles



## 1. Créer une première mise en page

- choisir les dimensions de la page. Voyez avec les collègues qui font la PAO pour qu'ils vous indiquent les dimensions habituelles ; sinon choisissez 4 ou 5 tailles de figure (c'est la largeur qui compte)
- choisir l'orientation de la page : portrait ou paysage
- Insérer les éléments dont vous aurez besoin : carte, nord, échelle, carte de localisation avec aperçu et nouvelle échelle, etc.

## 2. Enregistrer un modèle et l'utiliser

### 2.1. Enregistrer une mise en page comme modèle

- pour l'enregistrer comme modèle : Menu *Mise en page - Enregistrer comme modèle*. L'emplacement par défaut est le dossier composer_templates dans votre profil utilisateur ou dans le profil par défaut si vous n'en avez pas créé

```
C:\Users\VotreSession\AppData\Roaming\QGIS\QGIS3\profiles\VotreProfil\composer_templates
```

ou 

```
C:\Users\VotreSession\AppData\Roaming\QGIS\QGIS3\profiles\default\composer_templates
```

- si vous avez créé un profil utilisateur et que vous n'avez pas le dossier composer_templates, il suffit de le créer : ajouter un nouveau dossier et lui donner ce nom exactement
- vous pouvez l'enregistrer ailleurs mais c'est plus simple de garder cet emplacement puisque c'est à lui que QGIS fait appel par défaut. En plus, lorsque vous changerez de poste et copierez votre profil utilisateur pour ne rien perdre, tout sera dedans au lieu d'être dispersé dans votre ordinateur
- Quel nom lui donner ? A vous de voir, mais essayez d'être logique et concis : un numéro de taille, les dimensions en mm ou cm, l'orientation et les échelles prévues par exemple

```
taille_7_170x260_portrait_500_2000
```

On pourrait être encore plus concis et remplacer "portrait" par P et "paysage" par L (portrait et landscape en Anglais), à vous de voir ce qui vous parle le plus



### 2.2. Utiliser un modèle de mise en page

- ouvrir le gestionnaire de mise en page et dans la section *Nouveau depuis un modèle*, ouvrir le menu déroulant, choisir le modèle et cliquer sur *Créer*.

![nouveau depuis un modèle](images/modele_01.png)

 

- il ne vous reste plus qu'à ajuster les derniers éléments : mais vous aurez gagné du temps en n'ayant pas à paramétrer les dimensions de la page, l'ajout de graticules, de l'échelle, de la flèche nord, etc.



## 3. Aller plus loin : paramétrer pour différentes échelles

Vous risquez de ne pas toujours travailler aux mêmes échelles, il vous faut donc techniquement ajuster beaucoup de choses si vous voulez utiliser un modèle de base. 

On vous propose donc de pousser plus loin votre modèle de mise en page pour qu'il soit utilisable rapidement à différentes échelles prédéfinies.

Dans l'exemple qui suit, on veut conserver les mêmes dimensions de page et la même orientation, mais prévoir des cartes avec vignette de localisation, prêtes à être utilisées au 1/500 et au 1/2000. Dans la pratique, on aura autant de cartes et d'échelles graphiques que de facteurs d'échelle ; il suffira de cocher ou décocher les éléments qu'on ne souhaite pas faire apparaître. 

### 3.1. Créer une carte avec un premier facteur d'échelle

Par exemple, nous allons créer une première carte au 1/500. 

- ajouter une carte, la nommer *Carte principale 1/500* ; lui appliquer un cadre, des graticules 
- ajouter une échelle graphique pour cette *Carte principale 1/500* ; la nommer *échelle 1/500* ; la régler pour ce facteur d'échelle

### 3.2. Créer une deuxième carte avec un deuxième facteur d'échelle

Ajoutons maintenant une carte au 1/2000

- copier la *Carte principale 1/500* et la nommer *Carte principale 1/2000* ; la placer juste au-dessus ; régler les graticules

:bulb: Astuce : utilisez la commande *Coller en place* pour que votre carte se place exactement au-dessus de la précédente

- copier l'échelle graphique *échelle 1/500* et la nommer *échelle graphique 1/2000* ; la régler pour ce facteur d'échelle

### 3.3. Et ainsi de suite...

On peut continuer avec des vignettes de localisation, etc. 

C'est un peu long la première fois, mais vous gagnerez du temps dès que vous aurez à utiliser les modèles de mise en page : il vous suffira de décocher les cartes et les échelles dont vous n'avez pas besoin, voire de les supprimer de votre mise en page. 

Si vous vous sentez d'humeur altruiste, vous pouvez les partager avec vos collègues ! Il existe d'ailleurs des gabarits qui vous sont distribués lors de la formation niveau 2 "Les figures du rapport avec QGIS", créés par G. Achard-Corompt et C. Font. 