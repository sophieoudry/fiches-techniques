Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - S. Oudry, F. Robin

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# Mise en page : changer le style de l'entité selon l'atlas



Objectif : rendre plus visible l'entité concernée par l'atlas ; l'exemple présenté ici est fait avec des sépultures



# 1. Etapes préalables 

- paramétrer les étiquettes à l'échelle souhaitée pour la carte : taille de la police, emplacement par rapport à l'entité
- Facultatif : déclarer et modifier les styles et les thèmes



# 2. Créer une carte

- Créer une mise en page vide et ajouter une carte

- Onglet Atlas, cocher "Générer un atlas"

![paramètres de l'atlas](images/atlas_02_01.png)

La couche de couverture correspond ici à la couche de sépulture, c'est elle qui définira le déplacement. Le nom de la page de l'atlas correspond au numéro du vestige. Attention, c'est important pour le paramétrage par la suite ! 

Les pages de l'atlas sont triées ensuite selon ce même numéro. Ce n'est pas fait ici, mais vous pouvez filtrer l'atlas pour que seules certaines entités y figurent : exemple sur un site multi-périodes, un catalogue de sépultures spécifique à la période gallo-romaine et un autre à la période médiévale. 



# 3. Paramétrer le style des entités 

De retour dans la fenêtre principale du projet, sur la couche de sépulture, aller dans Propriétés > Symbologie. 

Au lieu de "Symbole unique", choisir "Ensemble de règles". Par défaut, il existe une seule règle, vous pouvez la supprimer avec le bouton moins en bas de la fenêtre. 

## 3.1. Paramétrer l'apparence des entités qui ressortent

Avec le bouton plus, ajouter une première règle :

![règle de symbole](images/atlas_02_02.png)

De haut en bas sur cette fenêtre : 

- Etiquette : c'est le nom que vous allez donner à cette règle. Par défaut c'est vide et vous pouvez le laisser ainsi

- Filtre : cliquer sur l'epsilon à droite et saisir la règle 

```
"num" = @atlas_pagename
```

"Num" est le nom du champ qui contient le numéro de l'entité (à trouver dans "Champs et valeurs") et "@atlas_pagename", c'est le nom de la page de l'atlas (à trouver dans "Variables")

- Echelle de visualisation : rien à changer *a priori* sauf si vous souhaitez que ce soit le cas
- Symbole : c'est là que vous allez choisir une symbologie qui ressort. Ici, simplement un remplissage en rouge
- Terminer en cliquant sur OK



## 3.2. Paramétrer l'apparence des autres entités

Ajouter une 2e règle avec le bouton plus. 

![](images/atlas_02_03.png) 

- Etiquette : le nom de la règle
- Au lieu de filtre, cocher "sinon" ou ELSE en langage informatique. Ca sert à appliquer cette règle à toutes les entités qui ne correspondent pas à la ou les règle(s) énoncée(s) précédemment. Ainsi dans notre cas, le logiciel mettra en rouge l'entité représentée dans la page de l'atlas et toutes les autres seront en gris. 
- Symbole : paramétrer la couleur pour qu'elle soit plus discrète que la précédente, ici gris clair. 
- Finir en cliquant sur OK

Les deux règles créées sont bien visibles : ![règles pour l'atlas](images/atlas_02_04.png) 



# 4. Retour à la mise en page

## 4.1. Générer l'atlas

Dans les propriétés de la carte 1, cocher "Contrôlé par l'atlas" et choisir l'échelle. Dans le cas de sépultures, les entités auront des dimensions relativement similaires, il est donc préférable de choisir une échelle fixe. 

Toutefois si vous représentez des vestiges dont les dimensions peuvent varier grandement, comme des fossés ou des bâtiments, il sera peut-être préférable de choisir l'option "Marge autour de l'élément" et de choisir la taille de cette marge.

Dans la barre d'outils de l'atlas, cliquer sur Aperçu de l'atlas pour le générer : ![générer un atlas](images/atlas_02_05.png) 



Et voilà ! L'entité de l'atlas (ici la st. 138) est bien en rouge et les autres sépultures en gris. 

![atlas](images/atlas_02_06.png)



## 4.2. Paramétrer les étiquettes

C'est un bon début, mais on pourrait souhaiter avoir une étiquette avec le numéro du vestige - et aucune autre étiquette de sépultures pour ne pas alourdir l'image. 

Le paramétrage se fait de la même manière pour les étiquettes que pour les symboles : avec un **ensemble de règles**.

Retourner sur la couche à étiqueter ; Propriétés > Etiquettes > Ensemble de règles

![règle d'étiquette 1](images/atlas_02_07.png) 

- pour aller plus vite, nous n'avons pas mis de nom à cette règle.

- pour le filtre, on utilise le même que pour les symboles : 

```
"num" = @atlas_pagename
```

- la case Etiquettes est à cocher et on la paramètre comme on le souhaite.

- finir en cloquant sur OK

De la même manière que pour les symboles, ajouter une deuxième règle pour les entités non représentées dans l'atlas.

![règle étiquette ELSE](images/atlas_02_08.png) 

- cocher la case "Sinon"
- décocher la case "Etiquettes"



Retourner dans la mise en page et cliquer sur "Rafraîchir", puis admirer

![atlas très beau](images/atlas_02_09.png)