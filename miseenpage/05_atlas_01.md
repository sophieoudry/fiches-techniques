

Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - équipe Formateurs & Référents SIG

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# Créer un atlas avec QGIS



## 1. Qu'est-ce qu'un atlas ?

Un atlas c'est une fonctionnalité du logiciel qui permet la génération automatique de cartes à partir d'une première mise en page : on aura une carte par entité de la couche que l'on souhaite mettre en évidence. Chacune des cartes sera centrée sur un point ou un polygone particulier de cette couche appelée **couche de couverture**. 

On peut utiliser un atlas pour générer des figures pour des sépultures, des bâtiments mais aussi des zones de prospection, des tracés linéaires, etc. Dans l'exemple qui suit, on va générer une carte pour chacune des extensions réalisées dans un diagnostic. 



## 2. Créer un atlas

### 2.1. Créer une mise en page 

On commence par créer une [nouvelle mise en page et lui apporter les éléments de base](https://formationsig.gitlab.io/fiches-techniques/miseenpage/01_mise_en_page_base.html) : carte, échelle graphique, flèche nord. 

{% hint style='info' %}
Il est tout à fait possible d'avoir un fond raster en WMS. 
{% endhint %}



### 2.2. Paramétrer l'atlas

Il faut ensuite paramétrer puis générer l'atlas : la première partie se fait dans le panneau Atlas. Si vous ne l'avez pas, faites un clic droit dans la zone grise et cochez *Atlas* ou Menu *Vue - Panneaux - Panneau Atlas*.

Cocher *Générer un atlas* pour activer les paramètres de configuration.

#### Sous-menu Configuration

![configuration de l'atlas](images/atlas_01_01.png) 

La **couche de couverture** est celle qui sert de repère à l'atlas et sur laquelle chaque page de l'atlas sera centrée : dans le cas d'un atlas de sépultures, la couche de couverture sera celle des sépultures. Dans le cas d'un linéaire, ce sera une couche de polygones délimitant les secteurs à présenter. 

Dans l'exemple qui suit - où l'on souhaite présenter des plans des extensions réalisées sur un diagnostic - la couche de couverture est la couche numSGA_ouverture. 

Dans certains cas, la couche de couverture peut être cachée, c'est le cas par exemple des polygones dans l'exemple d'un tracé linéaire. 

Le **Nom de la page** est celui qui sera donné à chaque page de l'atlas. Dans notre exemple, le nom de la page sera le numéro de l'extension, soit "numouvert".

Il est possible de générer un atlas sur une partie d'une couche seulement avec l'option **Filtrer avec** : ici, on ne souhaite que les extensions et pas les tranchées. 

- Cocher la case *Filtrer avec* puis cliquer sur le générateur d'expression ![générateur d'expression](images/atlas_01_02.png) 
- Dans la fenêtre, saisir l'expression qui permettra de filtrer, ici : 

```sql
 "typouvert" LIKE  'Fenetre' 
```

**Ordonner par** permet de trier les pages de l'atlas. Si vous utilisez des vestiges ou comme ici des extensions, il est préférable de trier dans un ordre numérique. Mais vous pourriez avoir besoin de trier en fonction d'autres critères, comme des datations ou des secteurs. 

On obtient donc pour notre exemple : 

![configuration achevée](images/atlas_01_03.png) 



#### Sous-menu Sortie

Dans cette section, vous pouvez paramétrer le nom des fichiers qui seront générés. Si vous **n'exportez qu'un seul fichier** (donc avec plusieurs pages), vous ne pouvez pas changer le nom, la zone reste grisée. 

Si vous exportez un fichier par vue de l'atlas, il vous faudra changer le nom ; par défaut c'est 'output_'|| @atlas_featurenumber, ce qui donne "output" suivi d'un numéro de 1 à n sans lien avec vos vestiges. 

Il est donc préférable de modifier ce nom, encore une fois avec le générateur d'expression ![epsilon](images/atlas_01_02.png)

```sql
'D128113_plan_extension_'  || @atlas_pagename 
```

 La partie entre apostrophes est le préfixe qui sera présent sur tous les noms de fichier, les deux barres servent à la concaténation (soit l'ajout juste à côté d'un autre élément). Enfin la variable @atlas_pagename renverra ce que nous avons demandé plus haut à *Nom de la page*, soit dans notre cas "numouvert".

On obtiendra donc des fichiers de type : D128113_plan_extension_588.pdf



### 2.3. Générer l'atlas

Dans le panneau *Propriétés de l'élément*, pour la carte, cocher *Contrôlé par Atlas*

![carte et atlas](images/atlas_01_04.png) 



Il reste à définir l'échelle de chacune des cartes : par défaut, le logiciel propose de présenter la totalité de l'élément avec une **marge de 10 %** autour. L'ampleur de cette marge est modifiable. C'est la solution à privilégier si vous présentez des éléments de taille très variable, comme ici pour nos extensions. 

Avec la deuxième option, QGIS choisira la meilleure échelle pour que l'élément de la couche de couverture soit ajusté au mieux à l'intérieur de la carte. 

{% hint style='danger' %}
Dans ces deux cas, l'échelle changera évidemment selon votre vue. Pensez à modifier les paramètres de l'échelle dans le sous-menu *Segments* : il faudra utiliser *Ajuster la largeur* au lieu de *Largeur fixe*. 
{% endhint %}

Enfin, dans le cas de zones ou vestiges de taille proche, comme des sépultures, il peut être plus judicieux de choisir une **échelle fixe** à paramétrer plus haut dans le sous-menu *Propriétés principales* de la carte. 

Tout est prêt, il ne reste plus qu'à générer l'atlas à partir du Menu *Atlas - Aperçu de l'Atlas* ou cliquer sur ![bouton Atlas](images/atlas_01_05.png) dans la barre d'outils de l'atlas. 

Les outils de navigation de l'atlas sont alors activés et il est possible de voir la liste des vues dans le menu déroulant.

![liste des vues](images/atlas_01_06.png) 

Le premier nombre (de 1 à n) correspond au numéro de vue de l'atlas (variable @atlas_featurenumber), le second est celui de la page que nous avons paramétré plus haut (variable @atlas_pagename). Dans notre exemple, c'est le numéro de l'extension. 



### 2.4. Autres éléments à ajouter

On peut ajouter d'autres éléments qui seront paramétrés par l'atlas également comme un titre, un extrait de la table attributaire, une légende, etc. 

Pour ajouter un **titre**, il faut ajouter une étiquette : Menu *Ajouter un élément - Ajouter une étiquette* ou cliquer sur ![étiquette](images/atlas_01_07.png) 

Si l'on souhaite un titre du genre "Extension n° ", dans les propriétés de l'étiquette, cliquer sur *Insérer une expression*. 

```sql
'Extension n° ' ||  @atlas_pagename 
```

Dans cet exemple, la variable @atlas_pagename va renvoyer la même chose que "numouvert". 

Il est possible d'aller un petit peu plus loin et demander la surface de cette extension, on complètera alors l'expression :

```sql
'Extension n° ' ||  @atlas_pagename   ||  '\n'  || 
 "surface"  || ' m²'
```

L'ensemble '\n' sert à renvoyer à la ligne et doit être placé entre deux symboles de concaténation.

Voici un exemple de mise en page pour une série de cartes d'extensions : 

![atlas terminé](images/atlas_01_08.png)

La légende a été ajoutée et on a coché *Ne montrer que les entités à l'intérieur de l'entité atlas courante*. Ainsi, la légende se met à jour automatiquement en fonction de ce qui est présent dans la page de l'atlas.

![atlas légende adaptative](images/atlas_01_09.png)



Il ne vous reste plus qu'à exporter votre atlas aux formats habituels : image, svg ou pdf ![export](images/atlas_01_10.png )

#### Pour aller plus loin

Consultez la fiche [Mise en page : changer le style de l'entité selon l'atlas](https://formationsig.gitlab.io/fiches-techniques/miseenpage/02_atlas_02.html) pour savoir comment faire ressortir l'entité concernée par l'atlas et uniquement elle. 