Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - équipe Formateurs & Référents SIG

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# Créer une carte simple

Depuis la version 3 de QGIS, la carte - anciennement nommée "composeur d'impression" - s'appelle "mise en page". Elle est accessible directement par l'icône ![icone nouvelle mise en page](images/mep_01.png) dans la barre d'outils *Projet* ou Menu Projet > Nouvelle mise en page (Ctrl + P)



![mise en page base](images/mep_02.png)	

Pour spécifier la taille de la page : clic droit dans la zone de rendu de la mise en page.



Ne pas hésiter à rafraîchir avec ![icone rafraîchir](images/mep_03.png) 



## Ajouter une carte 

Sélectionner l'outil ![icone carte](images/mep_04.png)ou *Ajouter un élément* > *Ajouter Carte* et tracer un rectangle représentant la taille de la carte. Dans l'onglet *Propriétés de l'objet*, spécifier si besoin sa taille et son orientation, mais surtout **son échelle**.



## Ajouter une échelle graphique

Sélectionner l'outil ![icone échelle](images/mep_05.png)ou *Ajouter un élément* > *Ajouter Echelle graphique* et tracer un rectangle sur la carte. Dans l'onglet *Propriétés de l'objet*, spécifier le nombre et la longueur des segments qui la composent.



## Ajouter une flèche nord

Sélectionner l'outil ![icone nord](images/mep_06.png) ou *Ajouter un élément* > *Ajouter Flèche du nord* et tracer un rectangle sur la carte. Dans l'onglet *Propriétés de l'objet*, aller dans *Rechercher dans les répertoires* pour utiliser l'un des nord par défaut de QGIS. 

Pour utiliser votre propre nord au format svg, toujours dans *Propriétés de l'objet*, aller à *Propriétés principales* > *Source de l'image* et cliquer sur pour accéder à votre fichier. 



## Exporter la carte

Sélectionner l'outil ![icone export pdf](images/mep_07.png) pour exporter en pdf ou l'outil ![icone export svg](images/mep_08.png) pour exporter en svg.



## Pour aller plus loin

Il est fortement recommandé de suivre la formation Niveau 2 Les Figures du rapport avec QGIS. 

