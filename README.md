# ![sigFT2](images/sigFT2.png)Fiches Techniques SIG

Ensemble (*work in progress*) des fiches techniques détaillant certaines manipulations dans QGIS.

{% hint style='info' %}
Il est possible de télécharger **l'ensemble** au format PDF   [![pdf](images/pdf.png)](https://gitlab.com/formationsig/fiches-techniques/-/jobs/artifacts/master/raw/public/fiches-techniques.pdf?job=pages) (:warning: ​*work in progress* - attention au nombre de pages) 
{% endhint %} 

**Nouveautés des différentes versions**

* [Nouveautés de la version 3.12](https://formationsig.gitlab.io/fiches-techniques/nouveautes/nouveautes_3.12.html)
* [Nouveautés de la version 3.14](https://formationsig.gitlab.io/fiches-techniques/nouveautes/nouveautes_3.14.html)

**Prise en main de QGIS**

*   [Installer le logiciel](https://formationsig.gitlab.io/fiches-techniques/priseenmain/00_installer.html)
*   [L'interface de QGIS](https://formationsig.gitlab.io/fiches-techniques/priseenmain/01_interface.html)
*   [Les Systèmes de Coordonnées de Référence](https://formationsig.gitlab.io/fiches-techniques/priseenmain/04_projection_SCR.html)
*   [Constituer un SIG](https://formationsig.gitlab.io/fiches-techniques/priseenmain/02_alimenter.html)
*   [Interrogation simple des données](https://formationsig.gitlab.io/fiches-techniques/priseenmain/03_interroger.html)
*   [Ajouter des extensions](https://formationsig.gitlab.io/fiches-techniques/priseenmain/05_extensions.html)

**Vecteur - Dessin et saisie**

*   [Dessiner dans QGIS : les outils de base](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/01_numerisation_1.html)
*   [Accrochage et topologie](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/02_accrochage.html)
*   [Outil de nœuds](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/03_outil_noeuds.html)
*   [Vérification de validité et de géométrie](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/04_verif_geom.html)
*   [Les courbes de Bézier avec l'extension Bezier Editing](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/06_bezier_editing.html)
*   [Dessiner du pierre à pierre](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/05_pap.html)
*   [Coupes stratigraphiques et QGIS](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/coupe_SIG/coupes_QGIS.html)

**Vecteur - Manipulation et interrogation de données**

*  [Jointure attributaire : liens de 1 à 1](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/02_jointure_att.html)
*  [Relation attributaire : liens de 1 à n](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/09_relation_1_n.html)
*  [Jointure spatiale](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/03_jointure_spatiale.html)
*  [Calculatrice de champs](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/01_calcu_champ.html)
*  [Les opérateurs SQL](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/08_operateurs_sql.html)
*  [Requête attributaire](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/04_requete_att.html)
*  [Requête spatiale](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/05_requete_spatiale.html)
*  [Gestion des identifiants uniques](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/11_gestion_id_uniques.html)
*  [Ajouter une couche de points csv](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/07_couche_points_csv.html)
*  [Analyse par maille ou carte de densité](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/06_analyse_maille.html)  
*  [Calculs d'orientation](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/10_calculs_orientation.html)

**Vecteur - Symbologie**

*  [Cartes du BRGM et leurs légendes](https://formationsig.gitlab.io/fiches-techniques/vecteur_symbologie/01_cartes_legendes_BRGM.html)
*  [Ajouter des palettes de couleurs](https://formationsig.gitlab.io/fiches-techniques/vecteur_symbologie/04_palette_couleurs.html)
*  [Connecteurs d'étiquettes (versions antérieures à 3.10)](https://formationsig.gitlab.io/fiches-techniques/vecteur_symbologie/02_connecteur_etiquette.html)
*  [Connecteurs d'étiquettes (version 3.10 et après)](https://formationsig.gitlab.io/fiches-techniques/vecteur_symbologie/03_connecteurs_v3_10.html)
*  [Utiliser les styles de couches et les thèmes de mise en page](https://formationsig.gitlab.io/fiches-techniques/vecteur_symbologie/05_styles_themes.html)
*  [Gérer les superpositions de vestiges](https://formationsig.gitlab.io/fiches-techniques/vecteur_symbologie/06_superpositions.html)

**Raster**

*  [Géoréférencer une image](https://formationsig.gitlab.io/fiches-techniques/raster/02_georef.html) 
*  [Ajouter un flux WMS/WFS](https://formationsig.gitlab.io/fiches-techniques/raster/01_flux_WMS.html)
*  [Réduire des images géoréférencées en lot](https://formationsig.gitlab.io/fiches-techniques/raster/03_reduire_photo_georef.html)
*  [Utiliser les données altimétriques du RGE-alti de l'IGN](https://formationsig.gitlab.io/fiches-techniques/raster/04_rge_alti.html)
*  [Fusionner des dalles MNT en un seul raster](https://formationsig.gitlab.io/fiches-techniques/raster/05_fusion_raster.html)
*   [Gérer la symbologie d'un MNT (Modèle Numérique de Terrain)](https://formationsig.gitlab.io/fiches-techniques/raster/06_symbol_MNT.html)
*  [Générer des courbes de niveau à partir d'un MNT](https://formationsig.gitlab.io/fiches-techniques/raster/07_courbes_niveau.html)
*  [Générer un profil du terrain à partir d'un MNT et calculer le pourcentage de pente](https://formationsig.gitlab.io/fiches-techniques/raster/08_calcul_pente.html)
*  [Joindre les attributs d'un raster à une couche de points : l'extension Point Sampling Tool](https://formationsig.gitlab.io/fiches-techniques/raster/09_point_sampling_tool.html)

**Topo**

- L'extension ArchéoCAD [à venir]
- La chaîne opératoire en topographie [à venir]
- Créer un MNT ou Modèle numérique de terrain [à venir]
- Calcul de cubature à partir d'un MNT et utilisation de la calculatrice raster [à venir]

**Mise en page**

*  [Créer une carte simple](https://formationsig.gitlab.io/fiches-techniques/miseenpage/01_mise_en_page_base.html)
*  [Créer un atlas](https://formationsig.gitlab.io/fiches-techniques/miseenpage/05_atlas_01.html)
*  [Mise en valeur des entités de l'atlas](https://formationsig.gitlab.io/fiches-techniques/miseenpage/02_atlas_02.html)
*  [Créer un catalogue de sépultures](https://halshs.archives-ouvertes.fr/halshs-03782910) : lien externe vers HAL
*  [Créer et utiliser des modèles de mise en page](https://formationsig.gitlab.io/fiches-techniques/miseenpage/04_modeles.html)
*  [Utiliser les projets pré-paramétrés pour générer les cartes de localisation](https://formationsig.gitlab.io/fiches-techniques/miseenpage/03_carte_loc.html)

**Autres outils**

*  [QField pour QGIS](https://formationsig.gitlab.io/fiches-techniques/autres_outils/01_qfield.html)

La version en ligne se trouve à l'adresse [https://formationsig.gitlab.io/fiches-techniques ](https://formationsig.gitlab.io/fiches-techniques)             

[![back_to_toc](images/back.png) table des matières - Formations SIG](https://formationsig.gitlab.io/toc/)

Toute la documentation est sous licence [![licence CC](images/CC.png)](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/)

