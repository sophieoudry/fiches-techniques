Ce document est sous licence Creative Commons ![Document publié sous licence Creative Commons - CC BY-NC-SA](images/CC.png)

Inrap - S. Oudry

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# Utilisation de QField (QGIS mobile) [![pdf](images/pdf.png)](https://gitlab.com/formationsig/fiches-techniques/-/raw/master/pdf/qfield.pdf?inline=false)

## 1. QField c'est quoi ? 

QField est une application conçue pour utiliser QGIS sur un appareil mobile (smartphone ou tablette). Il ne fonctionne pour l'instant que sous Android mais une application iOs est en cours de réalisation. 

Avec cette application, vous gérez le *même* projet QGIS sur le terrain et sur l'ordinateur, via des synchronisations.



Pré-requis : 

* Installer l'application QField sur le smartphone ou la tablette Android
* Installer l'extension QFieldSync sur QGIS



Liens vers la documentation disponible sur [l'extension QFieldSync](https://qfield.org/docs/fr/qfieldsync/index.html) et sur la [gestion du projet QGIS pour QField](https://qfield.org/docs/fr/project-management/index.html). 

## 2. Exemple de projet QGIS pour QField

### 2.1. Alimentation du projet sur ordinateur

Pour l'exemple ci-dessous, nous avons créé un projet QGIS [**projet_2020_Qfield.qgz**](https://gitlab.com/formationsig/fiches-techniques/-/raw/master/pdf/projet_test_QField.zip) alimenté avec plusieurs couches : 

- un fond de carte OSM
- 3 couches Geopackage : emprise, poly, isolats

{% hint style='info' %}QField supporte de nombreux types de fichiers, voir [ici pour la liste complète](https://qfield.org/docs/fr/project-management/dataformat.html).{% endhint %}

Pour simplifier l'affichage, la couche **emprise** sera non identifiable et non interrogeable ; en gros, elle n'est là qu'en fond d'écran. Pour cela, aller dans les Projet > Propriétés > Sources de données. 

![paramétrer les sources de données](images/qfield_01.png) 

Pour simplifier la saisie sur le terrain, certains champs des couches poly et isolats sont paramétrés avec des listes de valeurs et des cases à cocher.

**couche poly **

| Nom du champ | Paramètres                      |
| ------------ | ------------------------------- |
| type         | liste de valeurs (US ou fait)   |
| description  | texte libre avec catégorisation |
| fouille      | case à cocher                   |
| releve       | case à cocher                   |
| topo         | case à cocher                   |

**couche isolats**

| Nom du champ | Paramètres                              |
| ------------ | --------------------------------------- |
| type         | liste de valeurs (alti, objet) éditable |
| commentaire  | texte libre                             |



Si vous avez réalisé des **thèmes** de carte, QField peut les retenir. 



### 2.2. Export du projet vers QField

#### 1. Commencer par configurer le projet depuis le menu Extension



![Extension QField Sync](images/qfield_02.png)   



Pour les couches issues de flux (OpenStreetMap, WMS, WFS, PostGIS), il n'y a pas beaucoup de choix, on laisse donc "Aucune action".

Pour les couches vecteur, plusieurs actions sont possibles :

* copier 
* conserver l'existant (copier si absent)
* édition hors-ligne 
* supprimer

Il est conseillé d'utiliser "édition hors-ligne". Une copie de travail de la couche est faite dans le répertoire du paquet. Chaque modification réalisée dans le projet empaqueté durant le travail est enregistré dans un fichier de journalisation. Plus tard, lors de la synchronisation-retour, ce fichier sera relancé et tous les changements appliqués aussi à la base de données principale. Il n’y a pas de conflit de traitement.

Cocher "Créer un fond de carte" pour obtenir le raster de l'OpenStreetMap. 

![configurer les couches](images/qfield_03.png)  



#### 2. Réaliser les paquets pour QField

Une fois la configuration terminée, il est temps de réaliser les paquets pour QField.

Cliquer sur la première des deux icônes de la barre d'outils QFieldSync : ![synchronisation QField](images/qfield_04.png) 

Sélectionner le répertoire d'export ; celui proposé par défaut fonctionne très bien. L'étendue de la carte d'affichage peut être sélectionnée à partir du canevas principal. Enfin, cliquer sur **Créer**.

![etendue carte](images/qfield_05.png)  



#### 3. Copier le dossier d'export dans l'appareil mobile

Brancher l'appareil mobile à l'ordi et copier le dossier à la racine de la mémoire de stockage interne. 



## 3. Utiliser QField sur le terrain

### 3.1. Ouvrir QField et charger le projet

![ouvrir QField](images/qfield_06.png) 

Choisir : *Open local project* à la première utilisation. Par la suite, l'application liste les projets récents.

Sélectionner un projet QGIS depuis le stockage interne

![ouvrir un projet dans le stockage interne](images/qfield_07.png) 



Sélectionner le dossier qui contient le projet empaqueté, ici : projet_2020_QField



La carte s'affiche et trois icônes sont disponibles : 

* en bas à droite, l'icône de positionnement
* en haut à droite, l'outil de recherche
* en haut à gauche, le menu rapide

![accueil projet](images/qfield_08.png) 

#### Menu rapide

![menu rapide](images/qfield_09.png) 

Ce menu donne la **liste des couches** et affiche la légende (exemple ici avec la catégorisation sur la couche poly)

On peut sélectionner la couche pour passer en **mode édition** avec le crayon en haut à droite.

L'icône des **paramètres** permet de :

* ouvrir un projet
* ouvrir les paramètres de l'application et notamment l'affichage de l'échelle graphique (scale bar)
* utiliser l'outil de mesure 

![outil mesure](images/qfield_10.png) 



Il est possible de configurer des **thèmes** de cartes sur le projet et de les afficher dans QField à partir du menu rapide. 



### 3.2. Interroger les données existantes

Sélectionner l'entité, une fenêtre s'affiche en bas et liste les résultats pour toutes les couches. 

![sélectionner des entités](images/qfield_11.png) 

On obtient les informations issues de la table attributaire.

![sélectionner des entités](images/qfield_12.png) 

Utiliser les flèches pour faire défiler les entités sélectionnées.

Cliquer sur le bouton centrer pour centrer la carte sur l'entité sélectionnée

Cliquer sur le bouton éditer pour modifier les attributs de l'entité sélectionnée





### 3.3. Editer les données 

#### Activer le GPS

Activer le GPS sur le smartphone : Menu > Paramètres > Personnel > Localisation > Activer. Choisir le mode Haute précision.

Sur QField, le bouton de positionnement est en bas à droite. S'il est grisé, c'est que le positionnement est désactivé. Toucher le bouton une fois pour l'activer, il passe en bleu. Pour accéder aux détails, faire un appui long sur le bouton. On a alors plusieurs options :

- Enable Positioning : si c'est décoché, on n'a pas accès à la localisation
- Center to Current Location : centrer la carte sur l'endroit où l'on est
- Show Position Information : donne les coordonnées et la précision

![positionnement sur le smartphone](images/qfield_13.png)  



#### Ajouter un point

Passer en mode édition sur la couche à modifier (ici isolats), déplacer la carte pour que le petit cercle de localisation et la croix au centre de l'écran se superposent. Puis à droite cliquer sur le bouton + pour ajouter un point.

![ajouter un point](images/qfield_14.png) 

La table d'attributs s'ouvre et vous pouvez alors la renseigner. 

![renseigner la table attributaire](images/qfield_15.png) 

Si vous avez activé la localisation, vous pouvez utiliser le bouton **Centrer** (sur fond circulaire noir) pour forcer le centrage de votre position sous la croix. 

Pour des lignes et des polygones, il faut suivre le même procédé en ajoutant des nœuds avec le bouton + et se déplacer entre chaque nœud. Cliquer sur la coche verte pour valider la création et remplir le formulaire d'attributs. 

 ![ajouter un polygone](images/qfield_16.png)



#### Modifier des entités existantes avec les cases à cocher

Ceci peut être utile pour valider rapidement des champs dans son projet, par exemple pour indiquer si un vestige a été fouillé, relevé et/ou topographié.

Sélectionner la couche à interroger, sélectionner l'entité et passer en mode édition via la fenêtre qui s'est affichée. Cocher la ou les cases concernées et valider, le mode édition est quitté automatiquement. 

![cases à cocher](images/qfield_17.png) 



La **suppression** d'entités est possible en mode édition : sélectionner l'entité puis appuyer sur le bouton corbeille dans la liste. 

![supprimer une entité](images/qfield_18.png) 



## 4. Retour au bureau

Commencer par re-ouvrir le projet dans QGIS (celui que vous avez sauvegardé précédemment avec Enregister Sous … ).

Copier le dossier du projet depuis l'appareil mobile sur l'ordinateur (par exemple dans un dossier import) et utiliser **Synchroniser depuis QField** avec la 2e icône de la barre d'outils![paquet](images/qfield_04.png)pour synchroniser les changements depuis le projet portable vers le projet principal.

![synchronisation retour](images/qfield_19.png) 