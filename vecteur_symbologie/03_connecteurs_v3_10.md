Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - équipe Formateurs & Référents SIG

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# Les connecteurs d'étiquettes (lignes de renvoi) dans QGIS

Les connecteurs d'étiquettes dans QGIS sont des lignes générées automatiquement entre l'étiquette et l'entité étiquetée. Ceux-ci sont utiles lorsque la densité d'entités est importante et gène l'étiquetage. Depuis la version 3.10 de QGIS, les connecteurs d'étiquettes sont nativement présent dans le logiciel.  
Les paramètres sont accessibles depuis un nouvel onglet ***Connecteurs*** dans le menu ***Etiquettes*** des ***Propriétés*** de la couche à étiqueter ou depuis le panneau de ***Style de Couche***.

![Menu ***Connecteurs***](images/connecteur_013.png) 



## 1. Activer la présence de connecteurs

1. Cocher la case ***Dessiner les connecteurs***.
2. Il s'agit ensuite de définir le style de connecteur. Par défaut, QGIS dessine une ***ligne simple*** mais un autre style ***Ligne angle droit*** est proposé.
3. Le symbole de la ligne est totalement paramétrable comme n'importe quel symbole.



## 2. Paramètres supplémentaires

4. QGIS propose un mode automatique de dessin du connecteur en fonction d'une distance minimale entre l'étiquette et l'entité. A partir de cette distance, la ligne de connecteur est dessinée. Ce menu permet de définir cette distance selon l'unité définie par l'utilisateur.

5. Ce paramètre permet de déplacer l'extrémité de la ligne de connecteur par rapport à l'entité (choix de l'unité possible).

6. Ce paramètre permet de déplacer l'extrémité de la ligne de connecteur par rapport à l'étiquette (choix de l'unité possible).

7. Cette option permet de dessiner autant de connecteurs que de parties pour une entité multi-parties.

8. Ce menu déroulant permet de définir à quel endroit la ligne de connecteur doit s'ancrer sur l'entité.  

   a. ***Pôle d'inaccessibilité*** : ancre le connecteur sur l'entité depuis un emplacement d'étiquette sans conflit  
   b. ***Point sur le pourtour*** : ancre le connecteur sur le pourtour d'un polygone à la distance minimale par rapport à l'étiquette (équivalent du style défini dans le § précédent.  
   c. ***Point dans la surface*** : ancre le connecteur au centre de l'entité et de manière obligatoire dans la surface du polygone.
   d. ***Centroïde*** : ancre le connecteur au centroïde géométrique de l'entité.

9. La boîte de ***Définition imposée par les données*** permet également de conditionner le dessin de connecteurs selon les données saisies dans la table attributaire. Il est donc possible d'ici d'appeler le champ *"auxiliary_storage_user_custom_connecteur"* de type booléen créé dans le premier paragraphe de ce guide.

![Paramétrage de la boîte de ***Définition imposée par les données***](images/connecteur_014.png) 

10. Les autres boîtes de ***Définition imposée par les données*** sont également disponibles pour l'ensemble des paramètres des connecteurs.



## 3. Exemple de réalisation de figure avec connecteurs d'étiquettes

Ci-après un exemple de figure réalisée dans QGIS avec utilisation des lignes de connecteurs d'étiquettes. Cette exemple est issue du rapport de diagnostic de Beauce-la-Romaine (41) dirigée par Edith Rivoire. La carte a été réalisée par Thomas Guillemard en 2019.

![Exemple de figure avec connecteurs d'étiquettes](images/connecteur_015.png)

