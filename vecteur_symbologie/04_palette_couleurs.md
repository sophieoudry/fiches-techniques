Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - équipe Formateurs & Référents SIG

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# Ajouter une palette de couleurs prédéfinies

Dans QGIS, on peut ajouter des palettes de couleurs prédéfinies qui permettent de choisir une couleur
plus rapidement qu’avec la roue chromatique : ces couleurs apparaissent alors sous les *Couleurs
récentes.*

![couleurs récentes](images/palette_couleur_01.png) 



## 1. Télécharger une palette de couleurs

Télécharger le [fichier compressé avec les tables de couleurs](zip/table_couleurs_qgis.zip).

Pour y accéder aisément par la suite, enregistrer cette palette dans le dossier .qgis3

```
C:\Users\votresession\AppData\Roaming\QGIS\QGIS3\profiles\default\palettes
```

:heavy_check_mark:  Si vous avez créé votre profil utilisateur, pensez à utiliser celui-là plutôt que le profil par défaut.



## 2. Importer une palette de couleurs dans QGIS



Pour importer une palette, passer par le menu *Préférences > Options > Couleurs*  

Dans la fenêtre principale, si des couleurs sont déjà présentes, il faut toutes les sélectionner
(Ctrl + A), puis les supprimer.
Cliquer sur le bouton dossier *Importer les couleurs depuis le fichier*

![options des couleurs](images/palette_couleur_02.png) 

Sélectionner l'un des fichiers **.pgl** téléchargé plus haut. 

Le contenu de la palette apparaît dans la fenêtre Couleurs parmi les couleurs standards : on peut
cliquer sur OK.

Recommencer pour l'autre fichier.



![palette](images/palette_couleur_03.png) 

Quand on retourne dans les outils de choix de couleurs, on a alors une large palette prédéterminée : cette palette sera disponible dans le logiciel partout où l’on peut trouver l’outil de sélection de couleur.



![palette](images/palette_couleur_04.png)  



## 3. Importer des palettes depuis ColorBrewer

Le site [ColorBrewer](https://colorbrewer2.org/#type=sequential&scheme=YlOrRd&n=4) vous permet de créer vos propres palettes en fonction d'un nombre de classes, de la nature de vos données et d'une gamme de couleurs. 

- Développer l'onglet EXPORT et choisir "GIMP color palette", puis enregistrer le fichier .gpl

![export gpl](images/palette_couleur_05.png) 

- Importer dans QGIS en suivant la manipulation présentée plus haut. 