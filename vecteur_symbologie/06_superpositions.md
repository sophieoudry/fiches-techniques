Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - équipe Formateurs & Référents SIG

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# Gérer simplement les superpositions de vestiges



## 1. Créer un champ pour la gestion

- dans la couche des vestiges, créer un champ *ordre* (type nombre entier, longueur 1, valeur pour tout le monde "0")
- dessiner les nouvelles entités et leur attribuer la valeur "-1", "0" ou "1" en fonction des superpositions selon l'ordre : -1 en dessous, puis 0, puis 1



## 2. Adapter la symbologie

- Dans les propriétés de la couche, onglet *Symbologie - Rendu de couche*, cocher *Contrôle de l'ordre de rendu des entités*, puis cliquer sur l'icône à droite pour ouvrir la boîte de dialogue.

![rendu de couche](images/superpositions_01.png) 



<img src="images/superpositions_02.png" alt="symbole à cliquer" style="zoom:150%;" />

- Sélectionner le champ *ordre*, puis choisir *Ascendant*.

![ordre de rendu](images/superpositions_03.png)



