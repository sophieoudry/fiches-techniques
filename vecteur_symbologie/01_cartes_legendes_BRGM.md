> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.22 de QGIS
Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)

Inrap - formateurs SIG

# Accéder aux cartes géologiques et à leur légende en format vectoriel



Le [BRGM a mis à disposition](https://www.brgm.fr/fr/actualite/actualite/science-ouverte-brgm-donne-acces-gratuit-cartes-geologiques) les cartes géologiques au 1/50 000 vectorisées et harmonisées au format .shp [(Bd Charm-50)](http://infoterre.brgm.fr/formulaire/telechargement-cartes-geologiques-departementales-150-000-bd-charm-50)

{% hint style='info' %}
Pour rappel, les données du BRGM sont utilisables pour un usage scientifique, technique et pédagogique. On n'oublie pas de citer ses sources (c'est pas parce que c'est gratuit qu'on peut s'en dispenser) !
{% endhint %}

## 1. Télécharger les cartes et fichiers de style pour son département
* Dans l'adresse ci-après, remplacer "XXX" par le numéro de votre département. 
http://infoterre.brgm.fr/telechargements/BDCharm50/GEO050K_HARM_XXX.zip
Par exemple, pour télécharger les données de la Corrèze le lien est :
http://infoterre.brgm.fr/telechargements/BDCharm50/GEO050K_HARM_019.zip


* Télécharger les fichiers de motifs pour la symbologie des cartes géologiques ici : http://infoterre.brgm.fr/telechargements/BDCharm50/Outils.zip

* Dézipper les deux fichiers

## 2. Appliquer les styles (à ne faire qu'à la première utilisation)

Le protocole est détaillé dans une vidéo du BRGM disponible sur YouTube
[Installation des motifs pour la symbologie des cartes géologiques dans QGIS](https://www.youtube.com/watch?v=atkI_MLkrX0)

* Dans **Windows**, copier le contenu du dossier Point_Symbols dans le dossier C:\Windows\Fonts

	![ajout de police](images/brgm_01.JPG)

* Dans **QGIS** aller dans le menu *Préférence > Options >* Onglet *Système*
	![onglet système de qgis](images/brgm_02.JPG)

	Dans la zone "chemin SVG" cliquez sur l'icone ![icône plus](images/brgm_03.jpg)

	Ajouter le chemin vers le dossier Line_Symbols.
	
	Procéder de la même manière pour ajouter le lien vers le dossier Polygons_Patterns
	
	![ajout du lien vers dossier polygon](images/brgm_04.JPG)
	
	> Afin de pérenniser le lien vers les dossiers Outils du BRGM, ils peuvent au préalable être enregistrés dans un dossier qui regroupe les symbologies contenu dans le dossier de votre profil *Default* ou *Utilisateur*.
	> Généralement situé à l'adresse : C:\Users\\*votrenomdesession*\AppData\Roaming\QGIS\QGIS3\profiles\default



## 3. Chargement des cartes géologiques dans QGIS 

Le descriptif des cartes géologiques en .pdf est contenu dans chaque dossier de cartes géologiques téléchargés.

Les formations géologiques sont contenues dans la couche GEO050K_HARM_XXX_S_FGEOL_2154.shp

L'ensemble des cartes géologiques s'ouvrent dans QGIS en les ajoutant comme fichier vecteur. La symbologie et la légende sont disponibles.

![exemple de carte terminée](images/brgm_05.JPG)