Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - équipe Formateurs & Référents SIG

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.4 de QGIS



# Les connecteurs d'étiquettes (lignes de renvoi) dans QGIS

Les connecteurs d'étiquettes dans QGIS sont des lignes générées automatiquement entre l'étiquette et l'entité étiquetée. Ceux-ci sont utiles lorsque la densité d'entités est importante et gène l'étiquetage. Comme toujours dans QGIS, plusieurs options permettent d'aboutir au même résultat.
Si vous utilisez la version 3.10 ou postérieure =>  [connecteurs d'étiquettes QGIS v.3.10](https://formationsig.gitlab.io/fiches-techniques/vecteur_symbologie/03_connecteurs_v3_10.html)

![Exemple de connecteurs d'étiquette](images/connecteur_001.png) 



## 1. Préparation de la couche d'étiquettes

### 1.1. Création d'un duplicata d'étiquettes

Le symbole d'étiquette va porter sur un **duplicata** de la couche à étiqueter. Cette manipulation permet de gérer l'emplacement des étiquettes  selon une échelle donnée.

{% hint style='working' %} **Rappel** : Le duplicata est une occurrence de la même couche. Les modifications faites dans une des occurrences de la couche se répercutent à toutes les autres.  
Pour créer un duplicata, il suffit de faire un ***clic-droit*** sur la couche à étiqueter et de choisir l'option ***Dupliquer la couche***. {% endhint %}



### 1.2. Création du champ de stockage auxiliaire pour les connecteurs

Dans tous les cas, QGIS doit être en mesure de différencier le symbole entre les entités étiquetées avec ou sans connecteur. Un champ de présence/absence (booléen) est donc nécessaire. **Ce champ ne doit pas être créé dans la table de la couche source pour éviter de polluer les données avec des informations non scientifiques.**  
Nous conseillons l'utilisation d'un champ de stockage auxiliaire.

{% hint style='danger' %}**Attention** : Le stockage auxiliaire ne fonctionne correctement qu'avec une clé primaire dans la couche concernée. Si aucune clé primaire n'existe il faudra la créer dans un nouveau champ (peut être appelé *"pk"*). La calculatrice de champ permet de calculer un incrément automatiquement avec l'expression `$id` ou avec la variable `@row_number`.{% endhint %}

* Si ce n'est pas déjà fait, ***Créer*** la base de données de Stockage Auxiliaire dans les ***Propriétés*** du duplicata, onglet ***Stockage auxiliaire*** :

![Création de la base de données de stockage auxiliaire](images/connecteur_002.png)
* Sélectionner le champ de **clé primaire** :

![Sélection du champ de clé primaire](images/connecteur_003.png)
* Ajouter un nouveau champ, que nous pouvons nommer *"connecteur"* , typé en entier. Ce champ pourra être complété par un 0 lorsqu'un connecteur n'est pas nécessaire et un 1 lorsqu'il c'est le cas.

![Création du champ *"connecteur"* de stockage auxiliaire](images/connecteur_004.png)



Ce champ de stockage auxiliaire apparaît désormais dans la table du duplicata d'étiquette dans la colonne *auxiliary_storage_custom_connecteur*. Il présente ainsi l'avantage d'être complété facilement sur une sélection active. 

![Table de la couche d'étiquettes complétée d'un champ *"connecteur"* de stockage auxiliaire](images/connecteur_005.png)



:bangbang:  Pour que le champ booléen de présence/absence fonctionne correctement, toutes les valeurs des lignes du nouveau champ doivent être complétées (soit par 0, soit par 1). Dans un premier temps, remplir cette colonne de la la valeur 0. 

{% hint style='tip' %} **Rappel** : Deux outils sont disponibles pour remplir un champ rapidement en mode ***Edition*** :  

* L'outil ***Calculatrice de champ*** (1)
* L'outil de ***Mise à jour de valeurs*** (2). Sélectionner tout d'abord le champ à mettre à jour, puis la valeur, puis appuyer sur le bouton ***Tout mettre à jour***. 

![Icône de l'outil ***Calculatrice de champ*** et de l'outil de mise à jour](images/connecteur_006.png){% endhint %}



### 1.3. Création des champs de stockage auxiliaire pour le placement des étiquettes

Avant de passer à la création du connecteur à proprement dite, les fonctionnalités de stockage auxiliaire vont nous permettre de générer les champs X et Y de placement d'étiquettes. QGIS peut les créer automatiquement :  

***Propriétés*** du duplicata d'étiquettes / Onglet ***Etiquettes*** / Activer le mode d'étiquetage ***Etiquettes simples*** / Onglet ***Position*** / sous-menu ***Donnée définie*** / Appuyer sur ***Définition de données imposées*** (le bouton de menu blanc) pour la Coordonnée X / ***Stocker les données dans le projet***. Répéter l'opération pour la Coordonnée Y.

![Création automatique des champs de stockage auxiliaire pour le placement des étiquettes](images/connecteur_007.png)



Les boîtes de ***Définition de données imposées*** deviennent jaunes et les deux champs de placement d'étiquettes *"labelling_positionx"* et *"labelling_positiony"* ont été ajoutés à la table par QGIS.



## 2. Utilisation du ***Générateur de géométrie*** dans la ***Symbologie*** de la couche d'étiquettes

Cette procédure peut être mise en œuvre dans QGIS dans toutes les versions du logiciel.

{% hint style='tip' %} **A propos** : Le ***Générateur de géométrie***  

Le ***Générateur de géométrie*** est un type de symbole dans QGIS qui permet à l'utilisateur de créer, à partir d'une couche possédant une géométrie définie (point, polyligne, polygone) une ou plusieurs entités géométriques pouvant être de natures différentes à partir d'expression en SQL.{% endhint %}

1. Avant de modifier le symbole sur le duplicata d'étiquette, déclarer un nouveau ***Style***. On peut le nommer *Connecteur*. 

![Déclarer un style *Connecteur* sur le duplicata d'étiquettes](images/connecteur_008.png)

2. Dans les ***Propriétés*** du duplicata d'étiquettes ou dans le panneau de ***Styles de couche***, onglet ***Symbologie***, définir le type de symbole en ***Ensemble de règles***.

3. Créer deux règles :  
  a. La première concerne les entités pour lesquelles aucun connecteur n'est nécessaire.  

  b. Paramètres : Filtrer avec l'expression ` "auxiliary_storage_user_custom_connecteur" = 0` et le symbole sélectionné ne doit posséder ni remplissage, ni trait

![Paramètres du symbole pour les étiquettes sans connecteur](images/connecteur_009.png)

b. La seconde concerne les entités pour lesquelles un connecteur est nécessaire. C'est pour cette règle que le générateur de géométrie entre en scène.
* Filtrer la règle d'étiquetage avec l'expression ` "auxiliary_storage_user_custom_connecteur" = 1` (1)
* Changer le ***Type de symbole*** / ***Remplissage simple*** par ***Générateur de géométrie*** (2)
* Sélectionner le ***Type de géométrie*** en ***Polyligne*** (3)
* Saisir l'expression SQL (4):
```sql
make_line(closest_point($geometry,make_point("auxiliary_storage_labeling_positionx","auxiliary_storage_labeling_positiony")),make_point("auxiliary_storage_labeling_positionx","auxiliary_storage_labeling_positiony"))
```

{% hint style='tip' %}**A propos** : L'expression du ***Générateur de géométrie*** 

L'expression ci-dessus peut être décomposée selon les fonctions qui la compose.  

`make_line` : il s'agit de la fonction qui demande la création d'une polyligne. Entre parenthèse figurent les arguments de la fonction, c'est-à-dire la liste des points qui constituent la ligne.  

`closest_point` : cette fonction permet de comparer plusieurs géométries et de choisir la position qui réduit au maximum la distance entre deux objets. Pour le cas présenté ici, la distance la plus faible entre la géométrie de l'entité à étiqueter (`$geometry`) et les coordonnées de l'étiquette. 
`make_point` : il s'agit de la fonction de création d'un point. Les arguments permettent de définir les coordonnées du point.  

La fonction complète peut donc se résumer ainsi : trace une ligne (`make_line`) dont le premier point est celui qui réduit au maximum la distance (`closest_point`) entre l'entité à étiqueter (`$geometry`) et les coordonnées de l'étiquette (`make_point("auxiliary_storage_labeling_positionx","auxiliary_storage_labeling_positiony")`) et le deuxième point correspond aux coordonnées de l'étiquette (`make_point("auxiliary_storage_labeling_positionx","auxiliary_storage_labeling_positiony")`).  Il n'est pas nécessaire d'ajouter les formules de politesses ("cher QGIS, s'il-te-plaît" et "Merci QGIS"), elles sont induites dans le SQL.{% endhint %}

* Il est ensuite possible de déterminer le symbole pour le connecteur (couleur, aspect et épaisseur dans la rubrique ***Ligne simple*** du symbole (5).

![Paramètres du symbole pour les lignes avec connecteur](images/connecteur_010.png) 

4. Définir l'alignement des étiquettes pour les adapter à la position par rapport à l'entité à étiqueter.  

  En fonction de la position de l'étiquette, la ligne du connecteur se placera soit à droite (right), soit à gauche (left), soit en bas (bottom), soit en haut (top) de celle-ci. Selon les coordonnées de départ et d'arrivée de la ligne du connecteur, QGIS adapte l'alignement.

![Paramètres des alignements des étiquettes](images/connecteur_011.png)



a. Dans les ***Propriétés*** du duplicata / ***Etiquettes*** / ***Emplacement*** / ***Données définies*** / ***Alignement horizontal*** / ***Définition de données imposées*** / ***Editer***, saisir l'expression SQL suivante :
```sql
if(X(closest_point($geometry,make_point("auxiliary_storage_labeling_positionx","auxiliary_storage_labeling_positiony")))>
X(make_point("auxiliary_storage_labeling_positionx","auxiliary_storage_labeling_positiony")),'Right','Left')
```

b. Dans les ***Propriétés*** du duplicata / ***Etiquettes*** / ***Emplacement*** / ***Données définies*** / ***Alignement vertical*** / ***Définition de données imposées*** / ***Editer***, saisir l'expression SQL suivante :
```sql
if(Y(closest_point($geometry,make_point("auxiliary_storage_labeling_positionx","auxiliary_storage_labeling_positiony")))<Y(make_point("auxiliary_storage_labeling_positionx","auxiliary_storage_labeling_positiony")),'Bottom','Top')
```
{% hint style='tip' %}**A propos** : L'expression d'alignement des étiquettes  

De la même manière que le générateur de géométrie, ces expressions s'appuient sur la fonction `closest_point`.  La première expression concerne l'alignement horizontal signifie : si (`if`) la coordonnée X du départ de la ligne de connecteur tracé selon la distance minimale entre l'entité et l'étiquette (`X(closest_point($geometry,make_point("auxiliary_storage_labeling_positionx","auxiliary_storage_labeling_positiony"))`) est supérieure (`>`) à la coordonnée X de l'étiquette (`X(make_point("auxiliary_storage_labeling_positionx","auxiliary_storage_labeling_positiony"))`) alors, l'alignement se fera à droite (`,'Right'`) sinon, il se fera à gauche (`,'Left'`)  

La deuxième expression concerne l'alignement vertical et reprend la même idée mais avec la coordonnée Y : si (`if`) la coordonnée Y du départ de la ligne de connecteur tracé selon la distance minimale entre l'entité et l'étiquette (`Y(closest_point($geometry,make_point("auxiliary_storage_labeling_positionx","auxiliary_storage_labeling_positiony"))`) est inférieure (`<`) à la coordonnée Y de l'étiquette (`Y(make_point("auxiliary_storage_labeling_positionx","auxiliary_storage_labeling_positiony"))`) alors, l'alignement se fera en bas (`,'Bottom'`) sinon, il se fera en haut (`,'Top'`). {% endhint %}



Et voilà le résultat :  ![Résultat des paramètres définis pour les connecteurs d'étiquettes](images/connecteur_012.png)



:heavy_exclamation_mark: On peut reproduire facilement ces paramètres pour d'autres couches. Seules les étapes décrites dans le § *Préparation de la couche d'étiquettes* seront à exécuter avant de charger le fichier de style sur la couche.
