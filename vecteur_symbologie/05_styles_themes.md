Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - S. Oudry, C. Font

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# Paramétrer des styles et des thèmes

:warning:  les manipulations sont faites ici à partir de la version 3.10 de QGIS. Elles sont réalisables avec des versions plus anciennes, sachez seulement que dans les versions 2.x, le "thème" s'appelle un "réglage" et la "mise en page" est le "composeur d'impression".



## 1. Pourquoi paramétrer des styles ? 

Un style est l'ensemble d'éléments de symbologie liés à une couche. Par exemple, pour une emprise, vous pouvez avoir un premier style avec un fond transparent, un contour noir en tiret-point et une étiquette indiquant la surface. Vous pouvez avoir un deuxième style avec le même fond mais avec un contour rouge continu et sans étiquette.

Chaque style permet de représenter la même couche différemment, selon ce que vous avez à montrer et en l'adaptant si nécessaire à l'échelle prévue pour la figure. 

Paramétrer la symbologie d'une couche prend du temps et l'utilisation d'un style vous permet de ne pas recommencer ces paramétrages puisque le style est mémorisé dans le projet. 



## 2. Créer et utiliser un style 

### 2.1. Créer un style

Attention ! les modifications de styles sont mémorisées en continu et toute modification dans un style actif  écrase les paramètres précédents.

Pour créer un style : clic droit sur une couche - *Propriétés* ; en bas vous avez un menu *Style*

![menu style](images/styles_01.png) 

- Cliquer sur *Ajouter...* Il faut commencer par donner un nom, essayez d'être parlants : exemple "fond transparent, contour rouge continu". 

Le point noir devant le nom indique que c'est le style actif.

- modifier le style pour qu'il corresponde : ici, fond transparent et contour rouge en continu

Vous pouvez utiliser des styles pour des symbologies plus élaborées comme des catégorisations.



### 2.2. Naviguer d'un style à l'autre

Pour passer rapidement d'un style à l'autre, clic droit sur la couche - *Styles* et choisir le style voulu.

![passer d'un style à l'autre](images/styles_02.png) 



Il est toujours possible de revenir au style par défaut, celui que QGIS avait choisi. 



## 3. Utiliser des thèmes 

### 3.1. Les fondamentaux des thèmes

Un thème est le regroupement de couches cochées et des styles associés. Les thèmes sont accessibles par l'icone en forme d'œil en haut du panneau des Couches. 

{% hint style='danger' %}

Il n'est pas nécessaire d'avoir un style différent pour chaque thème : un même style peut être utilisé dans différents thèmes. {% endhint %}

![menu des thèmes](images/styles_03.png) 

Chaque thème servira pour une figure ; pour cela utiliser un numéro qui sera celui de la figure et essayer d'être explicite dans le nom. Les thèmes sont rangés par ordre de création et non par ordre alpha-numérique. 

Les thèmes sont enregistrés dans le projet et ne peuvent donc pas être réutilisés dans un autre projet. 

#### Créer un thème

Cocher les couches qui seront affichées et leur appliquer le style voulu. 

Dans le menu des Thèmes, *Ajouter un thème...* et lui donner un nom. 



#### Modifier un thème

On peut supprimer un thème ou le remplacer si on a fait des modifications. 



### 3.2. Utiliser les thèmes pour créer des cartes

- Créer une mise en page et ajouter une carte. 

- Dans le sous-menu Couches, cocher *suivre le thème de la carte* et choisir le thème

![suivre le thème de la carte](images/styles_04.png) 

Il ne vous reste plus qu'à compléter votre mise en page et exporter votre carte. 



Si le thème est modifié dans la fenêtre principale du projet, les modifications seront répercutées dans les mises en page.