Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - équipe Formateurs & Référents SIG

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# L'accrochage et la topologie

Les options d'accrochage sont des aides au dessin et permettent de respecter la topologie lors de la numérisation. L'ensemble des paramètres est disponible dans la barre d'outils Accrochage, accessible par un clic-droit dans les menus. Préférez le mode d'accrochage **Avancé** pour avoir accès à toutes les options.

![barre d'outils d'accrochage](images/accrochage_01.png)

1. Activer/désactiver l'accrochage
2. Sélectionner les couches sur lesquelles activer l'accrochage
3. Par défaut, il est actif sur **toutes** les couches
4. Possible de l'activer sur la couche **active**
5. Possible de sélectionner les couches sur lesquelles l'accrochage est actif et leurs paramètres  d'accrochage respectif en mode *Configuration avancée*
6. Possible d'ouvrir dans une fenêtre à part les options évoquées au point précédent
7. En mode *Configuration avancée*, ce bouton permet d'accéder aux paramètres pour chacune des couches du projet
8. Activer/désactiver l'Edition topologique. Cette option permet de conserver les éventuelles relations topologiques existantes entre plusieurs entités si l'une d'entres elles est modifiée
9. Activer/désactiver l'accrochage sur les intersections
10. Option Activer le tracé. Cet outil, très utile, permet, lorsqu'il est actif de suivre un tracé existant (reproduction des segments et sommets rencontrés) sur les couches pour lesquelles l'accrochage est actif
11. Permet d'activer l'accrochage couche par couche
12. Permet de sélectionner sur quelle géométrie se fait l'accrochage : sommet, segment ou les deux
13. Rayon de tolérance (sous lequel l'accrochage se fait automatiquement) de l'accrochage
14. Unité du rayon de tolérance (pixels : indépendant de l'échelle de la carte, mètre : dépendant de l'échelle de la carte)
15. Option Eviter les intersections

Penser à cocher ou à décocher cette option avant de dessiner ou vous pourriez avoir des surprises.
Attention également aux relations stratigraphiques.



{% hint style='danger' %} L'extension **Bezier Editing** ne respecte pas la contrainte *Eviter les intersections*. Mais l'option *Eviter les intersections* peut toutefois s'appliquer pour toutes les entités déjà dessinées et ce, quel que soit l'outil utilisé pour les dessiner (Bezier Editing inclus). Il suffit de couper/coller l'entité sélectionnée pour laquelle on souhaite éviter la superposition. {% endhint %}



## Mode et tolérance d'accrochage

Le mode et la tolérance d'accrochage sont des options indispensables pour la numérisation.
Nous utiliserons le mode d'accrochage sur les sommets et les segments des zones existantes avec une tolérance de 12 pixels. Ceci correspond à un bon compromis entre l'accrochage intempestif (si vous  augmentez le nombre de pixels) et l'absence d'accrochage (si vous diminuez le nombre de pixels).



{% hint style='tip' %} **Quelle est la différence entre unité de carte ou pixel ?**

Exemple : Un trait de 10 unités de carte fera toujours 10 m sur la carte (si le projet est paramétré en mètres), et sa taille à l’écran variera donc en fonction de l’échelle d’affichage et de sortie. En revanche, un trait de 10 pixels aura toujours la même taille à l’écran, quelle que soit l’échelle d’affichage du projet. {% endhint %}