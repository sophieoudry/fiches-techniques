Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - équipe Formateurs & Référents SIG

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# Réaliser un dessin de pierre à pierre dans QGIS



## 1. Mise en place

Pour commencer, il faut : 

- une image du mur géoréférencé : minute de terrain scannée et géoréférencée ou orthophotographie.
- une couche vecteur de ligne vierge. Cette nouvelle couche doit être positionnée au-dessus de la couche raster et doit être dans le bon SCR. 



## 2. Principes

Après cette mise en place, nous allons **tracer le contour des pierres**, mortier et autres éléments sur la couche de ligne. Puis on appliquera un **géotraitement** pour transformer les formes délimitées par les lignes en polygones. 

Il faudra alors travailler sur la table attributaire de la nouvelle couche de polygones, créée par le géotraitement, pour définir les identifiants des structures et les indications de symbologie. Ces dernières seront nécessaires pour différencier les composants du mur. 

{% hint style='info' %} L'exemple donné ici est celui d'un mur, mais ça fonctionne aussi pour d'autres types de vestiges, comme un ossuaire ou une sépulture collective. {% endhint %}



## 3. Le tracé

Se mettre en mode édition sur la couche de ligne et tracer le contour. Il s'agit de tracer chaque limite entre des éléments en prenant soin de faire dépasser le trait pour qu'il croise bien une autre limite et qu'il ferme ainsi une forme cohérente. 

{% hint style='tip' %} **Quel outil de numérisation utiliser ? **

Basiquement, on peut utiliser l'outil Créer une ligne mais d'autres outils sont possibles comme celui de l'extension Bezier Editing. {% endhint %} 



### 3.1. Principe de base du dessin

Le tracé revient sur lui-même pour que la forme soit fermée. Si une forme n'est pas fermée, elle ne sera pas prise en compte.

![pap 01](images/pap_01.jpg) ![pap 02](images/pap_02.jpg) 

A droite, le polygone créé après géotraitement. Les traits qui dépassent ont été effacés par le traitement. Chaque forme fermée sert de base à un polygone.



{% hint style='working' %} Conseil : lors du dessin de chacune de ces lignes, comme il s'agit d'entités nouvelles, la fenêtre de saisie des attributs va apparaître très souvent. Vous pouvez la supprimer le temps de réaliser les tracés via le menu *Préférences - Options - Numérisation* et dans le sous-menu *Création d'entité*, cocher *Supprimer la fenêtre de saisie des attributs lors de la création de chaque nouvelle entité*.

Pensez à la réactiver après, une entité sans donnée attributaire, ça ne sert pas à grand chose. {% endhint %}



Le tracé peut être fait d’autant de lignes que l’on veut tant que les traits se croisent pour fermer et délimiter la forme définitive souhaitée.

![pap 03](images/pap_03.jpg)





Pour les recouvrements de pierres, il suffit de faire dépasser les traits dans l'une des formes. Vous dessinerez en premier la pierre qui est visible en entier, puis celles qui sont dessous. Au moment du passage en polygones, la superposition sera respectée. 

![pap 04](images/pap_04.jpg)

Ne pas oublier de faire des traits au niveau des interstices entre les pierres pour limiter la forme du mortier
lui-même. Même pour un mur en pierres sèches ou monté à la terre, il peut être intéressant de matérialiser (limiter) la structure interne du mur, pour des questions de topologie (limites/emprise réelle du mur). Ces éléments internes peuvent ne pas être représentés par la suite si besoin.

![pap 05](images/pap_05.jpg) 

Les polygones créés après géotraitement :

![pap 06](images/pap_06.jpg) 

### 3.2. Deux erreurs communes

#### Oublier un des traits

Oublier un des traits qui devrait fermer une forme ou ne pas la fermer complètement. Cela arrive typiquement avec les lignes de limite du mortier, entre les moellons. L’aspect répétitif de ces tracés et leur petite taille prédisposent à l’oubli. Il arrive également qu’en traçant trop rapidement un segment, on ne  ferme pas réellement une forme en croisant un autre trait : on s’arrête juste avant. L’écart est parfois très  mince. Ces erreurs ne sont évidentes qu’après géotraitement. Il faut simplement examiner patiemment le tracé pour chercher la (ou les !) source(s) de l’erreur et relancer le géotraitement après correction.

![pap 07](images/pap_07.jpg) 

Les polygones créés après géotraitement : 

![pap 08](images/pap_08.jpg) 

#### Créer des formes non souhaitées

Créer des formes non souhaitées par un croisement de traits inopportuns : typiquement des traits destinés à fermer des formes, en dépassant d’une ligne, en créent une autre en se recroisant eux-mêmes.

![pap 09](images/pap_09.jpg) ![pap 10](images/pap_10.jpg) 



## 4. Le passage en polygones

Il faut maintenant appliquer à la couche de lignes (qui contient le tracé des formes) un **géotraitement** qui s'appelle **Mise en polygones**. Il est accessible via le menu *Boite à outils - Géométrie vectorielle - Mise en polygones*. 

![pap 11](images/pap_11.png) 

![pap 12](images/pap_12.png) 

{% hint style='danger' %} Attention ! A partir de la version 3.14, ce traitement n'existe plus : il vous faut utiliser *Polygonize*, toujours dans le menu de Géométrie vectorielle. Le traitement *Lignes vers polygones*, qui pourrait s'apparenter, ne donne absolument pas les mêmes résultats.
{% endhint %}



Dans la fenêtre qui s'ouvre, choisir la couche en entrée (votre couche de lignes), puis cliquer sur Exécuter. Cela créera une couche temporaire que vous pouvez utiliser pour vérifier que vos tracés sont corrects. Une fois la vérification effectuée, il vous suffit de créer une copie pérenne avec un clic droit sur la couche temporaire puis *Exporter - Sauvegarder les entités sous*. 

![fenêtre mise en polygones](images/pap_13.png) 

Si vous constatez des erreurs ou des manques, corrigez la couche de ligne et relancez le traitement. C'est pour cela qu'on conseille de n'enregistrer la couche de manière pérenne qu'une fois les vérifications effectuées et pas directement à l'issue du géotraitement. 



## 5. Finaliser la table attributaire et interroger les données

Une fois votre couche de polygones créée, vous pouvez compléter sa table attributaire avec autant d'éléments que nécessaire : numéro d'US ou de fait, indication de composant (pierre, mortier, etc.), indication de matériau, de datation et ainsi de suite. 

Vous faites une sélection des entités concernées et vous pouvez compléter les informations en lot. 

### 5.1. Compléter les informations avec la calculatrice de champ

Une fois votre sélection effectuée, ouvrez la [calculatrice de champ](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/01_calcu_champ.html). Vérifiez que seules les entités sélectionnées sont cochées ("Ne mettre à jour que les xx entités sélectionnées"), puis à droite, cochez *Mise à jour d'un champ existant*, puis choisissez dans le menu déroulant le champ à mettre à jour. 

Dans la console à gauche, saisissez l'information requise, ici 'tuile'. Cliquez sur ok.

![calculatrice de champ](images/pap_14.png)



### 5.2. Compléter les informations à partir de la barre d'outil de numérisation

Avec l'outil *Modifier les attributs de toutes les entités sélectionnées simultanément*, disponible dans la barre d'outil de numérisation, vous pouvez modifier rapidement les entités sélectionnées. 

![pap 15](images/pap_15.png) 

Dans la fenêtre qui s'ouvre, saisissez simplement l'information requise, ici "tuile". Cliquer sur OK et pensez à enregistrer les modifications de la couche. 

![pap 16](images/pap_16.png) 



### 5.3. Compléter les informations à partir de la table attributaire

Enfin, vous pouvez compléter les informations à partir de la table attributaire. Une fois vos entités sélectionnées, passez en mode édition et dans la barre du haut, dans le menu déroulant, choisissez le champ à compléter, saisissez l'information requise puis cliquez sur *Mettre à jour la sélection*. 

![pap 17](images/pap_17.png) 



### 5.4. Interroger les données

Une fois votre table attributaire bien renseignée, il ne vous reste plus qu'à utiliser les outils de symbologie et de requête mis à disposition par QGIS pour tirer le meilleur parti de votre dessin. 