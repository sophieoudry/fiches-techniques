<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyAlgorithm="0" version="3.12.2-București" labelsEnabled="1" simplifyDrawingTol="1" maxScale="0" simplifyDrawingHints="1" readOnly="0" styleCategories="AllStyleCategories" simplifyLocal="1" simplifyMaxScale="1" hasScaleBasedVisibilityFlag="0" minScale="100000000">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 symbollevels="1" forceraster="0" type="RuleRenderer" enableorderby="0">
    <rules key="{dc48442c-f976-46e3-afbe-ed7d7437a81c}">
      <rule label="unité stratigraphique" filter="&quot;typoly&quot; like 'US'" key="{e1c356b4-4a1a-4ae6-b96b-66cecd151bb5}" symbol="0"/>
      <rule label="inclusion" filter="&quot;typoly&quot;  like  'inclusion' " key="{1d4948ff-36ae-4ea0-b460-19c2f91aba6f}" symbol="1"/>
    </rules>
    <symbols>
      <symbol clip_to_extent="1" force_rhr="0" type="fill" name="0" alpha="1">
        <layer pass="1" locked="0" class="SimpleFill" enabled="1">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="255,255,255,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="128,128,128,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.25"/>
          <prop k="outline_width_unit" v="Point"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" force_rhr="0" type="fill" name="1" alpha="1">
        <layer pass="2" locked="0" class="SimpleFill" enabled="1">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="190,178,151,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="Point"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <labeling type="rule-based">
    <rules key="{9af7588e-8466-48c0-9a94-d2c550856a75}">
      <rule key="{3894f40c-1908-4c0d-b709-85dd082e2aa1}">
        <settings calloutType="simple">
          <text-style fontLetterSpacing="0" previewBkgrdColor="255,255,255,255" fontItalic="0" fieldName="numus" textColor="0,0,0,255" namedStyle="Normal" fontSize="6" fontKerning="1" fontWordSpacing="0" fontUnderline="0" useSubstitutions="0" fontSizeMapUnitScale="3x:0,0,0,0,0,0" fontCapitals="0" textOrientation="horizontal" fontSizeUnit="Point" fontStrikeout="0" blendMode="0" isExpression="0" fontWeight="50" textOpacity="1" multilineHeight="1" fontFamily="Arial Narrow">
            <text-buffer bufferBlendMode="0" bufferColor="255,255,255,255" bufferSizeUnits="MM" bufferOpacity="1" bufferJoinStyle="128" bufferNoFill="1" bufferDraw="0" bufferSize="1" bufferSizeMapUnitScale="3x:0,0,0,0,0,0"/>
            <text-mask maskEnabled="0" maskType="0" maskedSymbolLayers="" maskJoinStyle="128" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskSize="0" maskSizeUnits="MM" maskOpacity="1"/>
            <background shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeSizeUnit="MM" shapeSVGFile="" shapeFillColor="255,255,255,255" shapeSizeY="0" shapeBlendMode="0" shapeRadiiUnit="MM" shapeJoinStyle="64" shapeBorderColor="0,0,0,255" shapeBorderWidth="0.25" shapeRotationType="0" shapeBorderWidthUnit="Point" shapeSizeType="0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeDraw="1" shapeOpacity="1" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiY="0" shapeOffsetUnit="MM" shapeRotation="0" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeSizeX="0" shapeType="3" shapeRadiiX="0" shapeOffsetY="0" shapeOffsetX="0"/>
            <shadow shadowOffsetDist="1" shadowScale="100" shadowColor="0,0,0,255" shadowOpacity="0.7" shadowRadiusUnit="MM" shadowBlendMode="6" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowDraw="0" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowRadius="1.5" shadowOffsetGlobal="1" shadowOffsetAngle="135" shadowRadiusAlphaOnly="0" shadowUnder="0" shadowOffsetUnit="MM"/>
            <dd_properties>
              <Option type="Map">
                <Option value="" type="QString" name="name"/>
                <Option name="properties"/>
                <Option value="collection" type="QString" name="type"/>
              </Option>
            </dd_properties>
            <substitutions/>
          </text-style>
          <text-format wrapChar="" placeDirectionSymbol="0" addDirectionSymbol="0" decimals="3" reverseDirectionSymbol="0" autoWrapLength="0" multilineAlign="4294967295" useMaxLineLengthForAutoWrap="1" leftDirectionSymbol="&lt;" rightDirectionSymbol=">" formatNumbers="0" plussign="0"/>
          <placement geometryGeneratorEnabled="0" centroidWhole="0" offsetUnits="MM" repeatDistanceUnits="MM" fitInPolygonOnly="0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" offsetType="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" placementFlags="10" centroidInside="0" layerType="UnknownGeometry" quadOffset="4" overrunDistanceUnit="MM" rotationAngle="0" distUnits="MM" geometryGenerator="" dist="0" priority="5" preserveRotation="1" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" placement="0" yOffset="0" maxCurvedCharAngleOut="-25" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" overrunDistance="0" maxCurvedCharAngleIn="25" xOffset="0" geometryGeneratorType="PointGeometry" distMapUnitScale="3x:0,0,0,0,0,0" repeatDistance="0"/>
          <rendering obstacleType="0" obstacleFactor="1" upsidedownLabels="0" limitNumLabels="0" scaleVisibility="0" fontMaxPixelSize="10000" scaleMin="0" displayAll="1" minFeatureSize="0" labelPerPart="0" drawLabels="1" fontLimitPixelSize="0" maxNumLabels="2000" obstacle="1" scaleMax="0" fontMinPixelSize="3" mergeLines="0" zIndex="0"/>
          <dd_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </dd_properties>
          <callout type="simple">
            <Option type="Map">
              <Option value="pole_of_inaccessibility" type="QString" name="anchorPoint"/>
              <Option type="Map" name="ddProperties">
                <Option value="" type="QString" name="name"/>
                <Option name="properties"/>
                <Option value="collection" type="QString" name="type"/>
              </Option>
              <Option value="false" type="bool" name="drawToAllParts"/>
              <Option value="0" type="QString" name="enabled"/>
              <Option value="&lt;symbol clip_to_extent=&quot;1&quot; force_rhr=&quot;0&quot; type=&quot;line&quot; name=&quot;symbol&quot; alpha=&quot;1&quot;>&lt;layer pass=&quot;0&quot; locked=&quot;0&quot; class=&quot;SimpleLine&quot; enabled=&quot;1&quot;>&lt;prop k=&quot;capstyle&quot; v=&quot;square&quot;/>&lt;prop k=&quot;customdash&quot; v=&quot;5;2&quot;/>&lt;prop k=&quot;customdash_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;customdash_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;draw_inside_polygon&quot; v=&quot;0&quot;/>&lt;prop k=&quot;joinstyle&quot; v=&quot;bevel&quot;/>&lt;prop k=&quot;line_color&quot; v=&quot;60,60,60,255&quot;/>&lt;prop k=&quot;line_style&quot; v=&quot;solid&quot;/>&lt;prop k=&quot;line_width&quot; v=&quot;0.3&quot;/>&lt;prop k=&quot;line_width_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;offset&quot; v=&quot;0&quot;/>&lt;prop k=&quot;offset_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;offset_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;ring_filter&quot; v=&quot;0&quot;/>&lt;prop k=&quot;use_custom_dash&quot; v=&quot;0&quot;/>&lt;prop k=&quot;width_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; type=&quot;QString&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; type=&quot;QString&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>" type="QString" name="lineSymbol"/>
              <Option value="0" type="double" name="minLength"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="minLengthMapUnitScale"/>
              <Option value="MM" type="QString" name="minLengthUnit"/>
              <Option value="0" type="double" name="offsetFromAnchor"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="offsetFromAnchorMapUnitScale"/>
              <Option value="MM" type="QString" name="offsetFromAnchorUnit"/>
              <Option value="0" type="double" name="offsetFromLabel"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="offsetFromLabelMapUnitScale"/>
              <Option value="MM" type="QString" name="offsetFromLabelUnit"/>
            </Option>
          </callout>
        </settings>
      </rule>
      <rule description="terre végétale" filter=" &quot;detail&quot;  =  'terre végétale' " key="{b46d3114-ef32-44c8-9cc6-e10d2f5d66e9}">
        <settings calloutType="simple">
          <text-style fontLetterSpacing="0" previewBkgrdColor="255,255,255,255" fontItalic="0" fieldName="'TV'" textColor="0,0,0,255" namedStyle="Normal" fontSize="6" fontKerning="1" fontWordSpacing="0" fontUnderline="0" useSubstitutions="0" fontSizeMapUnitScale="3x:0,0,0,0,0,0" fontCapitals="0" textOrientation="horizontal" fontSizeUnit="Point" fontStrikeout="0" blendMode="0" isExpression="1" fontWeight="50" textOpacity="1" multilineHeight="1" fontFamily="Arial Narrow">
            <text-buffer bufferBlendMode="0" bufferColor="255,255,255,255" bufferSizeUnits="MM" bufferOpacity="1" bufferJoinStyle="128" bufferNoFill="1" bufferDraw="0" bufferSize="1" bufferSizeMapUnitScale="3x:0,0,0,0,0,0"/>
            <text-mask maskEnabled="0" maskType="0" maskedSymbolLayers="" maskJoinStyle="128" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskSize="0" maskSizeUnits="MM" maskOpacity="1"/>
            <background shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeSizeUnit="MM" shapeSVGFile="" shapeFillColor="255,255,255,255" shapeSizeY="0" shapeBlendMode="0" shapeRadiiUnit="MM" shapeJoinStyle="64" shapeBorderColor="0,0,0,255" shapeBorderWidth="0.25" shapeRotationType="0" shapeBorderWidthUnit="Point" shapeSizeType="0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeDraw="0" shapeOpacity="1" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiY="0" shapeOffsetUnit="MM" shapeRotation="0" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeSizeX="0" shapeType="3" shapeRadiiX="0" shapeOffsetY="0" shapeOffsetX="0"/>
            <shadow shadowOffsetDist="1" shadowScale="100" shadowColor="0,0,0,255" shadowOpacity="0.7" shadowRadiusUnit="MM" shadowBlendMode="6" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowDraw="0" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowRadius="1.5" shadowOffsetGlobal="1" shadowOffsetAngle="135" shadowRadiusAlphaOnly="0" shadowUnder="0" shadowOffsetUnit="MM"/>
            <dd_properties>
              <Option type="Map">
                <Option value="" type="QString" name="name"/>
                <Option name="properties"/>
                <Option value="collection" type="QString" name="type"/>
              </Option>
            </dd_properties>
            <substitutions/>
          </text-style>
          <text-format wrapChar="" placeDirectionSymbol="0" addDirectionSymbol="0" decimals="3" reverseDirectionSymbol="0" autoWrapLength="0" multilineAlign="4294967295" useMaxLineLengthForAutoWrap="1" leftDirectionSymbol="&lt;" rightDirectionSymbol=">" formatNumbers="0" plussign="0"/>
          <placement geometryGeneratorEnabled="0" centroidWhole="0" offsetUnits="MM" repeatDistanceUnits="MM" fitInPolygonOnly="0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" offsetType="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" placementFlags="10" centroidInside="0" layerType="UnknownGeometry" quadOffset="4" overrunDistanceUnit="MM" rotationAngle="0" distUnits="MM" geometryGenerator="" dist="0" priority="5" preserveRotation="1" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" placement="0" yOffset="0" maxCurvedCharAngleOut="-25" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" overrunDistance="0" maxCurvedCharAngleIn="25" xOffset="0" geometryGeneratorType="PointGeometry" distMapUnitScale="3x:0,0,0,0,0,0" repeatDistance="0"/>
          <rendering obstacleType="0" obstacleFactor="1" upsidedownLabels="0" limitNumLabels="0" scaleVisibility="0" fontMaxPixelSize="10000" scaleMin="0" displayAll="0" minFeatureSize="0" labelPerPart="0" drawLabels="1" fontLimitPixelSize="0" maxNumLabels="2000" obstacle="1" scaleMax="0" fontMinPixelSize="3" mergeLines="0" zIndex="0"/>
          <dd_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </dd_properties>
          <callout type="simple">
            <Option type="Map">
              <Option value="pole_of_inaccessibility" type="QString" name="anchorPoint"/>
              <Option type="Map" name="ddProperties">
                <Option value="" type="QString" name="name"/>
                <Option name="properties"/>
                <Option value="collection" type="QString" name="type"/>
              </Option>
              <Option value="false" type="bool" name="drawToAllParts"/>
              <Option value="0" type="QString" name="enabled"/>
              <Option value="&lt;symbol clip_to_extent=&quot;1&quot; force_rhr=&quot;0&quot; type=&quot;line&quot; name=&quot;symbol&quot; alpha=&quot;1&quot;>&lt;layer pass=&quot;0&quot; locked=&quot;0&quot; class=&quot;SimpleLine&quot; enabled=&quot;1&quot;>&lt;prop k=&quot;capstyle&quot; v=&quot;square&quot;/>&lt;prop k=&quot;customdash&quot; v=&quot;5;2&quot;/>&lt;prop k=&quot;customdash_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;customdash_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;draw_inside_polygon&quot; v=&quot;0&quot;/>&lt;prop k=&quot;joinstyle&quot; v=&quot;bevel&quot;/>&lt;prop k=&quot;line_color&quot; v=&quot;60,60,60,255&quot;/>&lt;prop k=&quot;line_style&quot; v=&quot;solid&quot;/>&lt;prop k=&quot;line_width&quot; v=&quot;0.3&quot;/>&lt;prop k=&quot;line_width_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;offset&quot; v=&quot;0&quot;/>&lt;prop k=&quot;offset_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;offset_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;ring_filter&quot; v=&quot;0&quot;/>&lt;prop k=&quot;use_custom_dash&quot; v=&quot;0&quot;/>&lt;prop k=&quot;width_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; type=&quot;QString&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; type=&quot;QString&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>" type="QString" name="lineSymbol"/>
              <Option value="0" type="double" name="minLength"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="minLengthMapUnitScale"/>
              <Option value="MM" type="QString" name="minLengthUnit"/>
              <Option value="0" type="double" name="offsetFromAnchor"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="offsetFromAnchorMapUnitScale"/>
              <Option value="MM" type="QString" name="offsetFromAnchorUnit"/>
              <Option value="0" type="double" name="offsetFromLabel"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="offsetFromLabelMapUnitScale"/>
              <Option value="MM" type="QString" name="offsetFromLabelUnit"/>
            </Option>
          </callout>
        </settings>
      </rule>
      <rule description="limon argileux brun clair" filter=" &quot;detail&quot;  =  'limon argileux brun clair'" key="{e438ddbd-3e53-4e70-8aa5-6ba49b1d12d0}">
        <settings calloutType="simple">
          <text-style fontLetterSpacing="0" previewBkgrdColor="255,255,255,255" fontItalic="0" fieldName="'LABC'" textColor="0,0,0,255" namedStyle="Normal" fontSize="6" fontKerning="1" fontWordSpacing="0" fontUnderline="0" useSubstitutions="0" fontSizeMapUnitScale="3x:0,0,0,0,0,0" fontCapitals="0" textOrientation="horizontal" fontSizeUnit="Point" fontStrikeout="0" blendMode="0" isExpression="1" fontWeight="50" textOpacity="1" multilineHeight="1" fontFamily="Arial Narrow">
            <text-buffer bufferBlendMode="0" bufferColor="255,255,255,255" bufferSizeUnits="MM" bufferOpacity="1" bufferJoinStyle="128" bufferNoFill="1" bufferDraw="0" bufferSize="1" bufferSizeMapUnitScale="3x:0,0,0,0,0,0"/>
            <text-mask maskEnabled="0" maskType="0" maskedSymbolLayers="" maskJoinStyle="128" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskSize="0" maskSizeUnits="MM" maskOpacity="1"/>
            <background shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeSizeUnit="MM" shapeSVGFile="" shapeFillColor="255,255,255,255" shapeSizeY="0" shapeBlendMode="0" shapeRadiiUnit="MM" shapeJoinStyle="64" shapeBorderColor="0,0,0,255" shapeBorderWidth="0.25" shapeRotationType="0" shapeBorderWidthUnit="Point" shapeSizeType="0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeDraw="0" shapeOpacity="1" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiY="0" shapeOffsetUnit="MM" shapeRotation="0" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeSizeX="0" shapeType="3" shapeRadiiX="0" shapeOffsetY="0" shapeOffsetX="0"/>
            <shadow shadowOffsetDist="1" shadowScale="100" shadowColor="0,0,0,255" shadowOpacity="0.7" shadowRadiusUnit="MM" shadowBlendMode="6" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowDraw="0" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowRadius="1.5" shadowOffsetGlobal="1" shadowOffsetAngle="135" shadowRadiusAlphaOnly="0" shadowUnder="0" shadowOffsetUnit="MM"/>
            <dd_properties>
              <Option type="Map">
                <Option value="" type="QString" name="name"/>
                <Option name="properties"/>
                <Option value="collection" type="QString" name="type"/>
              </Option>
            </dd_properties>
            <substitutions/>
          </text-style>
          <text-format wrapChar="" placeDirectionSymbol="0" addDirectionSymbol="0" decimals="3" reverseDirectionSymbol="0" autoWrapLength="0" multilineAlign="4294967295" useMaxLineLengthForAutoWrap="1" leftDirectionSymbol="&lt;" rightDirectionSymbol=">" formatNumbers="0" plussign="0"/>
          <placement geometryGeneratorEnabled="0" centroidWhole="0" offsetUnits="MM" repeatDistanceUnits="MM" fitInPolygonOnly="0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" offsetType="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" placementFlags="10" centroidInside="1" layerType="UnknownGeometry" quadOffset="4" overrunDistanceUnit="MM" rotationAngle="0" distUnits="MM" geometryGenerator="" dist="0" priority="5" preserveRotation="1" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" placement="0" yOffset="0" maxCurvedCharAngleOut="-25" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" overrunDistance="0" maxCurvedCharAngleIn="25" xOffset="0" geometryGeneratorType="PointGeometry" distMapUnitScale="3x:0,0,0,0,0,0" repeatDistance="0"/>
          <rendering obstacleType="0" obstacleFactor="1" upsidedownLabels="0" limitNumLabels="0" scaleVisibility="0" fontMaxPixelSize="10000" scaleMin="0" displayAll="1" minFeatureSize="0" labelPerPart="1" drawLabels="1" fontLimitPixelSize="0" maxNumLabels="2000" obstacle="1" scaleMax="0" fontMinPixelSize="3" mergeLines="0" zIndex="0"/>
          <dd_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </dd_properties>
          <callout type="simple">
            <Option type="Map">
              <Option value="pole_of_inaccessibility" type="QString" name="anchorPoint"/>
              <Option type="Map" name="ddProperties">
                <Option value="" type="QString" name="name"/>
                <Option name="properties"/>
                <Option value="collection" type="QString" name="type"/>
              </Option>
              <Option value="false" type="bool" name="drawToAllParts"/>
              <Option value="0" type="QString" name="enabled"/>
              <Option value="&lt;symbol clip_to_extent=&quot;1&quot; force_rhr=&quot;0&quot; type=&quot;line&quot; name=&quot;symbol&quot; alpha=&quot;1&quot;>&lt;layer pass=&quot;0&quot; locked=&quot;0&quot; class=&quot;SimpleLine&quot; enabled=&quot;1&quot;>&lt;prop k=&quot;capstyle&quot; v=&quot;square&quot;/>&lt;prop k=&quot;customdash&quot; v=&quot;5;2&quot;/>&lt;prop k=&quot;customdash_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;customdash_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;draw_inside_polygon&quot; v=&quot;0&quot;/>&lt;prop k=&quot;joinstyle&quot; v=&quot;bevel&quot;/>&lt;prop k=&quot;line_color&quot; v=&quot;60,60,60,255&quot;/>&lt;prop k=&quot;line_style&quot; v=&quot;solid&quot;/>&lt;prop k=&quot;line_width&quot; v=&quot;0.3&quot;/>&lt;prop k=&quot;line_width_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;offset&quot; v=&quot;0&quot;/>&lt;prop k=&quot;offset_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;offset_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;ring_filter&quot; v=&quot;0&quot;/>&lt;prop k=&quot;use_custom_dash&quot; v=&quot;0&quot;/>&lt;prop k=&quot;width_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; type=&quot;QString&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; type=&quot;QString&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>" type="QString" name="lineSymbol"/>
              <Option value="0" type="double" name="minLength"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="minLengthMapUnitScale"/>
              <Option value="MM" type="QString" name="minLengthUnit"/>
              <Option value="0" type="double" name="offsetFromAnchor"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="offsetFromAnchorMapUnitScale"/>
              <Option value="MM" type="QString" name="offsetFromAnchorUnit"/>
              <Option value="0" type="double" name="offsetFromLabel"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="offsetFromLabelMapUnitScale"/>
              <Option value="MM" type="QString" name="offsetFromLabelUnit"/>
            </Option>
          </callout>
        </settings>
      </rule>
      <rule description="limon des plateaux" filter=" &quot;detail&quot;  =  'limon des plateaux'" key="{3257a59b-9234-4f6e-a471-79a1cf5802ff}">
        <settings calloutType="simple">
          <text-style fontLetterSpacing="0" previewBkgrdColor="255,255,255,255" fontItalic="0" fieldName="'LP'" textColor="0,0,0,255" namedStyle="Normal" fontSize="6" fontKerning="1" fontWordSpacing="0" fontUnderline="0" useSubstitutions="0" fontSizeMapUnitScale="3x:0,0,0,0,0,0" fontCapitals="0" textOrientation="horizontal" fontSizeUnit="Point" fontStrikeout="0" blendMode="0" isExpression="1" fontWeight="50" textOpacity="1" multilineHeight="1" fontFamily="Arial Narrow">
            <text-buffer bufferBlendMode="0" bufferColor="255,255,255,255" bufferSizeUnits="MM" bufferOpacity="1" bufferJoinStyle="128" bufferNoFill="1" bufferDraw="0" bufferSize="1" bufferSizeMapUnitScale="3x:0,0,0,0,0,0"/>
            <text-mask maskEnabled="0" maskType="0" maskedSymbolLayers="" maskJoinStyle="128" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskSize="0" maskSizeUnits="MM" maskOpacity="1"/>
            <background shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeSizeUnit="MM" shapeSVGFile="" shapeFillColor="255,255,255,255" shapeSizeY="0" shapeBlendMode="0" shapeRadiiUnit="MM" shapeJoinStyle="64" shapeBorderColor="0,0,0,255" shapeBorderWidth="0.25" shapeRotationType="0" shapeBorderWidthUnit="Point" shapeSizeType="0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeDraw="0" shapeOpacity="1" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiY="0" shapeOffsetUnit="MM" shapeRotation="0" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeSizeX="0" shapeType="3" shapeRadiiX="0" shapeOffsetY="0" shapeOffsetX="0"/>
            <shadow shadowOffsetDist="1" shadowScale="100" shadowColor="0,0,0,255" shadowOpacity="0.7" shadowRadiusUnit="MM" shadowBlendMode="6" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowDraw="0" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowRadius="1.5" shadowOffsetGlobal="1" shadowOffsetAngle="135" shadowRadiusAlphaOnly="0" shadowUnder="0" shadowOffsetUnit="MM"/>
            <dd_properties>
              <Option type="Map">
                <Option value="" type="QString" name="name"/>
                <Option name="properties"/>
                <Option value="collection" type="QString" name="type"/>
              </Option>
            </dd_properties>
            <substitutions/>
          </text-style>
          <text-format wrapChar="" placeDirectionSymbol="0" addDirectionSymbol="0" decimals="3" reverseDirectionSymbol="0" autoWrapLength="0" multilineAlign="4294967295" useMaxLineLengthForAutoWrap="1" leftDirectionSymbol="&lt;" rightDirectionSymbol=">" formatNumbers="0" plussign="0"/>
          <placement geometryGeneratorEnabled="0" centroidWhole="0" offsetUnits="MM" repeatDistanceUnits="MM" fitInPolygonOnly="0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" offsetType="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" placementFlags="10" centroidInside="1" layerType="UnknownGeometry" quadOffset="4" overrunDistanceUnit="MM" rotationAngle="0" distUnits="MM" geometryGenerator="" dist="0" priority="5" preserveRotation="1" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" placement="0" yOffset="0" maxCurvedCharAngleOut="-25" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" overrunDistance="0" maxCurvedCharAngleIn="25" xOffset="0" geometryGeneratorType="PointGeometry" distMapUnitScale="3x:0,0,0,0,0,0" repeatDistance="0"/>
          <rendering obstacleType="0" obstacleFactor="1" upsidedownLabels="0" limitNumLabels="0" scaleVisibility="0" fontMaxPixelSize="10000" scaleMin="0" displayAll="0" minFeatureSize="0" labelPerPart="1" drawLabels="1" fontLimitPixelSize="0" maxNumLabels="2000" obstacle="1" scaleMax="0" fontMinPixelSize="3" mergeLines="0" zIndex="0"/>
          <dd_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </dd_properties>
          <callout type="simple">
            <Option type="Map">
              <Option value="pole_of_inaccessibility" type="QString" name="anchorPoint"/>
              <Option type="Map" name="ddProperties">
                <Option value="" type="QString" name="name"/>
                <Option name="properties"/>
                <Option value="collection" type="QString" name="type"/>
              </Option>
              <Option value="false" type="bool" name="drawToAllParts"/>
              <Option value="0" type="QString" name="enabled"/>
              <Option value="&lt;symbol clip_to_extent=&quot;1&quot; force_rhr=&quot;0&quot; type=&quot;line&quot; name=&quot;symbol&quot; alpha=&quot;1&quot;>&lt;layer pass=&quot;0&quot; locked=&quot;0&quot; class=&quot;SimpleLine&quot; enabled=&quot;1&quot;>&lt;prop k=&quot;capstyle&quot; v=&quot;square&quot;/>&lt;prop k=&quot;customdash&quot; v=&quot;5;2&quot;/>&lt;prop k=&quot;customdash_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;customdash_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;draw_inside_polygon&quot; v=&quot;0&quot;/>&lt;prop k=&quot;joinstyle&quot; v=&quot;bevel&quot;/>&lt;prop k=&quot;line_color&quot; v=&quot;60,60,60,255&quot;/>&lt;prop k=&quot;line_style&quot; v=&quot;solid&quot;/>&lt;prop k=&quot;line_width&quot; v=&quot;0.3&quot;/>&lt;prop k=&quot;line_width_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;offset&quot; v=&quot;0&quot;/>&lt;prop k=&quot;offset_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;offset_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;ring_filter&quot; v=&quot;0&quot;/>&lt;prop k=&quot;use_custom_dash&quot; v=&quot;0&quot;/>&lt;prop k=&quot;width_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; type=&quot;QString&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; type=&quot;QString&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>" type="QString" name="lineSymbol"/>
              <Option value="0" type="double" name="minLength"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="minLengthMapUnitScale"/>
              <Option value="MM" type="QString" name="minLengthUnit"/>
              <Option value="0" type="double" name="offsetFromAnchor"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="offsetFromAnchorMapUnitScale"/>
              <Option value="MM" type="QString" name="offsetFromAnchorUnit"/>
              <Option value="0" type="double" name="offsetFromLabel"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="offsetFromLabelMapUnitScale"/>
              <Option value="MM" type="QString" name="offsetFromLabelUnit"/>
            </Option>
          </callout>
        </settings>
      </rule>
    </rules>
  </labeling>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>COALESCE( "id", '&lt;NULL>' )</value>
      <value>COALESCE( "id", '&lt;NULL>' )</value>
      <value>COALESCE( "id", '&lt;NULL>' )</value>
      <value>COALESCE( "id", '&lt;NULL>' )</value>
    </property>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory penColor="#000000" height="15" opacity="1" penAlpha="255" width="15" barWidth="5" scaleBasedVisibility="0" showAxis="1" sizeType="MM" maxScaleDenominator="1e+08" spacing="5" direction="0" spacingUnitScale="3x:0,0,0,0,0,0" penWidth="0" labelPlacementMethod="XHeight" rotationOffset="270" diagramOrientation="Up" spacingUnit="MM" scaleDependency="Area" lineSizeScale="3x:0,0,0,0,0,0" backgroundAlpha="255" backgroundColor="#ffffff" enabled="0" minScaleDenominator="0" lineSizeType="MM" minimumSize="0" sizeScale="3x:0,0,0,0,0,0">
      <fontProperties description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      <attribute label="" color="#000000" field=""/>
      <axisSymbol>
        <symbol clip_to_extent="1" force_rhr="0" type="line" name="" alpha="1">
          <layer pass="0" locked="0" class="SimpleLine" enabled="1">
            <prop k="capstyle" v="square"/>
            <prop k="customdash" v="5;2"/>
            <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="customdash_unit" v="MM"/>
            <prop k="draw_inside_polygon" v="0"/>
            <prop k="joinstyle" v="bevel"/>
            <prop k="line_color" v="35,35,35,255"/>
            <prop k="line_style" v="solid"/>
            <prop k="line_width" v="0.26"/>
            <prop k="line_width_unit" v="MM"/>
            <prop k="offset" v="0"/>
            <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="offset_unit" v="MM"/>
            <prop k="ring_filter" v="0"/>
            <prop k="use_custom_dash" v="0"/>
            <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <data_defined_properties>
              <Option type="Map">
                <Option value="" type="QString" name="name"/>
                <Option name="properties"/>
                <Option value="collection" type="QString" name="type"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings zIndex="0" placement="1" showAll="1" dist="0" obstacle="0" linePlacementFlags="18" priority="0">
    <properties>
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option name="properties"/>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration type="Map">
      <Option type="Map" name="QgsGeometryGapCheck">
        <Option value="0" type="double" name="allowedGapsBuffer"/>
        <Option value="false" type="bool" name="allowedGapsEnabled"/>
        <Option value="" type="QString" name="allowedGapsLayer"/>
      </Option>
    </checkConfiguration>
  </geometryOptions>
  <referencedLayers/>
  <referencingLayers/>
  <fieldConfiguration>
    <field name="id_line">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="id_coupe">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="numfait">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" type="QString" name="IsMultiline"/>
            <Option value="0" type="QString" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="numus">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" type="QString" name="IsMultiline"/>
            <Option value="0" type="QString" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="typline">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="id_poly">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="typoly">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option type="List" name="map">
              <Option type="Map">
                <Option value="US" type="QString" name="US"/>
              </Option>
              <Option type="Map">
                <Option value="inclusion" type="QString" name="inclusion"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="detail">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="" field="id_line" index="0"/>
    <alias name="" field="id_coupe" index="1"/>
    <alias name="" field="numfait" index="2"/>
    <alias name="" field="numus" index="3"/>
    <alias name="" field="typline" index="4"/>
    <alias name="" field="id_poly" index="5"/>
    <alias name="" field="typoly" index="6"/>
    <alias name="" field="detail" index="7"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default applyOnUpdate="0" field="id_line" expression=""/>
    <default applyOnUpdate="0" field="id_coupe" expression=""/>
    <default applyOnUpdate="0" field="numfait" expression=""/>
    <default applyOnUpdate="0" field="numus" expression=""/>
    <default applyOnUpdate="0" field="typline" expression=""/>
    <default applyOnUpdate="0" field="id_poly" expression=""/>
    <default applyOnUpdate="0" field="typoly" expression=""/>
    <default applyOnUpdate="0" field="detail" expression=""/>
  </defaults>
  <constraints>
    <constraint constraints="0" unique_strength="0" exp_strength="0" notnull_strength="0" field="id_line"/>
    <constraint constraints="0" unique_strength="0" exp_strength="0" notnull_strength="0" field="id_coupe"/>
    <constraint constraints="0" unique_strength="0" exp_strength="0" notnull_strength="0" field="numfait"/>
    <constraint constraints="0" unique_strength="0" exp_strength="0" notnull_strength="0" field="numus"/>
    <constraint constraints="0" unique_strength="0" exp_strength="0" notnull_strength="0" field="typline"/>
    <constraint constraints="0" unique_strength="0" exp_strength="0" notnull_strength="0" field="id_poly"/>
    <constraint constraints="0" unique_strength="0" exp_strength="0" notnull_strength="0" field="typoly"/>
    <constraint constraints="0" unique_strength="0" exp_strength="0" notnull_strength="0" field="detail"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" exp="" field="id_line"/>
    <constraint desc="" exp="" field="id_coupe"/>
    <constraint desc="" exp="" field="numfait"/>
    <constraint desc="" exp="" field="numus"/>
    <constraint desc="" exp="" field="typline"/>
    <constraint desc="" exp="" field="id_poly"/>
    <constraint desc="" exp="" field="typoly"/>
    <constraint desc="" exp="" field="detail"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="0" sortExpression="&quot;detail&quot;">
    <columns>
      <column width="-1" hidden="0" type="field" name="numfait"/>
      <column width="-1" hidden="0" type="field" name="numus"/>
      <column width="-1" hidden="1" type="actions"/>
      <column width="372" hidden="0" type="field" name="detail"/>
      <column width="-1" hidden="0" type="field" name="id_coupe"/>
      <column width="-1" hidden="0" type="field" name="id_poly"/>
      <column width="-1" hidden="0" type="field" name="typoly"/>
      <column width="-1" hidden="0" type="field" name="id_line"/>
      <column width="-1" hidden="0" type="field" name="typline"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1">G:/coupes_QGIS/03_exemples/projet1</editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath>G:/coupes_QGIS/03_exemples/projet1</editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="detail"/>
    <field editable="1" name="id"/>
    <field editable="1" name="id_coupe"/>
    <field editable="1" name="id_line"/>
    <field editable="1" name="id_poly"/>
    <field editable="1" name="matiere"/>
    <field editable="1" name="nature"/>
    <field editable="1" name="numfait"/>
    <field editable="1" name="numinute"/>
    <field editable="1" name="numus"/>
    <field editable="1" name="typline"/>
    <field editable="1" name="typoly"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="detail"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="id_coupe"/>
    <field labelOnTop="0" name="id_line"/>
    <field labelOnTop="0" name="id_poly"/>
    <field labelOnTop="0" name="matiere"/>
    <field labelOnTop="0" name="nature"/>
    <field labelOnTop="0" name="numfait"/>
    <field labelOnTop="0" name="numinute"/>
    <field labelOnTop="0" name="numus"/>
    <field labelOnTop="0" name="typline"/>
    <field labelOnTop="0" name="typoly"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>COALESCE( "id", '&lt;NULL>' )</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>2</layerGeometryType>
</qgis>
