# Coupes stratigraphiques et QGIS
![][logo_superQGIS] **Niveau hard-core du SIG**  :muscle: :x: :100:

> Auteurs : Caroline Font, Thomas Guillemard, Léa Roubaud, Christelle Seng - Mai 2020
___

## Principe et structuration

La vectorisation des relevés de coupe dans QGIS a de nombreux avantages. Parmi ceux-ci, les plus évidents sont :
* la possibilité de faire du traitement de données spatiales en exploitant l'enregistrement stratigraphique des archéologues
* l'exploitation possible des informations spatiales et géométriques (surface, profondeurs et altitudes...)
* du fait de la numérisation à l’échelle 1:1, la possibilité d’export dans toutes les échelles sans avoir recours à une mise à l’échelle des annotations et symbole (contrairement à Illustrator)
* la possibilité de gérer les symboles selon les attributs des données sur l’ensemble des entités numérisés pour une mise en page homogène et immédiate
* l'exploitation des outils d'atlas et de variables dans QGIS

### Principe

Les coupes stratigraphiques sont par définition des données verticales. Il n'existe pas de 3ème dimension dans QGIS mais il est néanmoins possible de détourner le système en deux dimensions du logiciel. En effet, le repère orthonormé d'origine se présente en abscisse en x et en ordonnée en y. Il est possible de considérer l'axe des ordonnées comme l'axe des z. Ainsi un objet dont les coordonnées seraient en x : 526262 m, en y : 6701985 m et en z : 98 m se trouverait en deux endroits dans le canevas de QGIS :
* en plan, aux coordonnées x et y habituelles selon le système de projection définie
* en pseudo-projection, aux coordonnées x (d'origine ou recalculé selon un axe de projection) et y = z soit pour cet exemple 98 m.

### Structuration

Quatre couches vectorielles constituent la structure pour l’enregistrement des relevés de coupe dans un logiciel de SIG. Pour plus de fonctionnalités et de performances, il est recommandé d’utiliser une base de données SpatiaLite. 

![Structure de la base de données spatiale stratigraphique][001]

> :mag: **NB** : Cette structure constitue une base de travail. Elle peut-être adaptée en fonction des problématiques spécifiques d'une opération. PK signifie Primary Key (clé primaire) et FK, Foreign Key (clé étrangère).  
> Des models 3 de QGIS ont été préparés pour automatiser la structuration sur les couches *line_coupe* et *poly_coupe* et figurent en [pièces jointes](#pièces-jointes) de ce guide

#### Les axes de relevés en plan (couche *axe_plan*)

Cette couche fait partie des **Six couches** préconisées pour la constitution d'une base de données spatiales d'une opération archéologique. Elle est fournie par le topographe puisqu'issue des levés.
Il s’agit d’une couche de ligne, constituée d’**un seul segment** (c’est-à-dire d’un point d’origine et d’un point de destination, deux points en tout). Un segment possède une direction qui correspond au sens de lecture de la coupe.

> :exclamation: **Attention** : si les axes sont constitués de plusieurs segments, il faut préalablement les exploser en autant de segments que possédés par la ligne. Dans la Boîte de traitement, utiliser l’outil suivant : Géométrie vectorielle/Exploser
des lignes.  
>![Outil Exploser des lignes dans la boîte à outils][002]  
>Cette outil génère une couche temporaire qui remplacera la couche d’axe d’origine.  
Il est important de veiller au sens de dessin de la coupe. En Occident, nous lisons les relevés de gauche à droite. Le premier point de l’axe doit donc correspondre au point gauche de lecture de la coupe.
Pour vérifier le sens de la ligne, il suffit d'appliquer un symbole de flèche sur la ligne.  
![Paramétrage du symbole de flèche][003]  
>En mode édition, il est possible d'inverser le sens de la ligne en utilisant l'outil d'inversion de ligne présent dans la barre d'outils de numérisation avancée (depuis la version 3.8 de QGIS).  
>![Outil d’inversion du sens de la ligne][004]
>

A partir de cette couche vectorielle, plusieurs données attributaires et géométriques permettent de constituer la base de données de numérisation des informations stratigraphiques en coupe :
* la clé primaire des axes en plan constitue l'identifiant de relation avec l'ensemble des couches de la base de données sous forme de clé étrangère
* l'azimuth, c'est-à-dire l'orientation de l'axe de coupe par rapport au nord géographique (dans la calculatrice de champ et après avoir vérifier le sens des lignes, l'azimuth peut être calculé avec la formule `degrees(azimuth(start_point($geometry),end_point($geometry)))`)
* l'altitude de l'axe de relevé disponible dans les attributs des points de levés topographiques (moyenne des deux points d'axe , puisqu'un axe de relevé est forcément horizontal)

#### Axe de relevé en coupe (couche *axe_coupe*)

Il s’agit d’une couche de ligne, également constituée d’**un seul segment** (c’est-à-dire d’un point d’origine et d’un point de destination, deux points en tout).
La dimension de l’axe de relevé en coupe doit correspondre à celle de la minute à l’échelle 1:1.
L’identifiant de cet axe en coupe est le même que pour l’axe en plan.  
Cet axe permet le géoréférencement de la minute sur les deux points d'extrémité.
La numérisation de cet axe peut-être automatisée dans QGIS selon le principe d'inversion des axes x et y = z expliqué plus haut. 

> :mag: Un model3 (*dessin_axe_coupe*) de QGIS a été préparé et figure en [pièce jointe](#pièces-jointes) de ce guide accompagné d'un fichier de style QGIS .qml qui définit le symbole en sortie de l'axe en coupe. 

Ce model3 génère une couche temporaire à partir de la couche **axe_plan** qu'il conviendra soit d'enregistrer en shape, soit d'intégrer dans la base de données SpatiaLite. Il fonctionne comme suit :
* Ajout d'une clé primaire **"id_axe"** autogénérée selon l'ordre de dessin des objets de la couche **axe_plan**
* Calcul et ajout de l'azimuth des lignes de la couche **axe_plan**
* Calcul et ajout de la longueur des axes de la couche **axe_plan**
* Calcul et ajout de la longueur cumulée de l'ensemble des axes
* Dessin de l'axe selon trois critères : 
  * l'origine de l'axe qui correspond à x = 0 (par défaut) + longueur cumulée des axes + intervalle d'entre-axes (le premier axe possède une coordonnée x à 0, par défaut. Dans le cas où la vectorisation se ferait en plusieurs fois et pour éviter les superpositions des coupes, il est possible de changer cette coordonnées X d'origine en spécifiant dans la fenètre du model3 sa valeur en mètres en tenant compte de l'intervalle avec le dernier axe dessiné) ; y = altitude de l'axe
  * l'intervalle d'entre-axes défini par l'utilisateur correspond à l'espace de dessin entre deux axes. Pour un export des coupes au 1/20, un intervalle de 2 m est conseillé
  * les coordonnées de fin de l'axe correspondant à x = 0 + longueur cumulée des axes + longueur de l'axe courant + intervalle d'entre-axes et y = altitude de l'axe

![Numérisation de l’axe en coupe][005]

#### Les entités linéaires en coupe (couche *line_coupe*)

Il s’agit d’une couche vectorielle de polylignes, ou de multipolylignes, et représentent les interfaces observées ou restituées. Elles sont de trois natures : 
* la limite d’observation de la coupe (limite de décapage, profondeur du sondage…), souvent représentée en trait-point
* les US négatives (limite de creusement par exemple), souvent représentées par un trait plus épais
* les restitutions d’interfaces d’US, souvent représentées par un pointillé  
Cette table contient la clé étrangère de l’identifiant d'axe. Le numéro de fait et d'US sont nécessaires pour les interfaces correspondants aux US négatives seulement.

![Numérisation des lignes d’interfaces][006]

#### Les surfaces en coupe (couche *poly_coupe*)

Il s’agit d’une couche vectorielle de polygones, ou de multipolygones, qui représente les surfaces observées ou restituées sur la coupe. Ces surfaces sont de deux natures : 
* les US 
* les inclusions (mobilier, matériaux divers, écofacts…). 

Une US dont une partie de la limite a été restituée par une interface doit être représentée en tenant compte de cette limite restituée. Le champ *"detail"* permet d'ajouter des détails sur la nature de l'US ou de l'inclusion si aucune base de données d'enregistrement n'existe par ailleurs.  
Une clé étrangère permet de relier ces surfaces à l’axe de relevé.

![Numérisation des surfaces en coupe][007]

## Quelques conseils pour la numérisation

S'agissant d'une base de données spatiales, la numérisation revêt un sens scientifique, à savoir, l'interprétation stratigraphique d'une coupe. Le respect des règles de topologie s'applique donc également pour le dessin en coupe et doit refléter les relations stratigraphiques existantes entre les objets.  Ainsi, et à titre d'exemples, les US en relations stratigraphiques sont dessinées en **adjacences** les unes des autres, les US négatives possèdent un tracé **identique** à l'interface de la ou des US constituant un fait et les inclusions, par définition, incluses dans une US, sont dessinées **sur** les US. 

Plusieurs méthodes de dessin sont possibles pour garantir à la fois un confort de dessin (avec des courbes de Bézier par exemple) et un respect des règles de topologie. Pour faciliter le dessin et en augmenter la vitesse d'exécution nous préconisons la méthode décrite ci-après selon un ordre précis.

### Dessin des axes en coupe

Comme expliqué [plus haut](https://gitlab.com/formationsig/fiches-techniques/-/blob/master/vecteur_dessin/coupe_SIG/coupes_QGIS.md#axe-de-relev%C3%A9-en-coupe-couche-axe_coupe), il est possible de générer automatiquement cette couche à partir de la couche **axe_plan** fournie par le topographe et après avoir vérifié que les axes comportaient bien un segment unique et que le sens de dessin correspondait bien au sens de lecture de la coupe.  
L'utilisation du model3 fourni permet d'éviter un dessin à la main de chaque axe qui est tout de même possible grâce aux outils de numérisation avancée de QGIS.

> :mag: **Utilisation des outils de numérisation avancée de QGIS** :  
>![Utilisation de l’outil de numérisation avancée][008]
>1. En session d'édition, activer l'outil
>2. Dessiner le premier point sur le canevas en spécifiant les coordonnées x (à 0 pour le premier axe et à distance du précédent axe pour ceux qui suivent) et y = altitude. Ne pas oublier de verrouiller (touche Entrée) les coordonnées avant de sortir de la fenêtre de numérisation avancée.
>3. Saisir la dimension du segment
>4. Verrouiller la dimension (touche Entrée) 
>5. Dessiner le second point du segment à l'horizontal du premier grâce aux guides de dessin de l'outil de numérisation avancée

:exclamation: La clé primaire des axes en coupe est indispensable ! Elle sert de clé étrangère à tous les objets dessinés (lignes et polygones) relatifs à la coupe. Elle peut-être identique à celle des axes en plan ou différente mais elle doit être présente absolument. Dans un shape, il est possible d'ajouter une clé primaire grâce à la formule `@row_number` ou `$id`. Le format SpatiaLite permet d'autogénérer une clé primaire.

### Géoréferencement de la minute sur la coupe

Idéalement, l'image raster représentant la coupe ne contient qu'une seule coupe pour faciliter l'archivage des données. Si le temps n'est pas suffisant pour individualiser les coupes sur un scan, il faudra créer autant de fichier géoréférencés que de coupes représentées sur ce dernier.

Le géoréférencement s'effectue avec la méthode Helmert sur deux points correspondant aux extrémités de la coupe. Activer l'accrochage sur les sommets d'**axe_coupe** pour plus de précision.  

![Géoréferencement de la minute][009]

Ne pas hésiter à vérifier les dimensions après le géoréférencement.

> :exclamation: **Attention** :  
>Si vous voulez prendre des mesures sur la coupe avec l'outil de mesure de QGIS, il faut le paramétrer pour obtenir des mesures Cartésiennes et non Ellipsoïdales. Les mesures ellipsoïdales tiendraient comptes des déformations du SCR qui n'ont pas lieu d'être en coupe.  
>![Outil de mesure en mode cartésien][010]

### Numérisation des interfaces et des surfaces

Comme évoqué plus haut, à terme, seules les US négatives, limites d'observation et restitutions sont conservées dans la couche **line_coupe**. Pour faciliter le dessin tout en garantissant un respect des règles de topologie, nous proposons une méthode de dessin qui permet à la fois d'utiliser les courbes de Bézier et de garantir un dessin sans erreur de géométrie ou de topologie. Dans la couche **poly_coupe**, figurent les US et les inclusions.  
Cette méthode nécessite d'utiliser les couches temporaires.

> :mag: **Rappel sur les couches temporaires** :  
>![Créer une couche temporaire][011] Les couches temporaires dans QGIS permettent de créer des fichiers de formes (points, lignes, polygones, ou même tables sans géométrie) qui, comme leur nom l'indique, ne sont pas enregistrées. Ces couches sont idéales pour dessiner des objets qui à terme ne seront pas conservés : des lignes de construction par exemples ou des résultats intermédiaires de calculs.  
> :exclamation: **Attention** : si ces couches ne sont pas enregistrées leur contenu disparaît à la fermeture du projet (y compris lors d'un plantage du logiciel).

1. Créer une couche temporaire de lignes ![Créer une couche temporaire][011]. Le nom n'est pas important puisque celle-ci ne sera pas conservée.
2. Dessiner sur cette couche la limite de décapage ou la limite supérieure de la coupe. Il est possible d'utiliser les courbes de Bézier pour respecter la courbe du dessin d'origine.  
![Dessin de la limite de décapage][012]
3. Activer l'accrochage sur les segments de la couche temporaire  
![Activation de l’accrochage sur la ligne temporaire][013]
4. Dessiner les limites verticales et horizontales de lecture de la coupe. L'utilisation de l'outil de numérisation avancée peut permettre de dessiner en se servant des guides verticaux et horizontaux (surlignés en jaune ci-dessous).  
![Utilisation des guides de l’outil de numérisation avancée][014]
5. Dessiner **toutes** les interfaces stratigraphiques en veillant à accrocher sur les segments pour respecter la topologie.
![Dessin des interfaces stratigraphiques de la coupe][015]
6. Sélectionner parmi les interfaces dessinées les seules : limites d'observation, US négatives et restitutions et les copier
7. Coller ces lignes sur la couche **line_coupe**
8. Avant de déselectionner les objets collés, il est possible de compléter par lot la table attributaire ![Outil de saisie d’attributs par lot][016]. On peut ainsi compléter la clé étrangère de numéro d'axe sur l'ensemble des lignes copiées.
9. Remplir les autres données attributaires nécessaires (*"typline"*, *"numfait"* et *"numus"* pour les US négatives)
10. Sur la couche de ligne temporaire, lancer le deuxième model3 (*coupe_line_vers_poly*) fourni en pièce jointe

> :mag: **NB** : Ce model3 permet de lancer une succession de traitements sur la couche de lignes temporaires :  
>Les lignes sont dans un premier temps légèrement prolongées aux extrémités pour créer une intersection (outil *Géométrie vectorielle/Prolonger les lignes*) puis des polygones sont créés pour chaque espace ainsi délimité (outil *Géométrie vectorielle/Mise en polygone*)  
>![Model3 de conversion des lignes vers des polygones][017]

11. Une nouvelle couche temporaire est créée. Sélectionner tous les polygones qui concernent la coupe dessinée, les copier et les coller dans la couche **poly_coupe**.  
![Résultat du model3][018]
13. Remplir les données attributaires, par lot ![Outil de saisie d’attributs par lot][016] et individuellement avec l'outil Identifier des entités ![Outil identifier des entités][019] 
14. Dessiner les éventuelles inclusions sur la couche **poly_coupe** avec l'outil souhaité (ajout de polygone ou courbe de bézier)

Votre coupe est terminée !

![Exemple de coupe achevée][020]

## Mise en page

Il est possible d'utiliser les mises en pages pour préparer des planches d'illustrations complexes qui incluent à la fois une vignette de localisation du fait, le plan, la coupe, des images et des extraits de table attributaire. Nous vous présenterons ci-après deux méthodes pour faciliter l'incorporation des coupes :  
* en générant un atlas
* en utilisant des variables

### Générer un atlas

Pour rappel, l'atlas permet de répéter une mise en page en déplaçant automatiquement le contenu des cartes selon une couche de couverture. En toute logique, la couche de couverture à privilégier pour un atlas incorporant les coupes serait la couche des axes en plan (**axe_plan**). La planche d'illustration comporterait *a minima* un premier élément carte pour la représentation en plan du ou des faits archéologiques et des axes de relevés et un autre élément carte pour la coupe. Le contenu de ces deux cartes se répondent selon la coupe concernée. Cependant, l'emprise en plan ne se trouve pas au même endroit en coupe. Il y aurait donc en théorie deux couches de couverture différentes : une pour le plan, une pour la couche. C'est impossible actuellement de paramétrer deux couches de couverture dans QGIS. Par contre, il est possible de contrôler l'emprise du contenu d'une carte dans les Propriétés de l'élément.

![Propriété d’emprise de contenu d’un élément carte][021]

A partir de cette fonctionnalité, l'idée est de calculer l'emprise de la coupe liée à l'axe en plan et de faire varier cette emprise pour chacune des pages de l'atlas. Plusieurs solutions sont possibles pour aboutir à ce résultat, toutes impliquent l'utilisation avancées des calculs en langage SQL.

Partant de ce principe, les étapes proposées de mise en place de l'atlas sont les suivantes :
1. Création de la couche de couverture en virtual layer ![Création d’une couche virtuelle][022] qui comporte les données géométriques de la couche **axe_plan** et, par jointure, les coordonnées x_min, x_max, y_min et y_max de la couche **axe_coupe**. La formule SQL est la suivante :  
`select p."id_axe", X(start_point(c.geometry)) as min_X, Y(start_point(c.geometry)) as min_Y, X(end_point(c.geometry)) as max_X, Y(end_point(c.geometry)) as max_Y, p.geometry as geometry  
from axe_plan as p  
join axe_coupe as c  
ON p."id_axe" =  c."id_coupe"`
2. Dans la mise en page, paramétrer l'atlas (1) avec, en couche de couverture, la couche virtuelle créée précédemment (2). Préciser le nom de la page comme étant l'identifiant (3) d'axe et ordonner par ce numéro (4).  
![Paramétrage de l’atlas][023]
3. Ajouter les différents éléments à votre mise en page, *a minima* :
* une carte dont le contenu est contrôlé par l'atlas qui contient le plan des faits archéologiques et les coupes associées  
![Carte contrôlée par l’atlas][024]
* une carte dont l'emprise est fonction des champs calculés dans la couche virtuelle (min_X, min_Y, max_X et max_Y) qui contient la coupe correspondante  
![Carte dont l’emprise est fonction des valeurs des données attributaires de la couche de couverture][025]
4. Générer l'atlas

> :mag: **NB** : nous vous conseillons d'utiliser les variables liées à l'atlas dans la configuration des symboles des couches présentes dans la mise en page. Elles vont permettront par exemple de ne faire apparaître que l'axe concerné par la coupe (`"id_axe" = @atlas_pagename`) d'afficher ou de mettre en surbrillance le fait concerné par des requêtes d'intersection : `intersects( $geometry,  @atlas_geometry )`.

En passant un peu de temps sur la mise en page de la première planche, on peut aboutir un niveau de complexité et de finition directement publiable.

![Exemple de planche d’atlas achevée][026]

### Utilisation de requêtes SQL et de variables

Dans le cas plus spécifique où plusieurs coupes sont attendues sur une même planche, pour illustrer un bâtiment sur poteaux par exemple, il est également possible "d'appeler" des coupes dans une mise en page. Comme pour le chapitre précédent, nous utiliserons la même couche virtuelle et les paramètres d'emprise définis par les données.

1. Si ce n'est pas fait, créer la couche virtuelle selon la requête suivante :  
```sql
SELECT p."id_axe", X(start_point(c.geometry)) AS min_X, Y(start_point(c.geometry)) AS min_Y, X(end_point(c.geometry)) AS max_X, Y(end_point(c.geometry)) AS max_Y, p.geometry AS geometry  
FROM axe_plan AS p  
JOIN axe_coupe AS c  
	ON p."id_axe" =  c."id_coupe"
```
2. Dans la mise en page, ajouter un élément carte et le renommer selon l'*"id_axe"* de la coupe à appeler. 
> :mag: **NB** : malheureusement il n'est pas toujours possible de numéroter un axe selon le fait concerné, plusieurs faits pouvant être traversés par un même axe et plusieurs axes pouvant traverser un même fait (relation de n à n). C'est pour cette raison que la clé primaire ne peut-être le numéro de fait. Afin de trouver facilement quels sont les numéros d'axe associés à quel(s) fait(s) archéologique(s), nous vous conseillons d'utiliser un tableau de jonction. Celui-ci est très facile à générer en utilisant la jointure spatiale ![Jointure spatiale][027].  
>![Paramétrer la jointure spatiale pour obtenir un tableau de correspondance « id_axe » ↔ « numpoly »][028]  
> le tableau obtenu ressemble à ça :  

| *"id_axe"* | *"numpoly"* |
| -------------------------- | ------------------------ |
| 2 | 150 |
| 3 | 160 |
| 4 | 3 |
| 5 | 4 |
| 6 | 5 |
| 7 | 6 |
| 8 | 61 |
| 9 | 80 |
| 10 | 7 |
| 11 | 79 |
| 12 | 25 |
| 13 | 78 |
| 14 | 26 |
| 14 | 85 |
| 15 | 77 |
| 16 | 77 |

> En filtrant sur le numéro de fait, on obtient facilement les numéros d'axes concernées.

3. Dans les propriétés de l'élément carte, et les paramètres d'emprise, sélectionner le menu Éditer... de la boîte de données définies par les valeurs de chaque coordonnée  
![Editer les coordonnées d’emprise de la carte][029]
4. Remplir comme suit les quatre paramètres de coordonnées :
* coordonnée min_X, la requête sera la suivante :  
```sql
attribute(get_feature('nom_de_la_couche_virtuelle','id_axe',@map_id),'min_X')
```
* coordonnée min_Y, la requête sera la suivante :  
```sql
attribute(get_feature('nom_de_la_couche_virtuelle','id_axe',@map_id),'min_Y')
```
* coordonnée max_X, la requête sera la suivante :  
```sql
attribute(get_feature('nom_de_la_couche_virtuelle','id_axe',@map_id),'max_X')
```
* coordonnée max_Y, la requête sera la suivante :  
```sql
attribute(get_feature('nom_de_la_couche_virtuelle','id_axe',@map_id),'max_Y')
```

> :mag: **NB** : plusieurs éléments de cette requêtes sont importants à commenter :  
>* `attribute('entité', 'champ')` : cet opérateur permet de récupérer l'attribut du champ qui contient la coordonnée souhaitée pour l'entité précisée en premier argument 
>* `get_feature('couche','champ','valeur')` : permet d'inclure une couche et une entité du projet dans la requête en précisant les arguments suivants :  
>   * 'couche', *couche* étant à remplacer par le nom de la couche virtuelle créée en 1
>   * 'champ' permet de préciser quel est le champ qui permet de trouver l'objet souhaité, ici *"id_axe"* 
>   * 'valeur', doit contenir le numéro de l'axe souhaité, on pourrait rentrer directement le numéro de l'axe mais on peut également utiliser les variables pour ne pas avoir à retoucher à chaque élément carte la requête. Nous proposons d'utiliser la variable `@map_id` qui correspond au nom courant de l'élément carte. D'où l'importance de renomment l'élément carte comme décrit dans le point 2.

5. Normalement, le contenu de la carte doit se mettre à jour en recentrant l'emprise sur la coupe du fait souhaitée. Si rien ne se passe, essayer d'actualiser la mise en page.
6. Si vous voulez insérer une nouvelle coupe dans cette mise en page, il suffit de copier/coller l'élément carte paramétrée dans les points précédents et d'en changer le nom pour qu'il corresponde au numéro de l'axe souhaité. Une nouvelle mise à jour de la mise en page devrait en changer le contenu.

Il est ainsi très facile de composer des mises en page complexes :

![Exemple de mise en page avec plusieurs coupes][030]

## Conclusion

Le traitement des coupes dans une base de données spatiales constitue une vraie opportunité d'aborder tous les aspects de l'enregistrement archéologique dans l'argumentaire scientifique. Il est désormais possible de constituer une base de données spatiales qui regroupe à la fois le plan issu du levé topographique et de relevés de terrain, les coupes issues des relevés et l'enregistrement attributaire descriptif.  
Hormis l'avantage certain de gérer l'ensemble de l'export d'illustrations complètes et complexes sans jongler entre les logiciels, cette dimension supplémentaire permet de formuler des requêtes et de les visualiser en direct sur l'ensemble des données stratigraphiques d'une opération archéologique.  
Parmi les requêtes attributaires possibles, il devient facile de visualiser les données stratigraphiques selon la nature des US, la présence de mobilier par cercles proportionnels, les textures... autant de descriptifs nécessaires à l'interprétation archéologique d'une occupation humaine. La dimension spatiale permet d'enrichir les données archéologiques par des requêtes géométriques : calcul des profondeurs automatiques, calcul de pente sur plusieurs coupes pour un fossé, support d'interpolation de surfaces... L'expérience et l'expérimentation compléteront par d'autres exploitations cette première liste.  
Le dessin des coupes n'est plus une simple mise au propre des minutes de terrain, il devient un traitement scientifique à part entière ce qui apporte une véritable valeur ajoutée au travail des archéologues sur le terrain et en phase d'étude. 

## Pièces jointes
* **Model 3** à copier/coller dans le dossier `C:\Users\ "*nom_session*" \AppData\Roaming\QGIS\QGIS3\profiles\default\processing\models` :  
   
   > :warning: Si vous utilisez le navigateur *Google Chrome* tous les fichiers seront téléchargés avec l'extension *.txt* → veillez à remettre l'extension d'origine (*.model3* ou  *.qml*)
   
   * [dessin_axe_coupe.model3](https://gitlab.com/formationsig/fiches-techniques/-/raw/master/vecteur_dessin/coupe_SIG/outils/models/dessin_axe_coupe.model3?inline=false) → à exécuter sur la couche axe_plan. Dessin automatique de la couche axe_coupe
   * [coupe_line_vers_poly.model3](https://gitlab.com/formationsig/fiches-techniques/-/raw/master/vecteur_dessin/coupe_SIG/outils/models/coupe_line_vers_poly.model3?inline=false) → à exécuter sur la couche temporaire des lignes évoquée [ici](#numérisation-des-interfaces-et-des-surfaces). Transforme la couche temporaire de lignes en polygones
   * [structuration_coupe_line.model3](https://gitlab.com/formationsig/fiches-techniques/-/raw/master/vecteur_dessin/coupe_SIG/outils/models/structuration_coupe_line.model3?inline=false) → à exécuter sur la couche définitive pour les interfaces. Ajoute les champs minimaux de structuration pour la couche line_coupe
   * [structuration_coupe_poly.model3](https://gitlab.com/formationsig/fiches-techniques/-/raw/master/vecteur_dessin/coupe_SIG/outils/models/structuration_coupe_poly.model3?inline=false) → à exécuter sur la couche définitive pour les surfaces. Ajoute les champs minimaux de structuration pour la couche poly_coupe
   
* **.qml** proposés pour les trois couches :
   * [style_axe_coupe.qml](https://gitlab.com/formationsig/fiches-techniques/-/raw/master/vecteur_dessin/coupe_SIG/outils/qml/style_axe_coupe.qml?inline=false) → proposition de symbole pour les axes de coupe avec l'indication de l'orientation et de l'altitude
   * [style_line_coupe.qml](https://gitlab.com/formationsig/fiches-techniques/-/raw/master/vecteur_dessin/coupe_SIG/outils/qml/style_line_coupe.qml?inline=false) → proposition de symbole pour les interfaces et liste déroulante des types d'interfaces
   * [style_poly_coupe.qml](https://gitlab.com/formationsig/fiches-techniques/-/raw/master/vecteur_dessin/coupe_SIG/outils/qml/style_poly_coupe.qml?inline=false) → proposition de symbole pour les surfaces en coupe et liste déroulante des types de surfaces

[001]:images/image_001.png
[002]:images/image_002.png
[003]:images/image_003.png
[004]:images/image_004.png
[005]:images/image_005.png
[006]:images/image_006.png
[007]:images/image_007.png
[008]:images/image_008.png
[009]:images/image_009.png
[010]:images/image_010.png
[011]:images/image_011.png
[012]:images/image_012.png
[013]:images/image_013.png
[014]:images/image_014.png
[015]:images/image_015.png
[016]:images/image_016.png
[017]:images/image_017.png
[018]:images/image_018.png
[019]:images/image_019.png
[020]:images/image_020.png
[021]:images/image_021.png
[022]:images/image_022.png
[023]:images/image_023.png
[024]:images/image_024.png
[025]:images/image_025.png
[026]:images/image_026.png
[027]:images/image_027.png
[028]:images/image_028.png
[029]:images/image_029.png
[030]:images/image_030.png
[logo_superQGIS]:images/logo_superQ.png
