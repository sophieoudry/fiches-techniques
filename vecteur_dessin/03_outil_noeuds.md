Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - équipe Formateurs & Référents SIG

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# L'outil de nœuds ou "éditeur de sommets"	

Cet outil permet de modifier la géométrie d’un objet existant en agissant sur les nœuds (ou sommets) et les segments de la géométrie. 

L'outil de nœuds n'est actif qu'en mode édition ; il s'agit de l'icône avec un marteau et un tournevis dans la barre d'outil "Edition". 

![outil noeud](images/noeud_01.png) 

L'outil peut être utilisé pour éditer une ou plusieurs couches en même temps (en **a** ci-dessous). Il est plus prudent de n'éditer qu'une seule couche à la fois.



En mode édition, les **sommets** des objets survolés par le curseur apparaissent sous la forme de cercles rouges, les **segments** sous forme de lignes rouges. 

Pour éditer les formes, il faut au préalable sélectionner par un clic l'élément de la forme d'origine à modifier (soit un ou plusieurs sommets, soit un ou plusieurs segments). Le choix de cette élément ce fait par survol du curseur sur l'entité à modifier. 

- **b** : lorsque le curseur survole un sommet, celui-ci se transforme en cercle (deux cercles concentriques apparaissent) 
- **c** : lorsqu'un segment est survolé, un halo rouge le met en évidence.
- **d** : lorsque le milieu d'un segment est survolé ou pour une ligne, son extrémité, un plus apparaît. 

![outil noeud détail](images/noeud_02.png)



#### Déplacer un élément

Commencer par sélectionner un élément (clic gauche), relâcher le clic, se placer à la destination, clic gauche pour indiquer la position finale. 



#### Annuler un déplacement

Faire un clic droit au lieu d'un clic gauche. La combinaison Ctrl + Z pour annuler la dernière action fonctionne pour cet outil.



#### Supprimer un élément

Sélectionner l'élément et appuyer sur la touche Suppr du clavier.



#### Sélectionner plusieurs éléments

Utiliser la touche Maj en même temps que la sélection ; les éléments sélectionnés apparaissent en bleu. 



#### Afficher le détail des nœuds

L'ensemble des nœuds apparaît sur l'entité et la liste des nœuds et leurs coordonnées apparaît sous forme d'un tableau. Il est possible de passer par ce tableau pour sélectionner un ou plusieurs (clic + Maj)
nœuds. Un clic-droit en dehors de la forme permet de sortir de l'Editeur de sommets.