# l'extension Bezier Editing

BezierEditing est une extension de QGIS pour éditer des entités en utilisant les courbes de Bézier.

> traduction française d'après le [wiki officiel de l'extension](https://github.com/tmizu23/BezierEditing/wiki/Document-(English)) avec l'aide du traducteur [DeepL](https://www.deepl.com/translator)


![BezierEditing](images/be_BezierEditing.png)


# Utilisation
Les outils suivants sont disponibles.   
![tools button](images/be_tools.png)


- Outil d'édition (*Bezier editing*)  
Créer une courbe de Bézier en traçant une ancre et en déplaçant la poignée.

- Outil à main levée (*Freehand tool*)  
Créer une courbe de Bezier en traçant une ligne.

- Outil diviser (*Split tool*)  
Diviser une courbe de Bézier.

- Outil fusionner (*Unsplit tool*)  
Fusionner deux courbes de Bézier.

- Outil afficher les poignées (*Show_Handle*)  
Afficher/désafficher les poignées de la courbe de Bézier.

- Outil annuler (*Undo tool*)  
Annule l'édition de la courbe de Bézier.



## ![](images/be_beziericon.svg) Outil d'édition (*Bezier editing*)
![](images/be_editing.gif)

1. Passez le calque en mode d'édition pour activer l'outil d'édition de courbes de Bézier.  
2. Cliquez sur le canevas de la carte pour ajouter une ancre. Pour déplacer la poignée, continuez de déplacer (*drag*) la souris.  
3. Si vous déplacez l'ancre , la position va changer.  
4. Affichez les poignées avec l'outil dédié ![](images/be_showhandleicon.svg), et si vous faites glisser la poignée, la forme de la courbe de Bézier changera. 
5. En outre, vous pouvez modifier la courbe avec les options suivantes .     
    - ![](images/be_anchor.svg) ajouter une ancre [clic]
    - ![](images/be_handle.svg) déplacer une ancre [glisser]
    - ![](images/be_anchor_add.svg) insérer une ancre dans une ligne de Bézier [alt + clic]
    - ![](images/be_anchor_del.svg) supprimer l'ancre d'une ligne de Bézier [shift + clic]  
    - ![](images/be_anchor.svg) forcer l'ajout d'une ancre sans la déplacer [ctrl + clic]
    - ![](images/be_handle.svg) déplacer la poignée [glisser]
    - ![](images/be_handle_add.svg) tirer la poignée de l'ancre [alt + drag]
    - ![](images/be_handle_del.svg) supprimer la poignée [shift + clic]
    - inverser la direction de la ligne de Bézier [clic droit sur la première ancre]
6. Faites un clic droit et entrez les attributs.
7. Si vous cliquez à nouveau sur l'entité avec le bouton droit de la souris, vous pourrez le modifier avec l'outil BezierEditing
    - Si les entités se chevauchent, celle qui est sélectionnée est prioritaire.
8. Vous pouvez également vous reporter au "Guide pratique" en bas de ce document.

## ![](images/be_freehandicon.svg) Outil à main levée (*Freehand tool*)
![](images/be_freehand.gif)

1. Avec le calque en mode édition, sélectionnez l'outil à main levée.
2. Faites glisser pour tracer une ligne.
3. Retracez la ligne pour la modifier.
4. Pour annuler la modification, appuyez sur le bouton d'annulation.
5. Faites un clic droit pour confirmer vos modifications.
6. Faites à nouveau un clic droit sur l'entité pour la modifier dans BezierEditing.
7. Les outils à main levée et les outils d'édition peuvent passer de l'un à l'autre.

## ![](images/be_spliticon.svg) Outil diviser (*Split tool*)
Attention: uniquement pour les polylignes.

![](images/be_split.gif)

1. Avec le calque en mode édition, sélectionnez l'outil diviser.
2. Faites un clic droit sur la polyligne pour la rendre éditable dans l'outil d'édition BezierEditing.
3. Cliquez à l'endroit où vous voulez diviser l'entité.

## ![](images/be_unspliticon.svg) Outil fusionner (*Unsplit tool*)
Attention: uniquement pour les polylignes.

![](images/be_unsplit.gif)

1. Avec le calque en mode édition, sélectionnez l'outil fusionner.
2. Cliquez ou faites rectangle de sélection pour sélectionner deux lignes que vous souhaitez fusionner.
3. Faire un clic droit (n'importe où) pour fusionner les deux lignes.

## ![](images/be_showhandleicon.svg) Outil afficher les poignées (*Show_Handle*)
Lorsque l'on appuie sur le bouton, les poignées sont affichées ou masquées.

## ![](images/be_undoicon.svg) Outil annuler (*Undo tool*)
Appuyez sur le bouton pour revenir à l'opération précédente d'édition de la courbe de Bézier.


## Guide pratique
1. Lorsque vous ajoutez une ancre, maintenez la touche Ctrl enfoncée et déplacez la souris pour afficher l'angle et la distance de l'ancre. L'angle est l'angle par rapport à l'horizontale si l'ancre est le deuxième point, et l'angle par rapport à la ligne entre les deux points si l'ancre est le troisième point.

    1. Le menu du guide pratique peut être affiché par [Ctrl] + clic droit sur l'outil d'édition.
    2. Pour activer l'accrochage à l'angle ou à la longueur, appuyez sur "*guide setting...*" (réglage du guide) dans le menu.
       - Définissez l'unité d'accrochage de l'angle et de la longueur. L'unité d'angle est le degré et l'unité de longueur est l'unité SCR du projet.
       - Si l'unité d'angle est réglée sur 45, l'accrochage se fait sur un intervalle de 45 degrés.
       - Si l'unité de longueur est fixée à 60, l'accrochage se fait à un intervalle de 60 mètres en RGF93 et de 60 secondes en WGS84.
       - Si la valeur est 0, il n'y a pas d'accrochage.
    3. Pour réinitialiser l'accrochage du guide pour l'ancre, appuyez sur "*reset guide*" (réinitialiser le guide) dans le menu.

    


## Notes
- BezierEditing supporte les couches de Point, de Polyligne, et de Polygone.
- Au besoin, changez les paramètres d'accrochage de QGIS.
- supporté par la v1.3.0 ~~Le CRS du projet est une latitude et une longitude inattendue.~~~~
- supporté par la v1.2.1 ~~Les entités créées avec d'autres outils ne peuvent pas être converties en courbes de Bézier.~~
- supporté par la v1.2.1 ~~Les entités qui ont été éditées par d'autres outils ne peuvent pas être converties correctement en courbes de Bezier.~~
- supporté par la v1.2.1 ~~Un polygone ne peut pas être reconverti à moins qu'il ne soit finalisé avec la première et la dernière ancre acdrochée.~~