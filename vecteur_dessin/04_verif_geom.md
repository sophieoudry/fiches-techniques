Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - équipe Formateurs & Référents SIG

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# Vérification des géométries

A toutes les étapes de la numérisation (lever topographique, dessin), des erreurs de géométrie sont susceptibles de se glisser dans vos vecteurs. Ces erreurs peuvent être des nœuds en double, des segments qui s'entrecroisent, etc. 

:bangbang: Il est important de les corriger, car la présence d'erreurs de géométrie dans les couches vecteurs empêche certaines manipulations, comme les jointures spatiales. Dans certains cas, elles peuvent même faire planter le logiciel. 



## Outil Vérifier la validité des géométries

* Vecteur > Outils de géométrie > Vérifier la validité


Sélectionner la couche et laisser les autres éléments par défaut

![outil Vérifier la validité](images/verif_geom_01.png)   

L'outil Vérifier la validité va créer trois couches temporaires : sortie valide, sortie invalide et erreur de sortie. 

![trois couches temporaires](images/verif_geom_02.png) 

Pour procéder aux corrections, ouvrir la table attributaire de la couche *Erreur de sortie* : elle recense les erreurs (nœuds en doublon, auto-intersection de segments). En sélectionnant la ligne de l'erreur et en zoomant sur l'entité sélectionnée (Ctrl + J), il est possible de voir l'erreur puis de la corriger. 

{% hint style='info' %} A vous de choisir sur quelle couche vous effectuez les corrections : la couche d'origine ou la couche "sortie invalide". Dans le premier cas, si vous faites de mauvaises manipulations, vous risquez de perdre vos données. Dans le second cas, vos données sont à l'abri mais vous aurez à réintégrer les entités corrigées.

Une solution possible est de conserver une copie de la couche d'origine et de travailler sur cette copie. {% endhint %}

Recommencer pour chacune des lignes. A la fin des corrections, supprimer ces trois champs temporaires et recommencer le traitement pour vérifier que toutes les erreurs ont bien été corrigées. 



## Outil Vérifier les géométries

* Vecteur > Vérifier les géométries

Cet outil permet de vérifier la géométrie et la topologie ; il nécessite que vous ayez vérifié les validités au préalable.

![outil Vérifier les géométries](images/verif_geom_03.png) 



En bas de la fenêtre, il est possible de choisir entre *Modifier les couches source* et *Créer une nouvelle couche*. Il est préférable de choisir la seconde option. 

![](images/verif_geom_04.png) 

Cliquer sur Exécuter. 

Le second onglet (Résultat) de la fenêtre s'affiche : 

![onglet Résultat de l'outil Vérifier les géométries](images/verif_geom_05.png) 

Cet outil permet de corriger les erreurs selon une correction par défaut (par exemple pour les nœuds en double, suppression du doublon) ou bien selon une correction choisie par l'utilisateur.

Les paramètres de correction par défaut peuvent être choisis via l'outil Paramètres de correction d'erreur (selon les versions de QGIS, cette fenêtre peut être totalement ou partiellement traduite en français) :

![paramètres de correction](images/verif_geom_06.png) 

Dans le doute, laisser à "No action".



{% hint style='working' %}

Selon la taille de votre fichier (nombre de points ou d'entités), il est préférable de ne pas lancer l'analyse sur toutes erreurs possibles mais de n'en sélectionner qu'une ou deux à la fois : ce processus de vérification est très gourmand et peut prendre beaucoup de temps.

{% endhint %}

