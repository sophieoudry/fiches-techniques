Ce document est sous [licence Creative Commons](https://creativecommons.org/licenses/by-nd/4.0/) ![](images/CC.png)  

Inrap - équipe Formateurs & Référents SIG

> Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS



# Le dessin (ou "numérisation") dans QGIS

## 1. L'édition

### Le mode édition

Pour numériser "dessiner" des formes dans GQIS, il faut être en **mode édition** ![mode édition](images/numerisation_01.png)et **positionné sur la couche** que l'on souhaite modifier.

![couche en mode édition](images/numerisation_02.png) 

Le dessin ne peut se faire que sur une couche vecteur (format .shp, .gpkg, etc.), dont on rappelle qu'elle ne peut avoir qu'une des géométries suivantes : *point*, *ligne* (ou polyligne) ou *polygone*, ce qui influe évidemment sur les possibilités de dessin au sein de la couche.



### Créer une couche vecteur vierge

Si on part de zéro, il faut avoir créé une **nouvelle couche vecteur** vierge ![créer une nouvelle couche](images/numerisation_03.png). Attention au système de coordonnées de références (en général IGNF Lambert 93, EPSG 2154), au type de géométrie souhaité et à la table attributaire : quels sont les champs (colonnes) que je souhaite pour créer ma couche. 

Vous pouvez aussi reprendre une des **6 couches vierges normalisées** fournies par le réseau des référents SIG Inrap pour le besoin des opérations courantes : elles contiennent déjà des tables attributaires préparées avec un format compatible pour la publication dans **Caviar**. 



### Précautions à prendre en mode édition

Tant que l'on est en mode édition, des annulations sont possibles (Ctrl + Z) jusqu'à ce que l'on sauvegarde ![bouton enregistrer](images/numerisation_04.png)les modifications effectuées.

Les modifications, une fois sauvegardées, affectent le fichier vecteur pour TOUS les projets qui utilisent ce vecteur : les modifications ne sont pas sauvegardées *seulement* dans votre projet. 

{% hint style='tip' %} Attention donc à **bien savoir sur quelle couche vous travaillez** : est-elle partagée avec d'autres ? est-ce une copie ? {% endhint %}



### Trois aspects de la numérisation

- La **création** qui se fait soit au travers d'outils simples qui permettent de créer des points, des lignes ou des polygones, soit au travers d'outils complémentaires accessibles par des extensions (création de formes spécifiques par exemple)
- La **modification** du tracé de base : rotation, ajout de sommet, etc.
- Des **outils d'aide au dessin** permettent de mieux contrôler la numérisation en obligeant le tracé à respecter certaines limites, à contraindre les longueurs ou les angles.



## 2. Le beau et le vrai

En archéologie, les tracés utilisés dans les SIG sont assez souvent des exports "bruts" issus des relevés topographiques. Il en résultat parfois un **dessin des structures qui est juste, mais assez fruste** et très modélisé. On peut évidemment raffiner et détailler le tracé, en reprenant par exemple les relevés de terrain. Le but peut être purement esthétique - pour coller à des normes de représentation - ou encore de viser à une plus grande exactitude des formes vues sur le terrain.

Dans tous les cas, gardez à l'esprit que le dessin dans un SIG n'est pas "neutre" comme dans un logiciel de DAO. Vous ne réalisez pas une figure finalisée ou figée mais **vous dessinez des géométries qui vont servir dans différentes représentations et dans d'éventuelles analyses**. La position (géoréférencée) et la géométrie des entités représentent ("modélisent") le réel. Elles influencent donc les analyses telles que les requêtes spatiales ou les calculs de surface. Elles s'accompagnent d'ailleurs d'une série de données, dans la table attributaire, même pour des représentations très détaillées de structures archéologiques. 

Vous pouvez choisir un mode de représentation complexe, par exemple avec des pierre à pierre complets, ou au contraire des tracés très simplifiés, très modélisés. Mon site, mon choix. Mais il convient d'anticiper les conséquences éventuelles sur la suite du traitement cartographique, ne serait-ce que pour les représentations à différentes échelles. Il faut également chercher à garder une cohérence (homogénéité) dans le dessin. Faut de quoi, certaines de vos requêtes risquent de reposer sur des bases non pertinentes ou incomplètes.



## 3. Attention aux formes

Pour qu'elles soient valides, les formes (lignes et polygones) doivent répondre à certaines règles, dont certaines sont évidentes : une ligne doit avoir plus de 2 points, un polygone plus de 3. Les segments d'un polygone ne doivent pas se croiser et les sommets ne doivent pas être en double (sommets strictement placés l'un sur l'autre). 

Ces erreurs peuvent arriver lorsque l'on dessine dans QGIS ou lors de l'export des données topo. 

QGIS signale ces erreurs par une **croix verte** en cours de dessin ou lors de l'utilisation de [l'outil de nœuds](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/outil_noeuds.html). Le logiciel n'empêche pas le dessin avec de telles erreurs, mais cela peut poser problème lors des traitements et des analyses. 

![erreur numérisation](images/numerisation_05.png) 

Attention, **en cours de dessin d'un polygone**, les segments peuvent être amenés à se croiser mais c'est parfois temporaire et sera réglé à la fin du dessin. QGIS signale tout de même ces erreurs. 



## 3. Les outils de numérisation de base

Ils sont accessibles par la barre d'outil de numérisation. Si cette barre n'est pas visible, clic droit dans la zone des barre d'outils et cocher la barre concernée. Passer en mode édition sur la couche. 

![barre d'outil numérisation](images/numerisation_06.png) 

- Créer une géométrie (selon la nature de la couche)

![créer point](images/numerisation_07.png) Créer un point

![créer ligne](images/numerisation_08.png) Créer une ligne

![créer polygone](images/numerisation_09.png) Créer un polygone

Pour les lignes et polygones : 1 clic par sommet, **clic droit** pour terminer. Pour les polygones, **ne pas revenir** sur le premier point, clic droit sur **l'avant-dernier point**. 

Une fenêtre avec les champs de la table attributaire apparaît en fin de dessin. 

![outil de noeud](images/numerisation_10.png) Outil de nœud : pour modifier la forme de votre géométrie à partir des sommets ou des segments. Voir la [fiche qui lui est dédiée](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/outil_noeuds.html). 

![modifier toutes les entités](images/numerisation_11.png) Modifier les attributs de toutes les entités simultanément : c'est déconseillé, à moins que vous ne sachiez exactement ce que vous faites

![supprimer entité](images/numerisation_12.png) Supprimer une entité : l'entité doit être sélectionnée au préalable

![couper entité](images/numerisation_13.png) Couper une entité : sur le principe du couper-coller dans un traitement de texte. Attention, ça ne découpe pas la géométrie, ça prend la totalité de l'entité. Pour découper une géométrie, voir la fiche sur la numérisation avancée. 

![copier entité](images/numerisation_14.png) Copier une entité

![coller entité](images/numerisation_15.png) Coller une entité

![annuler / refaire](images/numerisation_16.png) Annuler/refaire



## 4. L'extension Bezier Editing

Il existe plusieurs possibilités dans QGIS pour dessiner des polylignes ou polygones courbes. Nous conseillons l'utilisation de l'extension **Bezier Editing** qui se comporte à peu près de la même manière que l'outil Plume dans Adobe Illustrator (TM). 

Il est donc possible d'alterner les lignes droites et les lignes courbes. Un autre intérêt de cet outil est que le tracé reste **éditable**, même après que la forme a été fermée (pour cela, faire un clic droit dans la forme) Le dernier point ne doit **pas** se superposer au premier mais être suffisamment près pour ne pas créer de décrochement dans le courbe après validation par un clic-droit. 

Après finalisation du dessin, le logiciel ajoute des nœuds supplémentaires sur la ligne tracée avec l'outil, ce qui donne l'impression d'une courbe. 

![outil bezier](images/numerisation_17.png) 

1. Active l'outil
2. Permet le dessin à main levée
3. Bezier split : couper la courbe. Clic-droit pour la rendre éditable puis cliquer où on veut couper
4. Bezier unsplit : joindre deux courbes. Sélectionner les deux parties et clic droit
5. Permet de visualiser les tangentes de chaque courbe pour pouvoir en modifier l'aspect
6. Annuler